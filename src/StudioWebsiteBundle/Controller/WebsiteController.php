<?php

namespace StudioWebsiteBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


use Sng\Exception\NotFoundException;
use Sng\Model\Account\AccountMembership;
use Sng\Model\Account\Client;
use Sng\Model\Account\ClientService;
use Sng\Model\Account\Service;
use Sng\Model\Sale\Membership;
use Sng\Model\ScheduledClass\Booking;
use Sng\Model\ScheduledClass\ScheduledClass;
use Sng\Model\Site\Site;
use SngBundle\Service\ClassesService;
use SngBundle\Service\LocationsService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use SngBundle\Form\UserLoginType;
use SngBundle\Form\UserRegistrationType;

class WebsiteController extends Controller
{
    const ROBOTS_CACHE_KEY = 'robotstxt.%s';
    const ROBOTS_CACHE_TTL = 600;
    const FILTER_SITE_ID_KEY = 'sitesId';

    private function sendContactEmail($data)
    {
        $mailer = $this->get('mailer');
        $message = (new \Swift_Message('[Website][Contact] New message'))
            ->setFrom('noreply@sweatnglow.com')
            ->setTo('contact@sweatnglow.com')
            ->setBody(
                $this->renderView(
                    'SngBundle:Emails:Contact/contact_message.html.twig', array(
                        'name' => $data['name'],
                        'email' => $data['email'],
                        'message' => $data['message'],
                    )
                ),
                'text/html'
            );

        return $mailer->send($message);
    }


    /** STUDIO WEBSITE TEMPLATE CONTROLLERS  */

    /**
     * @Route("/studio-template/", name="studioTemplate")
     */
    public function studioTemplateAction()
    {
        return $this->render('StudioWebsiteBundle::index.html.twig', [
        ]);
    }
    /**
     * @Route("/studio-template/{slug}/schedule", name="studioTemplateSchedule")
     */
    public function studioTemplateScheduleAction(Request $request, $slug)
    {
        $locationsService = $this->get('sng.locations');
        
        $location = $locationsService->getLocationBySlug($slug);
        if (!$location) {
            throw new NotFoundHttpException('Studio was not found.');
        }

        $now = new \DateTime();
        $startLimit = clone $now;
        $startLimit->setTime(0 ,0 ,0);
        $startLimit->add(new \DateInterval('P3M'));
        $startLimit->sub(new \DateInterval('P7D'));

        $reqStart = $request->query->get('from');

        if (!empty($reqStart)) {
            try {
                $startDatetime = new \DateTime($reqStart);
                $startDatetime->setTime(0, 0,0 );

                // We have a hard limit of 3 months
                if ($startDatetime > $startLimit) {
                    $startDatetime = null;
                }
            } catch (\Exception $e) {}
        }
        if (!isset($startDatetime) || $startDatetime < $now) {
            $startDatetime = clone $now;
            $startDatetime->setTime(0, 0, 0);
        }

        $endDatetime = clone $startDatetime;
        $endDatetime->setTime(23, 59, 59);
        $endDatetime->add(new \DateInterval('P7D'));

        // Use the cache only when we don't have the 'start' param
        $classes = $this->get('sng.classes')->getClasses(
            [
                ClassesService::FILTER_LOCATIONS_KEY => [$location->getUuid()],
                ClassesService::FILTER_START_DATETIME_KEY => $startDatetime->format('Y-m-d H:i:s'),
                ClassesService::FILTER_END_DATETIME_KEY => $endDatetime->format('Y-m-d H:i:s'),
            ],
            ['startDatetime' => 'asc'],
            200,
            0,
            empty($reqStart)
        );

        // group by day
        $classesByDay = [];
        $interval = \DateInterval::createFromDateString('1 day');
        $period = new \DatePeriod($startDatetime, $interval, $endDatetime);
        foreach ($period as $dt) {
            $classesByDay[$dt->format('d-m-Y')] = [];
        }
        /** @var ScheduledClass $class */
        foreach ($classes as $class) {
            try {
                $startDate = new \DateTime($class->getStartDatetime());
            } catch (\Exception $e) {
                continue;
            }
            $classesByDay[$startDate->format('d-m-Y')][] = $class;
        }

        $nextDatetime = clone $endDatetime;
        $nextDatetime->add(new \DateInterval('PT0H1S'));
        if ($nextDatetime >= $startLimit) {
            $nextDatetime = null;
        }

        $prevDatetime = null;
        if ($startDatetime > $now) {
            $prevDatetime = clone $startDatetime;
            $prevDatetime->sub(new \DateInterval('P8D'));
        }

        return $this->render('StudioWebsiteBundle::schedule.html.twig', [
            'studio' => $location,
            'classes' => $classes,
            'classesByDay' => $classesByDay,
            'startDatetime' => $startDatetime,
            'endDatetime' => $endDatetime,
            'nextDatetime' => $nextDatetime,
            'prevDatetime' => $prevDatetime,
        ]);
    }

    /**
     * @Route("/studio-template/studio/{slug}/", name="studioTemplateStudio")
     */
    public function studioTemplateStudioAction($slug, Request $request)
    {
        $locationsService = $this->get('sng.locations');
        $location = $locationsService->getLocationBySlug($slug);
        if (!$location) {
            throw new NotFoundHttpException('Studio was not found.');
        }

        $now = new \DateTime();
        $endOfTheDay = clone $now;
        $endOfTheDay->modify('tomorrow');
        $endOfTheDay->modify('1 second ago');
        $todaysClasses = $this->get('sng.classes')->getClasses(
            [
                ClassesService::FILTER_LOCATIONS_KEY => [$location->getUuid()],
                ClassesService::FILTER_START_DATETIME_KEY => $now->format('Y-m-d H:i:s'),
                ClassesService::FILTER_END_DATETIME_KEY => $endOfTheDay->format('Y-m-d H:i:s'),
            ],
            ['startDatetime' => 'asc'],
            12,
            0,
            true
        );

        $staff = $this->get('sng.staff')->getActiveStaffByLocation(
            $location->getUuid(),
            ['featured' => 'desc']
        );

        return $this->render('StudioWebsiteBundle::studioDetail.html.twig', [
            'studio' => $location,
            'classes' => $todaysClasses,
            'teachers' => $staff,
        ]);
    }
    /**
     * @Route("/studio-template/teachers/", name="studioTemplateTeachers")
     */
    public function studioTemplateTeachersAction()
    {
        return $this->render('StudioWebsiteBundle::teachers.html.twig');
    }
    /**
     * @Route("/studio-template/about-sng/", name="studioTemplateAboutSng")
     */
    public function studioTemplateAboutSngAction()
    {
        return $this->render('StudioWebsiteBundle::about-sng.html.twig');
    }

    /**
     * @Route("/studio-template/about-hot-yoga/", name="studioTemplateAboutHotYoga")
     */
    public function studioTemplateAboutHotYogaAction()
    {
        return $this->render('StudioWebsiteBundle::about-hot-yoga.html.twig');
    }

    /**
     * @Route("/studio-template/contact/", name="studioTemplateContact")
     */
    public function studioTemplateContactAction(Request $request)
    {
        $form = $this->createForm('SngBundle\Form\ContactType', null, array(
            'action' => $this->generateUrl('contact'),
            'method' => 'POST',
            'attr' => array('id' => 'contactForm'),
        ));

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($this->sendContactEmail($form->getData())) {
                $this->get('session')->getFlashBag()->add('msg', 'Thanks, we’ll be in touch soon!');

                return $this->redirectToRoute('contact', array());
            } else {
            }
        }

        return $this->render('StudioWebsiteBundle::contact.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
