<?php

namespace SngBundle\Security\User;

use Symfony\Component\Security\Core\User\UserInterface;

class User implements UserInterface
{
    const ROLE_SNG_USER = 'ROLE_SNG_USER';
    const ROLE_FACEBOOK_USER = 'ROLE_FACEBOOK_USER';
    const ROLE_GOOGLE_USER = 'ROLE_GOOGLE_USER';

    private $uuid;
    private $email;
    private $password;
    private $name;
    private $profileImageUrl;
    private $confirmedAt;
    private $createdAt;
    private $roles;

    public function __construct()
    {
        $this->roles = ['ROLE_USER'];
    }

    public function getId()
    {
        return $this->getUuid();
    }

    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
    }

    public function getUuid()
    {
        return $this->uuid;
    }

    public function getUsername()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setProfileImageUrl($profileImageUrl)
    {
        $this->profileImageUrl = $profileImageUrl;
    }

    public function getProfileImageUrl()
    {
        return $this->profileImageUrl;
    }

    public function getSalt()
    {
        return null;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setConfirmedAt($confirmedAt)
    {
        $this->confirmedAt = $confirmedAt;
    }

    public function getConfirmedAt()
    {
        return $this->confirmedAt;
    }

    public function isConfirmed()
    {
        return $this->confirmedAt != '0000-00-00 00:00:00';
    }

    public function getRoles()
    {
        return $this->roles;
    }

    public function addRole($role)
    {
        if (!in_array($role, $this->roles)) {
            $this->roles[] = $role;
        }
    }

    public function eraseCredentials()
    {
    }
}
