<?php

namespace SngBundle\Security\User;

use Psr\Log\LoggerInterface;
use SngBundle\Form\UserLoginType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use League\OAuth2\Client\Provider\GenericProvider;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\Exception\AuthenticationCredentialsNotFoundException;
use Sng\Model\Account\Account;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

class SngAuthenticator extends AbstractGuardAuthenticator
{
    use TargetPathTrait;

    private $sngProvider;
    private $router;
    private $formFactory;
    private $requestStack;
    private $logger;

    public function __construct(
        GenericProvider $sngProvider,
        RouterInterface $router,
        FormFactoryInterface $formFactory,
        RequestStack $requestStack,
        LoggerInterface $logger
    )
    {
        $this->sngProvider = $sngProvider;
        $this->router = $router;
        $this->formFactory = $formFactory;
        $this->requestStack = $requestStack;
        $this->logger = $logger;
    }

    public function getCredentials(Request $request)
    {
        if ($request->getMethod() != 'POST' || $request->getPathInfo() != '/connect/sng-check') {
            return null;
        }

        $form = $this->formFactory->create(UserLoginType::class);
        $form->handleRequest($this->requestStack->getCurrentRequest());
        if ($form->isSubmitted() && $form->isValid()) {
            $formData = $form->getData();

            $username = $formData['email'];
            $password = $formData['password'];
            $request->getSession()->set(Security::LAST_USERNAME, $username);
            if (!is_null($username) && !is_null($password)) {
                return [
                    'username' => $username,
                    'password' => $password,
                ];
            }
        }

        throw new AuthenticationException(
            'There was an error getting access. Please try again.'
        );
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        if (empty($credentials['username']) || empty($credentials['password'])) {
            throw new AuthenticationException('Invalid credentials.');
        }

        try {
            // the credentials are really the access token
            $accessToken = $this->sngProvider->getAccessToken(
                'password',
                $credentials
            );
        } catch (IdentityProviderException $e) {
            $response = $e->getResponseBody();
            $errorCode = $response['error'];

            $this->logger->warning('SNG Authentication error - ' . $errorCode);

            if ($errorCode == 'access_denied') {
                throw new AuthenticationException('Invalid credentials.');
            } else {
                throw new AuthenticationException(
                    'There was an error logging you. Please try again.'
                );
            }
        }

        /* @var Account $sngUser */
        try {
            $sngUser = $this->sngProvider->getResourceOwner($accessToken);
        } catch (\UnexpectedValueException $e) {
            throw new AuthenticationException('Failed authentication. Please try again.');
        }

        try {
            $user = $userProvider->loadUserByUuid($sngUser->getId());
        } catch (AuthenticationCredentialsNotFoundException $e) {
            // continue
        } catch (\Exception $e) {
            throw new AuthenticationException('Failed authentication. Please try again.');
        }

        if (!isset($user)) {
            throw new AuthenticationException('Failed authentication. Please try again.');
        }

        $user->addRole(User::ROLE_SNG_USER);

        return $user;
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        return true;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $request->getSession()->set(Security::AUTHENTICATION_ERROR, $exception);
        $url = $this->router->generate('login');

        return new RedirectResponse($url);
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        $targetPath = $request->headers->get('referer');

        if ($request->getSession() instanceof SessionInterface) {
            if ($sessionTargetPath = $this->getTargetPath($request->getSession(), $providerKey)) {
                $targetPath = $sessionTargetPath;
                $this->removeTargetPath($request->getSession(), $providerKey);
            }
        }

        if (!$targetPath || strpos($targetPath, '/login') !== false) {
            $targetPath = $this->router->generate('homepage');
        }

        return new RedirectResponse($targetPath);
    }

    public function supportsRememberMe()
    {
        return true;
    }

    public function start(Request $request, AuthenticationException $authException = null)
    {
        $url = $this->router->generate('login');

        return new RedirectResponse($url);
    }
}
