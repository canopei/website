<?php

namespace SngBundle\Security\User;

use Psr\Log\LoggerInterface;
use SngBundle\Event\UserRegisteredEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use League\OAuth2\Client\Provider\Google;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use League\OAuth2\Client\Provider\GoogleUser;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sng\Exception\AlreadyExistsException;
use SngBundle\Security\Exception\EmailAlreadyExistsException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\Exception\AuthenticationCredentialsNotFoundException;
use Sng\ClientInterface;

class GoogleAuthenticator extends AbstractGuardAuthenticator
{
    private $googleProvider;
    private $router;
    private $sngClient;
    private $eventDispatcher;
    private $logger;

    public function __construct(
        Google $googleProvider,
        RouterInterface $router,
        ClientInterface $sngClient,
        EventDispatcherInterface $eventDispatcher,
        LoggerInterface $logger
    )
    {
        $this->googleProvider = $googleProvider;
        $this->router = $router;
        $this->sngClient = $sngClient;
        $this->eventDispatcher = $eventDispatcher;
        $this->logger = $logger;
    }

    public function getCredentials(Request $request)
    {
        if ($request->getPathInfo() != '/connect/google-check') {
            return null;
        }

        if ($code = $request->query->get('code')) {
            return $code;
        }

        throw new AuthenticationException(
            'There was an error getting access from Google. Please try again.'
        );
    }

    public function getUser($authorizationCode, UserProviderInterface $userProvider)
    {
        try {
            // the credentials are really the access token
            $accessToken = $this->googleProvider->getAccessToken(
                'authorization_code',
                ['code' => $authorizationCode]
            );
        } catch (IdentityProviderException $e) {
            $response = $e->getResponseBody();
            $errorCode = $response['error'];

            $this->logger->warning('Google Authentication error - ' . $errorCode);

            throw new AuthenticationException(
                'There was an error logging you with Google. Please try again.'
            );
        }

        /** @var GoogleUser $googleUser */
        $googleUser = $this->googleProvider->getResourceOwner($accessToken);

        try {
            $user = $userProvider->loadUserByExternalUserId(UserProvider::EXTERNAL_PROVIDER_GOOGLE, $googleUser->getId());
        } catch (AuthenticationCredentialsNotFoundException $e) {
            // continue
        } catch (\Exception $e) {
            throw new AuthenticationException('Failed authentication. Please try again.'.$e->getMessage());
        }

        if (!isset($user)) {
            // Register new user
            try {
                $account = $this->sngClient->Account()->createAccountWithExternalLogin(
                    UserProvider::EXTERNAL_PROVIDER_GOOGLE,
                    $googleUser->getId(),
                    $googleUser->getEmail(),
                    $googleUser->getFirstName(),
                    $googleUser->getLastName(),
                    $googleUser->getAvatar()
                );
            } catch (AlreadyExistsException $e) {
                throw new EmailAlreadyExistsException('An account already exists with this e-mail address.');
            } catch (\Exception $e) {
                throw new AuthenticationException('Failed authentication. Please try again.'.$e->getMessage());
            }

            try {
                $user = $userProvider->loadUserByExternalUserId(UserProvider::EXTERNAL_PROVIDER_GOOGLE, $googleUser->getId());
            } catch (\Exception $e) {
                throw new AuthenticationException('Failed registration. Please try again.'.$e->getMessage());
            }

            $userRegisteredEvent = new UserRegisteredEvent($account);
            $this->eventDispatcher->dispatch(UserRegisteredEvent::NAME, $userRegisteredEvent);
        }

        $user->addRole(User::ROLE_GOOGLE_USER);

        return $user;
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        return true;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $request->getSession()->set(Security::AUTHENTICATION_ERROR, $exception);
        $url = $this->router->generate('login', ['_fragment' => 'register']);

        return new RedirectResponse($url);
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        $targetPath = $request->headers->get('referer');

        if ($request->getSession() instanceof SessionInterface) {
            if ($sessionTargetPath = $this->getTargetPath($request->getSession(), $providerKey)) {
                $targetPath = $sessionTargetPath;
                $this->removeTargetPath($request->getSession(), $providerKey);
            }
        }

        if (!$targetPath || strpos($targetPath, '/login') !== false) {
            $targetPath = $this->router->generate('homepage');
        }

        return new RedirectResponse($targetPath);
    }

    public function supportsRememberMe()
    {
        return true;
    }

    public function start(Request $request, AuthenticationException $authException = null)
    {
        $url = $this->router->generate('login');

        return new RedirectResponse($url);
    }
}
