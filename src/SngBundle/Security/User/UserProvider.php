<?php

namespace SngBundle\Security\User;

use Sng\Model\Account\Account;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\AuthenticationCredentialsNotFoundException;
use Sng\Exception\AccessDeniedException;
use Sng\Exception\NotFoundException;

class UserProvider implements UserProviderInterface
{
    const EXTERNAL_PROVIDER_FACEBOOK = 'facebook';
    const EXTERNAL_PROVIDER_GOOGLE = 'google';

    private $sngClient;

    public function __construct(\Sng\ClientInterface $sngClient)
    {
        $this->sngClient = $sngClient;
    }

    public function loadUserByUsername($username)
    {
        return null;
    }

    public function loadUserByUuid($uuid)
    {
        try {
            /** @var Account $account */
            $account = $this->sngClient->Account()->getAccount($uuid);
        } catch (NotFoundException $e) {
            throw new AuthenticationCredentialsNotFoundException();
        } catch (\InvalidArgumentException $e) {
            throw new AuthenticationCredentialsNotFoundException();
        } catch (\UnexpectedValueException $e) {
            throw $e;
        }

        $user = new User();
        $user->setEmail($account->getEmail());
        $user->setUuid($account->getUuid());
        $user->setName($account->getFullName());
        $user->setProfileImageUrl($account->getProfileImageUrl());
        $user->setConfirmedAt($account->getConfirmedAt());
        $user->setCreatedAt($account->getCreatedAt());

        return $user;
    }

    public function loadUserByExternalUserId($providerName, $externalUserId)
    {
        try {
            $account = $this->sngClient->Account()->getAccountByExternalUserID($providerName, $externalUserId);
        } catch (NotFoundException $e) {
            throw new AuthenticationCredentialsNotFoundException();
        } catch (\InvalidArgumentException $e) {
            throw new AuthenticationCredentialsNotFoundException();
        } catch (\UnexpectedValueException $e) {
            throw $e;
        }

        $user = new User();
        $user->setEmail($account->getEmail());
        $user->setUuid($account->getUuid());
        $user->setName($account->getFullName());
        $user->setProfileImageUrl($account->getProfileImageUrl());
        $user->setConfirmedAt($account->getConfirmedAt());
        $user->setCreatedAt($account->getCreatedAt());

        return $user;
    }

    public function loadUserByEmailAndPassword($email, $password)
    {
        try {
            $accessToken = $this->sngClient->Auth()->authenticateAccount($email, $password);
        } catch (AccessDeniedException $e) {
            throw new AuthenticationCredentialsNotFoundException();
        } catch (\InvalidArgumentException $e) {
            throw new AuthenticationCredentialsNotFoundException();
        } catch (\UnexpectedValueException $e) {
            throw $e;
        }

        // Fetch the user details from the API
        $account = $this->sngClient->Account()->getAccount($email);

        $user = new User();
        $user->setEmail($account->getEmail());
        $user->setUuid($account->getUuid());
        $user->setName($account->getFullName());
        $user->setConfirmedAt($account->getConfirmedAt());
        $user->setCreatedAt($account->getCreatedAt());

        return $user;
    }

    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(
                sprintf('Instances of "%s" are not supported.', get_class($user))
            );
        }

        return $user;
    }

    public function supportsClass($class)
    {
        return User::class === $class;
    }
}
