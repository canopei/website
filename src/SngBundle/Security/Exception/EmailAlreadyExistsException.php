<?php

namespace SngBundle\Security\Exception;

use Symfony\Component\Security\Core\Exception\AuthenticationException;

class EmailAlreadyExistsException extends AuthenticationException
{
}
