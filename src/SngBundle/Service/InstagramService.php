<?php

namespace SngBundle\Service;

use Sng\ClientInterface;
use Sng\Exception\NotFoundException;
use Symfony\Component\Cache\Adapter\AdapterInterface;

class InstagramService
{
    protected $cache;
    protected $sngClient;

    const DEFAULT_LIMIT = 10;
    const TYPE_IMAGE = 'image';

    const USER_MEDIA_CACHE_KEY = 'instagram.user.media.';
    const USER_MEDIA_CACHE_TTL = 600;

    public function __construct(ClientInterface $sngClient, AdapterInterface $cache)
    {
        $this->sngClient = $sngClient;
        $this->cache = $cache;
    }

    /**
     * @param $username
     * @param int $limit
     * @param bool $useCache
     * @return mixed
     *
     * @throws NotFoundException
     * @throws \UnexpectedValueException
     */
    public function getUserMedia($username, $limit = self::DEFAULT_LIMIT, $useCache = false)
    {
        $cacheKey = self::USER_MEDIA_CACHE_KEY.md5(serialize([$username, $limit, $useCache]));
        $cacheItems = $this->cache->getItem($cacheKey);
        if (!$useCache || !$cacheItems->isHit()) {
            $items = $this->sngClient->Account()->getInstagramItemsForUsername($username, $limit);

            if (empty($items)) {
                return [];
            }

            $cacheItems->set($items);
            if ($useCache) {
                $cacheItems->expiresAfter(self::USER_MEDIA_CACHE_TTL);
                $this->cache->save($cacheItems);
            }
        }

        return $cacheItems->get();
    }
}