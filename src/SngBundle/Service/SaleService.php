<?php

namespace SngBundle\Service;

use Sng\ClientInterface;
use Sng\Model\Sale\Membership;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use SngBundle\Controller\PurchaseController;

class SaleService
{
    const MEMBERSHIPS_AVAILABLE_CACHE_KEY = 'sale.memberships.';
    const MEMBERSHIPS_AVAILABLE_CACHE_TTL = 10800; // 3 hours

    const MB_SITE_SERVICES_KEY = 'sale.mb.site_services.';
    const MB_SITE_SERVICES_TTL = 43200; // 12 hours

    const MB_CLASS_SERVICES_KEY = 'sale.mb.class_services.';
    const MB_CLASS_SERVICES_TTL = 3600; // 1 hour

    const SNG_PM_VALIDATOR = "#^SNG[^a-z0-9]+(.+)$#i";

    private $sngClient;
    private $cache;

    public function __construct(
        ClientInterface $sngClient,
        AdapterInterface $cache
    )
    {
        $this->sngClient = $sngClient;
        $this->cache = $cache;
    }

    public function getAvailableMemberships(array $uuids = [], $useCache = false)
    {
        $cacheKey = self::MEMBERSHIPS_AVAILABLE_CACHE_KEY.md5(serialize([$uuids, $useCache]));
        $cacheItem = $this->cache->getItem($cacheKey);

        if (!$useCache || !$cacheItem->isHit()) {
            $memberships = $this->sngClient->Sale()->getAvailableMemberships();

            $cacheItem->set($memberships);
            if ($useCache) {
                $cacheItem->expiresAfter(self::MEMBERSHIPS_AVAILABLE_CACHE_TTL);
                $this->cache->save($cacheItem);
            }
        }

        return $cacheItem->get();
    }

    public function getMembershipByUuid($uuid)
    {
        $memberships = $this->getAvailableMemberships([], true);

        /** @var Membership $memb */
        foreach ($memberships as $memb) {
            if ($memb->getUuid() === $uuid) {
                return $memb;
            }
        }

        return false;
    }

    public function getSngSaleServicesForClass($classUuid, $useCache = true)
    {
        $cacheKey = self::MB_CLASS_SERVICES_KEY . md5(serialize([$classUuid, $useCache]));
        $cacheItem = $this->cache->getItem($cacheKey);

        if (!$useCache || !$cacheItem->isHit()) {
            $allServices = $this->sngClient->Sale()->getSaleServicesForClass($classUuid);

            // filter out non-sng services
            $sngServices = [];
            foreach ($allServices as $service) {
                $serviceName = $service->getName();

                // filter out global drop in pricing option
                if (in_array(strtolower(trim($serviceName)), PurchaseController::SNG_DROPIN_PRICING_NAMES)) {
                    continue;
                }
                if (!preg_match(self::SNG_PM_VALIDATOR, $serviceName)) {
                    continue;
                }

                $service->setName(preg_replace(self::SNG_PM_VALIDATOR, "$1", $serviceName));

                $sngServices[] = $service;
            }

            $cacheItem->set($sngServices);
            if (count($sngServices) && $useCache) {
                $cacheItem->expiresAfter(self::MB_CLASS_SERVICES_TTL);
                $this->cache->save($cacheItem);
            }
        }

        return $cacheItem->get();
    }

    public function getSngSaleServicesForSite($siteUuid, $useCache = true)
    {
        $cacheKey = self::MB_SITE_SERVICES_KEY . md5(serialize([$siteUuid, $useCache]));
        $cacheItem = $this->cache->getItem($cacheKey);

        if (!$useCache || !$cacheItem->isHit()) {
            $allServices = $this->sngClient->Sale()->getSaleServicesForSite($siteUuid);

            // filter out non-sng services
            $sngServices = [];
            foreach ($allServices as $service) {
                $serviceName = $service->getName();

                // filter out global drop in pricing option
                if (in_array(strtolower(trim($serviceName)), PurchaseController::SNG_DROPIN_PRICING_NAMES)) {
                    continue;
                }
                if (!preg_match(self::SNG_PM_VALIDATOR, $serviceName)) {
                    continue;
                }

                $service->setName(preg_replace(self::SNG_PM_VALIDATOR, "$1", $serviceName));

                $sngServices[] = $service;
            }

            $cacheItem->set($sngServices);
            if (count($sngServices) && $useCache) {
                $cacheItem->expiresAfter(self::MB_SITE_SERVICES_TTL);
                $this->cache->save($cacheItem);
            }
        }

        return $cacheItem->get();
    }
}