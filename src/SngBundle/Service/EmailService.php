<?php

namespace SngBundle\Service;

use Sng\Model\Account\Account;
use Symfony\Component\Templating\EngineInterface;

class EmailService
{
    private $mailer;
    private $templating;
    private $locationsService;
    private $staffService;

    public function __construct(
        \Swift_Mailer $mailer,
        EngineInterface $templating,
        LocationsService $locationsService,
        StaffService $staffService
    ) {
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->locationsService = $locationsService;
        $this->staffService = $staffService;
    }

    public function sendWelcomeEmail(Account $account)
    {
        $cities = $this->locationsService->getAllCities();
        $topStaffData = $this->staffService->getActiveStaff([], 3);
        $topLocations = $this->locationsService->getFeaturedLocations([], 3);

        $params = [
            'account' => $account,
            'cities' => array_slice($cities, 0, 3),
            'teachers' => $topStaffData['staff'],
            'studios' => $topLocations,
            'emailType' => 'welcome'
        ];
        $message = (new \Swift_Message('Welcome to SweatNGlow'))
            ->setFrom('noreply@sweatnglow.com')
            ->setTo($account->getEmail())
            ->setBody(
                $this->templating->render('SngBundle:Emails:Account/welcome.html.twig', $params),
                'text/html'
            )
            ->addPart(
                $this->templating->render('SngBundle:Emails:Account/welcome.txt.twig', $params),
                'text/plain'
            );

        return $this->mailer->send($message);
    }

    public function sendConfirmationEmail(Account $account)
    {
        $params = [
            'account' => $account,
            'emailType' => 'confirmation_email'
        ];

        $message = (new \Swift_Message('Confirm your SweatNGlow account'))
            ->setFrom('noreply@sweatnglow.com')
            ->setTo($account->getEmail())
            ->setBody(
                $this->templating->render('SngBundle:Emails:Account/confirmation.html.twig', $params),
                'text/html'
            )
            ->addPart(
                $this->templating->render('SngBundle:Emails:Account/confirmation.txt.twig', $params),
                'text/plain'
            );

        return $this->mailer->send($message);
    }

    public function sendPasswordChangeEmail(Account $account, $code)
    {
        $params = [
            'account' => $account,
            'code' => $code,
            'emailType' => 'password_reset'
        ];

        $message = (new \Swift_Message('Change your SweatNGlow password'))
            ->setFrom('noreply@sweatnglow.com')
            ->setTo($account->getEmail())
            ->setBody(
                $this->templating->render('SngBundle:Emails:Account/password_change.html.twig', $params),
                'text/html'
            )
            ->addPart(
                $this->templating->render('SngBundle:Emails:Account/password_change.txt.twig', $params),
                'text/plain'
            );

        return $this->mailer->send($message);
    }
}