<?php

namespace SngBundle\Service;

use Sng\ClientInterface;
use Sng\Model\Account\Client;
use SngBundle\Security\User\User;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use SngBundle\Service\SaleService;

use SngBundle\Controller\PurchaseController;

class AccountsService extends \Twig_Extension
{
    const ACCOUNT_SERVICES_CACHE_KEY = 'account.services.';
    const ACCOUNT_SERVICES_CACHE_TTL = 600;

    const ACCOUNT_SCHEDULE_CACHE_KEY = 'account.schedule.';
    const ACCOUNT_SCHEDULE_CACHE_TTL = 600;

    const ACCOUNT_VISITS_CACHE_KEY = 'account.visits.';
    const ACCOUNT_VISITS_CACHE_TTL = 600;

    private $sngClient;
    private $cache;

    public function __construct(
        ClientInterface $sngClient,
        AdapterInterface $cache
    ) {
        $this->sngClient = $sngClient;
        $this->cache = $cache;
    }

    public function getAccountServices($accountUuid, $activeOnly = false, $useCache = false, $stripDropIns = false, $stripSngPrefix = true)
    {
        $cacheKey = $this->getAccountServiceCacheKey([$accountUuid, $activeOnly, $stripDropIns, $stripSngPrefix, $useCache]);
        $cache = $this->cache->getItem($cacheKey);

//        if (!$useCache || !$cache->isHit()) {
            $services = $this->sngClient->Account()->getAccountServices($accountUuid, $activeOnly);
            $memberships = $this->sngClient->Account()->getAccountMemberships($accountUuid);

            $allServices = $memberships;
            foreach ($services as $service) {
                if ($stripDropIns && in_array(strtolower(trim($service->getName())), PurchaseController::SNG_DROPIN_PRICING_NAMES)) {
                    continue;
                }
                if ($stripSngPrefix) {
                    $service->setName(preg_replace(SaleService::SNG_PM_VALIDATOR, "$1", $service->getName()));
                }

                $allServices[] = $service;
            }

            $cache->set($allServices);
            if ($useCache) {
                $cache->expiresAfter(self::ACCOUNT_SERVICES_CACHE_TTL);
                $this->cache->save($cache);
            }
//        }

        return $cache->get();
    }

    public function getAccountVisits($accountUuid, $limit = 15, $offset = 0, $useCache = false)
    {
        $cacheKey = $this->getAccountVisitsCacheKey([$accountUuid, $limit, $offset, $useCache]);
        $cache = $this->cache->getItem($cacheKey);

        if (!$useCache || !$cache->isHit()) {
            $visits = $this->sngClient->Account()->getAccountVisitsHistory($accountUuid, $limit, $offset);

            $cache->set($visits);
            if ($useCache) {
                $cache->expiresAfter(self::ACCOUNT_VISITS_CACHE_TTL);
                $this->cache->save($cache);
            }
        }

        return $cache->get();
    }

    public function getAccountSchedule($accountUuid, $endDate = null, $useCache = false)
    {
        $cacheKey = $this->getAccountScheduleCacheKey([$accountUuid, $useCache]);
        $cache = $this->cache->getItem($cacheKey);

        if (!$useCache || !$cache->isHit()) {
            $schedule = $this->sngClient->Account()->getAccountSchedule($accountUuid, $endDate);

            $cache->set($schedule);
            if ($useCache) {
                $cache->expiresAfter(self::ACCOUNT_SCHEDULE_CACHE_TTL);
                $this->cache->save($cache);
            }
        }

        return $cache->get();
    }

    public function invalidateAccountServicesCache($accountUuid)
    {
        // delete both for active only and for the other version
        $cacheKey = $this->getAccountServiceCacheKey([$accountUuid, false, true, true, true]);
        $this->cache->deleteItem($cacheKey);

        $cacheKey = $this->getAccountServiceCacheKey([$accountUuid, false, false, true, true]);
        $this->cache->deleteItem($cacheKey);
    }

    public function invalidateAccountScheduleCache($accountUuid)
    {
        $cacheKey = $this->getAccountScheduleCacheKey([$accountUuid, true]);
        $this->cache->deleteItem($cacheKey);

        $cacheKey = $this->getAccountScheduleCacheKey([$accountUuid, false]);
        $this->cache->deleteItem($cacheKey);
    }

    public function createClientForUser($siteMbId, User $user)
    {
        // prepare the Client object
        // split the full name into first and last names
        $nameFragments = explode(" ", $user->getName());
        $firstName = count($nameFragments) > 2 ? $user->getName() : $nameFragments[0];
        $lastName = count($nameFragments) == 2 ? $nameFragments[1] : '';


        $newClient = new Client();
        $newClient->setEmail($user->getEmail())
            ->setFirstName($firstName)
            ->setMiddleName(' ')
            ->setLastName($lastName)
            ->setCity('-')
            ->setState('-')
            ->setGender(0)
            ->setEmailOptIn(true)
            ->setIsCompany(false)
        ;
        /** @var Client $newClient */
        return $this->sngClient->Account()->createClient($siteMbId, $newClient);
    }

    protected function getAccountServiceCacheKey($context)
    {
        return self::ACCOUNT_SERVICES_CACHE_KEY.md5(serialize($context));
    }

    protected function getAccountVisitsCacheKey($context)
    {
        return self::ACCOUNT_VISITS_CACHE_KEY.md5(serialize($context));
    }

    protected function getAccountScheduleCacheKey($context)
    {
        return self::ACCOUNT_SCHEDULE_CACHE_KEY . md5(serialize($context));
    }
}