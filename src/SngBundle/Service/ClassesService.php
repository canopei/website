<?php

namespace SngBundle\Service;

use Psr\Log\LoggerInterface;
use Sng\ClientInterface;
use Elastica\SearchableInterface;
use Sng\Model\Account\Visit;
use Sng\Model\Account\Service;
use Sng\Model\Account\AccountMembership;
use Sng\Model\ScheduledClass\Program;
use Sng\Model\ScheduledClass\ScheduledClass;
use SngBundle\Security\User\User;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use SngBundle\Controller\PurchaseController;
use Sng\Model\ScheduledClass\Booking;

class ClassesService extends \Twig_Extension
{
    const DEFAULT_LIMIT = 10;

    const CITY_CLASS_CACHE_KEY = 'city.class.';
    const CITY_CLASS_CACHE_TTL = 600;

    const STAFF_CLASS_CACHE_KEY = 'staff.class.';
    const STAFF_CLASS_CACHE_TTL = 300;

    const LOCATION_CLASS_CACHE_KEY = 'location.class.';
    const LOCATION_CLASS_CACHE_TTL = 300;

    const SITE_PROGRAMS_CACHE_KEY = 'site.programs.';
    const SITE_PROGRAMS_CACHE_TTL = 86400;

    const FILTER_START_DATETIME_KEY = 'startDatetime';
    const FILTER_END_DATETIME_KEY = 'endDatetime';
    const FILTER_LOCATIONS_KEY = 'location';
    const FILTER_STAFF_KEY = 'staff';
    const FILTER_CITY_KEY = 'city';

    const AGGS_STAFF = 'aggsStaff';
    const AGGS_LOCATION = 'aggsLocation';


    const CLASS_STATUS_AVAILABLE = 0;
    const CLASS_STATUS_CANCELLED = 1;
    const CLASS_STATUS_FULLY_BOOKED = 2;
    const CLASS_STATUS_WAITLIST = 3;
    const CLASS_STATUS_BOOKED_BY_USER = 4;

    private $classStatusIdToText = [
        self::CLASS_STATUS_AVAILABLE=>'Available',
        self::CLASS_STATUS_CANCELLED=>'Cancelled',
        self::CLASS_STATUS_FULLY_BOOKED=>'Fully booked',
        self::CLASS_STATUS_WAITLIST=>'Waitlist',
        self::CLASS_STATUS_BOOKED_BY_USER=>'Booked',
    ];

    private $sngClient;
    private $esClassFinder;
    private $accountsService;
    private $cache;
    private $logger;

    private $usersScheduleCache;

    public function __construct(
        ClientInterface $sngClient,
        SearchableInterface $esClassFinder,
        AccountsService $accountsService,
        AdapterInterface $cache,
        LoggerInterface $logger
    ) {
        $this->sngClient = $sngClient;
        $this->esClassFinder = $esClassFinder;
        $this->accountsService = $accountsService;
        $this->cache = $cache;
        $this->logger = $logger;

        $this->usersScheduleCache = [];
    }

    public function getClasses($filters = [], $sort = [], $limit = self::DEFAULT_LIMIT, $offset = 0, $useCache = false)
    {
        $cacheKey = self::STAFF_CLASS_CACHE_KEY.md5(serialize([$filters, $sort, $limit, $offset, $useCache]));
        $cache = $this->cache->getItem($cacheKey);

        if (!$useCache || !$cache->isHit()) {
            $mainQuery = new \Elastica\Query\BoolQuery();

            if (isset($filters[self::FILTER_STAFF_KEY])) {
                $staffUuidQuery = new \Elastica\Query\Terms();
                $staffUuidQuery->setTerms('staff.uuid', $filters[self::FILTER_STAFF_KEY]);
                $mainQuery->addMust($staffUuidQuery);
            }

            if (isset($filters[self::FILTER_LOCATIONS_KEY])) {
                $locationUuidQuery = new \Elastica\Query\Terms();
                $locationUuidQuery->setTerms('location.uuid', $filters[self::FILTER_LOCATIONS_KEY]);
                $mainQuery->addMust($locationUuidQuery);
            }

            if (isset($filters[self::FILTER_CITY_KEY])) {
                $cityQuery = new \Elastica\Query\Terms();
                $cityQuery->setTerms('cityNA', $filters[self::FILTER_CITY_KEY]);
                $mainQuery->addMust($cityQuery);
            }

            // Date interval filter
            $now = new \DateTime();
            $end = clone $now;
            $end->add(new \DateInterval('P7D'));

            $startDatetime = isset($filters[self::FILTER_START_DATETIME_KEY]) ? $filters[self::FILTER_START_DATETIME_KEY] : $now->format('Y-m-d H:i:s');
            $endDatetime = isset($filters[self::FILTER_END_DATETIME_KEY]) ? $filters[self::FILTER_END_DATETIME_KEY] : $end->format('Y-m-d H:i:s');

            $startTimeQuery = new \Elastica\Query\Range();
            $startTimeQuery->addField('startDatetime', array('gte' => $startDatetime, 'lt' => $endDatetime));
            $mainQuery->addMust($startTimeQuery);
            // end date interval filter

            $activeQuery = new \Elastica\Query\Terms();
            $activeQuery->setTerms('active', array(true));
            $mainQuery->addMust($activeQuery);

             $isAvailableQuery = new \Elastica\Query\Terms();
             $isAvailableQuery->setTerms('isAvailable', array(true));
             $mainQuery->addMust($isAvailableQuery);

            $query = new \Elastica\Query($mainQuery);
            $query
                ->setFrom($offset)
                ->setSize($limit);

            foreach ($sort as $field => $dir) {
                $query->setSort([$field => $dir]);
            }

            $esResult = $this->esClassFinder->search($query);

            $classUuids = array_map(create_function('$o', 'return $o->getId();'), $esResult->getResults());

            // Batch this
            $classes = [];
            $batches = array_chunk($classUuids, 20);
            foreach ($batches as $batch) {
                $respClasses = $this->sngClient->ScheduledClass()->GetClassesByUUIDs($batch);
                $classes = array_merge($classes, $respClasses);
            }

            if (empty($classes)) {
                return [];
            }

            $cache->set($classes);
            if ($useCache) {
                $cache->expiresAfter(self::STAFF_CLASS_CACHE_TTL);
                $this->cache->save($cache);
            }
        }

        return $cache->get();
    }

    public function getNextClasses($filters = [], $aggs = '', $limit = 4)
    {
        $mainQuery = new \Elastica\Query\BoolQuery();

        if (isset($filters[self::FILTER_LOCATIONS_KEY])) {
            $locationUuidsQuery = new \Elastica\Query\Terms();
            $locationUuidsQuery->setTerms('location.uuid', $filters[self::FILTER_LOCATIONS_KEY]);
            $mainQuery->addMust($locationUuidsQuery);
        }

        if (isset($filters[self::FILTER_STAFF_KEY])) {
            $staffUuidQuery = new \Elastica\Query\Terms();
            $staffUuidQuery->setTerms('staff.uuid', $filters[self::FILTER_STAFF_KEY]);
            $mainQuery->addMust($staffUuidQuery);
        }

        $startTimeQuery = new \Elastica\Query\Range();
        $startTimeQuery->addField('startDatetime', array('gte' => 'now', 'lt' => 'now+7d'));
        $mainQuery->addMust($startTimeQuery);

        $activeQuery = new \Elastica\Query\Terms();
        $activeQuery->setTerms('active', array(true));
        $mainQuery->addMust($activeQuery);

        // TODO: commented this because the sandbox classes are somehow all unavailable
        // $isAvailableQuery = new \Elastica\Query\Terms();
        // $isAvailableQuery->setTerms('isAvailable', array(true));
        // $mainQuery->addMust($isAvailableQuery);

        switch ($aggs) {
            case self::AGGS_STAFF:
                $mainAgg = new \Elastica\Aggregation\Terms('next-class');
                $mainAgg->setField('staff.uuid');
                $mainAgg->setSize(99);
                break;
            case self::AGGS_LOCATION:
                $mainAgg = new \Elastica\Aggregation\Terms('next-class');
                $mainAgg->setField('location.uuid');
                $mainAgg->setSize(99);
                break;
            default:
                throw new \UnexpectedValueException("Invalid aggregation parameter.");
        }

        $topAgg = new \Elastica\Aggregation\TopHits('next-class_hits');
        $topAgg->setSort(["startDatetime" => ["order" => "asc"]]);
        $topAgg->setSize($limit);
        $mainAgg->addAggregation($topAgg);

        $query = new \Elastica\Query($mainQuery);

        $query
            ->addAggregation($mainAgg)
            ->setSize(0);

        $esResult = $this->esClassFinder->search($query);
        $aggData = $esResult->getAggregation('next-class');

        $classUuids = [];
        foreach ($aggData['buckets'] as $aggBucket) {
            $hits = $aggBucket['next-class_hits']['hits']['hits'];
            foreach ($hits as $hit) {
                $classUuids[] = $hit['_id'];
            }
        }

        $mixedClasses = [];
        $batches = array_chunk($classUuids, 20);
        foreach ($batches as $batch) {
            $respClasses = $this->sngClient->ScheduledClass()->GetClassesByUUIDs($batch);
            $mixedClasses = array_merge($mixedClasses, $respClasses);
        }

        $result = [];
        switch ($aggs) {
            case self::AGGS_STAFF:
                foreach ($mixedClasses as $class) {
                    if (!isset($result[$class->getStaff()->getUuid()])) {
                        $result[$class->getStaff()->getUuid()] = [];
                    }
                    $result[$class->getStaff()->getUuid()][] = $class;
                }
                break;
            case self::AGGS_LOCATION:
                foreach ($mixedClasses as $class) {
                    if (!isset($result[$class->getLocation()->getUuid()])) {
                        $result[$class->getLocation()->getUuid()] = [];
                    }
                    $result[$class->getLocation()->getUuid()][] = $class;
                }
                break;
            default:
                throw new \UnexpectedValueException("Invalid aggregation parameter.");
        }

        return $result;
    }

    public function isClassBookedByUser($classUuid, $accountUuid)
    {
        if (!isset($this->usersScheduleCache[$accountUuid])) {
            $this->usersScheduleCache[$accountUuid] = $this->accountsService->getAccountSchedule($accountUuid);
        }

        /** @var Visit $visit */
        foreach ($this->usersScheduleCache[$accountUuid] as $visit) {
            if ($visit->getClassUuid() === $classUuid) {
                return true;
            }
        }

        return false;
    }

    public function getStatusForUser(ScheduledClass $class, $user)
    {
        if ($class->getIsCanceled()) {
            return self::CLASS_STATUS_CANCELLED;
        }

        if ($user instanceof User && $this->isClassBookedByUser($class->getUuid(), $user->getUuid())) {
            return self::CLASS_STATUS_BOOKED_BY_USER;
        }

        if ($class->getWebCapacity() > 0) {
            if($class->getWebCapacity() <= $class->getWebBooked()){
                if ($class->getIsWaitlistAvailable()) {
                    return self::CLASS_STATUS_WAITLIST;
                } else {
                    return self::CLASS_STATUS_FULLY_BOOKED;
                }
            }
            return self::CLASS_STATUS_AVAILABLE;
        }

        if ($class->getMaxCapacity() > 0 && $class->getMaxCapacity() <= $class->getTotalBooked()) {
            if ($class->getIsWaitlistAvailable()) {
                return self::CLASS_STATUS_WAITLIST;
            } else {
                return self::CLASS_STATUS_FULLY_BOOKED;
            }
        }

        return self::CLASS_STATUS_AVAILABLE;
    }

    public function getTextStatusForUser($class, $user){
        $id = $this->getStatusForUser($class, $user);
        return isset($this->classStatusIdToText[$id]) ? $this->classStatusIdToText[$id] : '';
    }

    public function getFunctions()
    {
        return array(
            new \Twig_Function('getStatusForUser', [$this, 'getStatusForUser']),
            new \Twig_Function('getTextStatusForUser', [$this, 'getTextStatusForUser']),
            new \Twig_Function('isClassBookedByUser', [$this, 'isClassBookedByUser']),
        );
    }

    public function addAccountToClass(User $user, ScheduledClass $class, $serviceId, $membershipId)
    {
        $booking = new Booking();

        $classUuid = $class->getUuid();

        if (!empty($serviceId)) {
            $this->sngClient->ScheduledClass()->addAccountToClass($user->getUuid(), $classUuid, $serviceId);

            $booking->setServiceId($serviceId);
        } else if (!empty($membershipId)) {
            // we can pay with memberships only if the class program has a dropin service ID
            if ($class->getProgramDropinServiceMbId() == 0) {
                $this->logger->error('[addAccountToClass] Cannot find the dropin service MB ID.');
                return false;
            }

            // fetch the services
            $accountServices = $this->accountsService->getAccountServices($user->getUuid());

            // get the account membership
            /** @var AccountMembership $accountMembership */
            $accountMembership = null;
            foreach ($accountServices as $accountService) {
                if ($accountService instanceof AccountMembership && $accountService->getId() == $membershipId) {
                    $accountMembership = $accountService;
                }
            }

            if (empty($accountMembership)) {
                $this->logger->error('[addAccountToClass] Cannot find the account membership.');
                return false;
            }

            // validation checks
            if ($accountMembership->getRemaining() === 0 ||
                ($accountMembership->getRemaining() == -1 && strtotime($accountMembership->getValidUntil()) < time())) {
                return false;
            }

            // Create the dummy sale in MB
            try {
                $this->sngClient->Sale()->createSimpleSale(
                    $user->getUuid(),
                    $class->getLocation()->getSiteUuid(),
                    $class->getProgramDropinServiceMbId(),
                    $class->getUuid()
                );
            } catch (\InvalidArgumentException $e) {
                $this->logger->error('[addAccountToClass] Exception while creating the sale in MB: ' . $e->getMessage());
                return false;
            }

            // Refetch the services to find the one we just bought
            $lastServiceID = 0;
            $accountServices = $this->accountsService->getAccountServices($user->getUuid(), false, false, false, false);
            foreach ($accountServices as $accountService) {
                if ($accountService instanceof Service
                    && in_array(strtolower(trim($accountService->getName())), PurchaseController::SNG_DROPIN_PRICING_NAMES)
                    && $accountService->getMbId() > $lastServiceID) {
                    $lastServiceID = $accountService->getMbId();
                }
            }
            if (empty($lastServiceID)) {
                $this->logger->error('[addAccountToClass] Failed to fetch the last service bought.');
                return false;
            }

            if ($accountMembership->getRemaining() > 0) {
                $accountMembership->setRemaining($accountMembership->getRemaining() - 1);
                $this->sngClient->Account()->updateAccountMembership($accountMembership);
            }

            // Save the booking info to DB
            $booking->setAccountMembershipId($accountMembership->getId());
            $booking->setMembershipServiceId($lastServiceID);
        }

        $this->sngClient->ScheduledClass()->createBooking($user->getUuid(), $class->getUuid(), $booking);

        $this->accountsService->invalidateAccountScheduleCache($user->getUuid());
        $this->accountsService->invalidateAccountServicesCache($user->getUuid());

        return true;
    }

    public function getProgramsBySiteUuid($siteUuid)
    {
        if (empty($siteUuid)) {
            return [];
        }

        $cacheKey = self::SITE_PROGRAMS_CACHE_KEY . $siteUuid;
        $cache = $this->cache->getItem($cacheKey);
        if (!$cache->isHit()) {
            $programs = $this->sngClient->ScheduledClass()->getProgramsBySiteUuid($siteUuid);

            $cache->set($programs);
            $cache->expiresAfter(self::SITE_PROGRAMS_CACHE_TTL);
            $this->cache->save($cache);
        }

        return $cache->get();
    }

    public function hasSiteGlobalDropInPlan($siteUuid)
    {
        $programs = $this->getProgramsBySiteUuid($siteUuid);
        /** @var Program $program */
        foreach ($programs as $program) {
            if ($program->getDropinServiceMbId() <= 0) {
                return false;
            }
        }

        return true;
    }
}
