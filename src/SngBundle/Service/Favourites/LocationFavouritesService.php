<?php

namespace SngBundle\Service\Favourites;

class LocationFavouritesService extends AbstractFavouritesService
{
    protected function getType()
    {
        return 'locations';
    }

    protected function getUuids($accountUuid)
    {
        return $this->sngClient->Site()->getLocationFavouritesForAccount($accountUuid);
    }

    protected function getDetailed($uuids)
    {
        return $this->sngClient->Site()->getLocations($uuids);
    }

    protected function add($accountUuid, $itemUuid)
    {
        try {
            $this->sngClient->Site()->addLocationToFavourites($accountUuid, $itemUuid);

            return true;
        } catch(\Exception $e) {
            return false;
        }
    }

    protected function remove($accountUuid, $itemUuid)
    {
        try {
            $this->sngClient->Site()->removeLocationFromFavourites($accountUuid, $itemUuid);

            return true;
        } catch(\Exception $e) {
            return false;
        }
    }

    public function getFunctions()
    {
        return array(
            new \Twig_Function('isFavouriteLocation', [$this, 'isItemFavourite']),
        );
    }
}