<?php

namespace SngBundle\Service\Favourites;

use Sng\ClientInterface;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\HttpKernel\Event\PostResponseEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Guard\Token\PostAuthenticationGuardToken;

abstract class AbstractFavouritesService extends \Twig_Extension implements FavouritesServiceInterface
{
    protected $sngClient;
    protected $cache;
    protected $tokenStorage;
    protected $useCache;

    private $uuids;
    private $isChanged;

    const CACHE_UUIDS_KEY = 'favourites.uuids.%s.%s';
    const CACHE_UUIDS_TTL = 600;

    const CACHE_DETAILED_KEY = 'favourites.detailed.%s.%s';
    const CACHE_DETAILED_TTL = 600;

    public function __construct(
        ClientInterface $sngClient,
        AdapterInterface $cache,
        TokenStorageInterface $tokenStorage
    )
    {
        $this->sngClient = $sngClient;
        $this->cache = $cache;
        $this->tokenStorage = $tokenStorage;

        $this->isChanged = false;
        $this->useCache = true;
    }

    private function getUser()
    {
        $token = $this->tokenStorage->getToken();
        if (!$token instanceof PostAuthenticationGuardToken) {
            return null;
        }

        return $token->getUser();
    }

    protected function loadUuids()
    {
        if (is_null($this->uuids)) {
            $userUuid = $this->getUser()->getUuid();

            $cacheKey = $this->getUuidsCacheKey($userUuid);
            $cacheItem = $this->cache->getItem($cacheKey);
            if (!$cacheItem->isHit() && $this->useCache) {
                $this->uuids = $this->getUuids($userUuid);

                $cacheItem->set($this->uuids);
                $cacheItem->expiresAfter(self::CACHE_UUIDS_TTL);
                $this->cache->save($cacheItem);
            }

            $this->uuids = $cacheItem->get();
        }

        return $this->uuids;
    }

    public function getItems()
    {
        $user = $this->getUser();
        if (!$user) {
            return [];
        }

        $this->loadUuids();

        $cacheKey = $this->getDetailedCacheKey($user->getUuid());
        $cacheItem = $this->cache->getItem($cacheKey);
        if (!$cacheItem->isHit() && $this->useCache) {
            $detailed = $this->getDetailed($this->uuids);

            $cacheItem->set($detailed);
            $cacheItem->expiresAfter(self::CACHE_DETAILED_TTL);
            $this->cache->save($cacheItem);
        }

        return $cacheItem->get();
    }

    public function addItem($uuid)
    {
        $user = $this->getUser();
        if (!$user) {
            return false;
        }

        $this->loadUuids();
        $ok = true;
        if (!in_array($uuid, $this->uuids)) {
            $ok = $this->add($user->getUuid(), $uuid);
            if ($ok) {
                $this->uuids[] = $uuid;
                $this->isChanged = true;
            }
        }

        return $ok;
    }

    public function removeItem($uuid)
    {
        $user = $this->getUser();
        if (!$user) {
            return false;
        }

        $this->loadUuids();
        $ok = true;
        if (in_array($uuid, $this->uuids)) {
            $ok = $this->remove($user->getUuid(), $uuid);
            if ($ok) {
                $index = array_search($uuid, $this->uuids);
                if ($index !== false) {
                    unset($this->uuids[$index]);
                    // normalize the keys
                    $this->uuids = array_values($this->uuids);
                    $this->isChanged = true;
                }
            }
        }

        return $ok;
    }

    public function isItemFavourite($uuid)
    {
        $user = $this->getUser();
        if (!$user) {
            return false;
        }

        $this->loadUuids();

        return in_array($uuid, $this->uuids);
    }

    public function onKernelTerminate(PostResponseEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        $user = $this->getUser();
        if (!$user) {
            return false;
        }

        file_put_contents("/tmp/test", serialize($this->uuids));

        if ($this->isChanged) {
            // Update the UUIDs key
            $cacheKey = $this->getUuidsCacheKey($user->getUuid());
            $cacheItem = $this->cache->getItem($cacheKey);
            // We'll keep the remaining TTL - we won't extend it
            $cacheItem->set($this->uuids);
            $this->cache->save($cacheItem);

            // Invalidate the Detailed key
            $this->cache->deleteItem($this->getDetailedCacheKey($user->getUuid()));
        }
    }

    private function getUuidsCacheKey($userUuid)
    {
        return sprintf(self::CACHE_UUIDS_KEY, $this->getType(), $userUuid);
    }

    private function getDetailedCacheKey($userUuid)
    {
        return sprintf(self::CACHE_DETAILED_KEY, $this->getType(), $userUuid);
    }

    abstract protected function getType();
    abstract protected function getUuids($accountUuid);
    abstract protected function getDetailed($uuids);
    abstract protected function add($accountUuid, $itemUuid);
    abstract protected function remove($accountUuid, $itemUuid);
}