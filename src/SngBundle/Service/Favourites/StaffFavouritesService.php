<?php

namespace SngBundle\Service\Favourites;

use Sng\ClientInterface;
use Sng\Model\Staff\Staff;
use SngBundle\Service\ClassesService;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class StaffFavouritesService extends AbstractFavouritesService
{
    protected $classesService;

    public function __construct(
        ClientInterface $sngClient,
        AdapterInterface $cache,
        TokenStorageInterface $tokenStorage,
        ClassesService $classesService
    ) {
        parent::__construct($sngClient, $cache, $tokenStorage);

        $this->classesService = $classesService;
    }

    protected function getType()
    {
        return 'staff';
    }

    protected function getUuids($accountUuid)
    {
        return $this->sngClient->Staff()->getStaffFavouritesForAccount($accountUuid);
    }

    protected function getDetailed($uuids)
    {
        $detailedStaff = [];

        $batches = array_chunk($uuids, 20);
        foreach ($batches as $batch) {
            $staff = $this->sngClient->Staff()->getStaffByUUIDs($batch);

            $nextClasses = $this->classesService->getNextClasses([
                ClassesService::FILTER_STAFF_KEY => $batch,
            ], ClassesService::AGGS_STAFF, 1);

            /** @var Staff $oneStaff */
            foreach ($staff as $oneStaff) {
                if (isset($nextClasses[$oneStaff->getUuid()])) {
                    $oneStaff->setNextClasses($nextClasses[$oneStaff->getUuid()]);
                }
            }

            $detailedStaff = array_merge($detailedStaff, $staff);
        }

        return $detailedStaff;
    }

    protected function add($accountUuid, $itemUuid)
    {
        try {
            $this->sngClient->Staff()->addStaffToFavourites($accountUuid, $itemUuid);

            return true;
        } catch(\Exception $e) {
            return false;
        }
    }

    protected function remove($accountUuid, $itemUuid)
    {
        try {
            $this->sngClient->Staff()->removeStaffFromFavourites($accountUuid, $itemUuid);

            return true;
        } catch(\Exception $e) {
            return false;
        }
    }

    public function getFunctions()
    {
        return array(
            new \Twig_Function('isFavouriteStaff', [$this, 'isItemFavourite']),
        );
    }
}