<?php

namespace SngBundle\Service\Favourites;

interface FavouritesServiceInterface
{
    public function getItems();
    public function addItem($uuid);
    public function removeItem($uuid);
    public function isItemFavourite($uuid);
}