<?php

namespace SngBundle\Service;

use Sng\ClientInterface;
use Elastica\SearchableInterface;
use Sng\Exception\NotFoundException;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use SngBundle\Elastica\Query\ParentId;
use Symfony\Component\VarDumper\Caster\ClassStub;

class StaffService
{
    const DEFAULT_LIMIT = 10;

    const STAFF_PROFILE_CACHE_KEY = 'staff.profile.';
    const STAFF__PROFILE_CACHE_TTL = 300;

    const STAFF_CACHE_KEY = 'city.staff.';
    const STAFF_CACHE_TTL = 600;

    const LOCATION_STAFF_CACHE_KEY = 'location.staff.';
    const LOCATION_STAFF_CACHE_TTL = 600;

    const FILTER_CITY_NAME_KEY = 'cityName';
    const FILTER_INCLUDE_NEXT_CLASSES = 'nextClasses';

    const SORT_FEATURED_KEY = 'featured';

    private $sngClient;
    private $esClassFinder;
    private $esStaffFinder;
    private $classesService;
    private $cache;

    public function __construct(
        ClientInterface $sngClient,
        SearchableInterface $esClassFinder,
        SearchableInterface $esStaffFinder,
        ClassesService $classesService,
        AdapterInterface $cache
    ) {
        $this->sngClient = $sngClient;
        $this->esClassFinder = $esClassFinder;
        $this->esStaffFinder = $esStaffFinder;
        $this->classesService = $classesService;
        $this->cache = $cache;
    }

    public function getActiveStaffByLocation($locationUuid, $sort = [], $limit = self::DEFAULT_LIMIT, $offset = 0)
    {
        $cacheKey = self::LOCATION_STAFF_CACHE_KEY.md5(serialize([$locationUuid, $sort, $limit, $offset]));
        $cacheStaff = $this->cache->getItem($cacheKey);

        if (!$cacheStaff->isHit()) {
            $mainQuery = new \Elastica\Query\BoolQuery();

            $parentIdQuery = new ParentId('class', $locationUuid);
            $mainQuery->addMust($parentIdQuery);

            $startTimeQuery = new \Elastica\Query\Range();
            $startTimeQuery->addField('startDatetime', array('gte' => 'now', 'lt' => 'now+3d/d'));
            $mainQuery->addMust($startTimeQuery);

            $activeQuery = new \Elastica\Query\Terms();
            $activeQuery->setTerms('active', array(true));
            $mainQuery->addMust($activeQuery);

            $isAvailableQuery = new \Elastica\Query\Terms();
            $isAvailableQuery->setTerms('isAvailable', array(true));
            $mainQuery->addMust($isAvailableQuery);

            $staffUuidAgg = new \Elastica\Aggregation\Terms('staff');
            $staffUuidAgg->setField('staff.uuid');
            $staffUuidAgg->setSize($offset + $limit);

            if (isset($sort[self::SORT_FEATURED_KEY])) {
                $featuredAgg = new \Elastica\Aggregation\Avg('avg_featured');
                $featuredAgg->setField('staff.featured');

                $staffUuidAgg->setOrder('avg_featured', $sort[self::SORT_FEATURED_KEY]);
                $staffUuidAgg->addAggregation($featuredAgg);
            }

            $query = new \Elastica\Query($mainQuery);
            $query
                ->addAggregation($staffUuidAgg)
                ->setSize(0);

            foreach ($sort as $field => $dir) {
                $query->setSort([$field => $dir]);
            }

            $esResult = $this->esClassFinder->search($query);
            $aggs = $esResult->getAggregations();

            // skip the offset results
            $esSlicedResult = array_slice($aggs['staff']['buckets'], $offset, $limit);

            $staffUuids = array_map(create_function('$o', 'return $o["key"];'), $esSlicedResult);
            $staff = $this->sngClient->Staff()->GetStaffByUUIDs($staffUuids);

            if (empty($staff)) {
                return [];
            }

            // Fill-in the next classes
            $nextClasses = $this->classesService->getNextClasses([
                ClassesService::FILTER_STAFF_KEY => $staffUuids,
                ClassesService::FILTER_LOCATIONS_KEY => [$locationUuid],
            ], ClassesService::AGGS_STAFF, 1);

            foreach ($staff as $oneStaff) {
                if (isset($nextClasses[$oneStaff->getUuid()])) {
                    $oneStaff->setNextClasses($nextClasses[$oneStaff->getUuid()]);
                }
            }

            $cacheStaff->set($staff);
            $cacheStaff->expiresAfter(self::LOCATION_STAFF_CACHE_TTL);
            $this->cache->save($cacheStaff);
        }

        return $cacheStaff->get();
    }

    public function getActiveStaff($filters = [], $limit = self::DEFAULT_LIMIT, $offset = 0, $useCache = false)
    {
        $cacheKey = self::STAFF_CACHE_KEY.md5(serialize([$filters, $limit, $offset, $useCache]));
        $cacheStaff = $this->cache->getItem($cacheKey);

        if (!$useCache || !$cacheStaff->isHit() || true) {
            $mainQuery = new \Elastica\Query\BoolQuery();

            $startTimeQuery = new \Elastica\Query\Range();
            $startTimeQuery->addField('startDatetime', array('gte' => 'now', 'lt' => 'now+3d/d'));
            $mainQuery->addMust($startTimeQuery);

            $activeQuery = new \Elastica\Query\Terms();
            $activeQuery->setTerms('active', array(true));
            $mainQuery->addMust($activeQuery);

            $isAvailableQuery = new \Elastica\Query\Terms();
            $isAvailableQuery->setTerms('isAvailable', array(true));
            $mainQuery->addMust($isAvailableQuery);

            // Build the has_parent query
            $parentBoolQuery = new \Elastica\Query\BoolQuery();

            if (!empty($filters[self::FILTER_CITY_NAME_KEY])) {
                $cityQuery = new \Elastica\Query\Terms();
                $cityQuery->setTerms('cityNA', [$filters[self::FILTER_CITY_NAME_KEY]]);
                $parentBoolQuery->addMust($cityQuery);
            }

            $hasClassesQuery = new \Elastica\Query\Terms();
            $hasClassesQuery->setTerms('hasClasses', array(true));
            $parentBoolQuery->addMust($hasClassesQuery);

            $parentQuery = new \Elastica\Query\HasParent($parentBoolQuery, 'location');

            $mainQuery->addMust($parentQuery);

            $featuredAgg = new \Elastica\Aggregation\Avg('avg_featured');
            $featuredAgg->setField('staff.featured');

            $staffUuidAgg = new \Elastica\Aggregation\Terms('staff');
            $staffUuidAgg->setField('staff.uuid');
            $staffUuidAgg->setOrder('avg_featured', 'desc');
            $staffUuidAgg->setSize($limit + $offset);
            $staffUuidAgg->addAggregation($featuredAgg);

            $query = new \Elastica\Query($mainQuery);
            $query
                ->setSort(['startDatetime' => 'asc'])
                ->addAggregation($staffUuidAgg)
                ->setSize(0);

            $esResult = $this->esClassFinder->search($query);
            $aggs = $esResult->getAggregations();

            $staffUuids = array_map(create_function('$o', 'return $o["key"];'), $aggs['staff']['buckets']);

            // remove the offset
            $staffUuids = array_slice($staffUuids, $offset);

            $staff = [];
            // Batch this call
            $batches = array_chunk($staffUuids, 20);
            foreach ($batches as $batch) {
                $respStaff = $this->sngClient->Staff()->GetStaffByUUIDs($batch);

                if (isset($filters[self::FILTER_INCLUDE_NEXT_CLASSES])) {
                    // Fill-in the next classes
                    $nextClasses = $this->classesService->getNextClasses([
                        ClassesService::FILTER_STAFF_KEY => $batch,
                    ], ClassesService::AGGS_STAFF, (int) $filters[self::FILTER_INCLUDE_NEXT_CLASSES]);
                    foreach ($respStaff as $oneStaff) {
                        if (isset($nextClasses[$oneStaff->getUuid()])) {
                            $oneStaff->setNextClasses($nextClasses[$oneStaff->getUuid()]);
                        }
                    }
                }

                $staff = array_merge($staff, $respStaff);
            }

            if (empty($staff)) {
                return ['staff' => [], 'total' => $esResult->getTotalHits()];
            }

            $staffData = ['staff' => $staff, 'total' => $esResult->getTotalHits()];

            $cacheStaff->set($staffData);
            if ($useCache) {
                $cacheStaff->expiresAfter(self::STAFF_CACHE_TTL);
                $this->cache->save($cacheStaff);
            }
        }

        return $cacheStaff->get();
    }

    public function getStaff($filters = [], $sort = [], $limit = self::DEFAULT_LIMIT, $offset = 0, $useCache = false)
    {
        $cacheKey = self::STAFF_CACHE_KEY.md5(serialize([$filters, $sort, $limit, $offset, $useCache]));
        $cacheStaff = $this->cache->getItem($cacheKey);

        if (!$useCache || !$cacheStaff->isHit()) {
            $mainQuery = new \Elastica\Query\BoolQuery();

            if (!empty($filters[self::FILTER_CITY_NAME_KEY])) {
                $cityQuery = new \Elastica\Query\Terms();
                $cityQuery->setTerms('cityNA', [$filters[self::FILTER_CITY_NAME_KEY]]);
                $mainQuery->addMust($cityQuery);
            }

            $query = new \Elastica\Query($mainQuery);
            $query
                ->setFrom($offset)
                ->setSize($limit);

            foreach ($sort as $field => $dir) {
                $query->setSort([$field => $dir]);
            }

            $esResult = $this->esStaffFinder->search($query);
            $staffUuids = array_map(create_function('$o', 'return $o->getId();'), $esResult->getResults());

            $staff = [];
            // Batch this call
            $batches = array_chunk($staffUuids, 20);
            foreach ($batches as $batch) {
                $respStaff = $this->sngClient->Staff()->GetStaffByUUIDs($batch);

                if (isset($filters[self::FILTER_INCLUDE_NEXT_CLASSES])) {
                    // Fill-in the next classes
                    $nextClasses = $this->classesService->getNextClasses([
                        ClassesService::FILTER_STAFF_KEY => $batch,
                    ], ClassesService::AGGS_STAFF, (int) $filters[self::FILTER_INCLUDE_NEXT_CLASSES]);
                    foreach ($respStaff as $oneStaff) {
                        if (isset($nextClasses[$oneStaff->getUuid()])) {
                            $oneStaff->setNextClasses($nextClasses[$oneStaff->getUuid()]);
                        }
                    }
                }

                $staff = array_merge($staff, $respStaff);
            }

            if (empty($staff)) {
                return ['staff' => [], 'total' => $esResult->getTotalHits()];
            }

            $staffData = ['staff' => $staff, 'total' => $esResult->getTotalHits()];

            $cacheStaff->set($staffData);
            if ($useCache) {
                $cacheStaff->expiresAfter(self::STAFF_CACHE_TTL);
                $this->cache->save($cacheStaff);
            }
        }

        return $cacheStaff->get();
    }

    public function getStaffBySlug($slug)
    {
        $cacheKey = self::STAFF_PROFILE_CACHE_KEY.md5($slug);
        $cache = $this->cache->getItem($cacheKey);

        if (!$cache->isHit()) {
            try {
                $staff = $this->sngClient->Staff()->getStaffBySlug($slug);
            } catch (NotFoundException $e) {
                // do nothing
            }

            if (empty($staff)) {
                return null;
            }

            $cache->set($staff);
            $cache->expiresAfter(self::STAFF__PROFILE_CACHE_TTL);
            $this->cache->save($cache);
        }

        return $cache->get();
    }

}
