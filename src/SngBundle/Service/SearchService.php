<?php

namespace SngBundle\Service;

use Sng\ClientInterface;
use Elastica\SearchableInterface;
use Sng\Model\ScheduledClass\ScheduledClass;
use Sng\Model\Site\Location;
use Sng\Model\Staff\Staff;

class SearchService
{
    const DEFAULT_LIMIT = 10;

    const FILTER_CITY_KEY = 'city';

    private $sngClient;
    private $esFinder;
    private $classesService;

    public function __construct(
        ClientInterface $sngClient,
        SearchableInterface $esFinder,
        ClassesService $classesService
    ) {
        $this->sngClient = $sngClient;
        $this->esFinder = $esFinder;
        $this->classesService = $classesService;
    }

    public function getSearchResults($queryString, $filters = [], $limit = self::DEFAULT_LIMIT, $offset = 0)
    {
        $searchResult = [
            'total' => 0,
            'results' => [],
        ];

        $boolQuery = new \Elastica\Query\BoolQuery();

        $multiMatchQuery = new \Elastica\Query\MultiMatch();
        $multiMatchQuery->setQuery($queryString);
        $multiMatchQuery->setType(\Elastica\Query\MultiMatch::TYPE_MOST_FIELDS);
        $multiMatchQuery->setFields([
            "locationName^6", "locationDescription^3", "locationCity^4", "locationAddress^1", "staffName^2",
            "className^0.5", "classDescription^0.3", "sessionTypeName^0.3", "programName^0.3"
        ]);
        $multiMatchQuery->setFuzziness("AUTO");
        // The number of initial characters which will not be “fuzzified”.
        // This helps to reduce the number of terms which must be examined.
        $multiMatchQuery->setPrefixLength(0);
        $boolQuery->addMust($multiMatchQuery);

        $mainFilter = new \Elastica\Query\BoolQuery();
        $mainFilter->addShould(new \Elastica\Query\Term(["_type" => "location"]));
        $mainFilter->addShould(new \Elastica\Query\Term(["_type" => "staff"]));

        $classBoolFilter = new \Elastica\Query\BoolQuery();
        $classBoolFilter->addMust(new \Elastica\Query\Term(["_type" => "class"]));
        $classBoolFilter->addMust(new \Elastica\Query\Range('startDatetime', ['gte' => 'now', 'lt' => 'now+7d']));
        $mainFilter->addShould($classBoolFilter);

        if (!empty($filters[self::FILTER_CITY_KEY])) {
            $mainFilter->addMust(new \Elastica\Query\Term(["cityNA" => $filters[self::FILTER_CITY_KEY]]));
        }

        $boolQuery->addFilter($mainFilter);

        $query = new \Elastica\Query($boolQuery);
        $query
            ->setSort(["_score" => "desc", "startDatetime" => "asc"])
            ->setParam('stored_fields', ['id'])
            ->setFrom($offset)
            ->setSize($limit);

        $queryResult = $this->esFinder->search($query);

        $totalHits = $queryResult->getTotalHits();
        if ($totalHits === 0) {
            return $searchResult;
        }
        $searchResult['total'] = $totalHits;

        $results = $queryResult->getResults();

        $uuids = [
            'staff' => [],
            'class' => [],
            'location' => [],
        ];
        foreach ($results as $result) {
            switch ($result->getType()) {
                case 'location':
                case 'class':
                case 'staff':
                    $uuids[$result->getType()][] = $result->getId();
                    break;
                default:
            }
        }

        // Fetch the full objects
        $filled = [];
        if (!empty($uuids['location'])) {
            $locations = $this->sngClient->Site()->getLocations($uuids['location']);

            // Also get next classes
            $nextClasses = $this->classesService->getNextClasses([
                ClassesService::FILTER_LOCATIONS_KEY => $uuids['location']
            ], ClassesService::AGGS_LOCATION, 4);

            /** @var Location $location */
            foreach ($locations as $location) {
                $filled[$location->getUuid()] = $location;
                if (isset($nextClasses[$location->getUuid()])) {
                    $location->setNextClasses($nextClasses[$location->getUuid()]);
                }
            }
        }
        if (!empty($uuids['class'])) {
            $classes = $this->sngClient->ScheduledClass()->GetClassesByUUIDs($uuids['class']);
            /** @var ScheduledClass $class */
            foreach ($classes as $class) {
                $filled[$class->getUuid()] = $class;
            }
        }
        if (!empty($uuids['staff'])) {
            $staff = $this->sngClient->Staff()->GetStaffByUUIDs($uuids['staff']);

            // Also get next classes
            $nextClasses = $this->classesService->getNextClasses([
                ClassesService::FILTER_STAFF_KEY => $uuids['staff']
            ], ClassesService::AGGS_STAFF, 3);

            /** @var Staff $oneStaff */
            foreach ($staff as $oneStaff) {
                $filled[$oneStaff->getUuid()] = $oneStaff;
                if (isset($nextClasses[$oneStaff->getUuid()])) {
                    $oneStaff->setNextClasses($nextClasses[$oneStaff->getUuid()]);
                }
            }
        }

        foreach ($results as $result) {
            if (isset($filled[$result->getId()])) {
                $searchResult['results'][] = $filled[$result->getId()];
            }
        }

        return $searchResult;
    }
}