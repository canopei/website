<?php

namespace SngBundle\Service;

use Sng\Model\Site\City;
use Symfony\Component\HttpFoundation\Request;

class GeoIpService
{
    protected $locationsService;
    protected $environment;

    // Minimum distance of redirection in Km
    const DISTANCE_MIN_THRESHOLD = 100;

    const SERVER_KEY_GEOIP_CITY = 'GEOIP_CITY';
    const SERVER_KEY_GEOIP_LONGITUDE = 'GEOIP_LONGITUDE';
    const SERVER_KEY_GEOIP_LATITUDE = 'GEOIP_LATITUDE';

    public function __construct(LocationsService $locationsService, $environment)
    {
        $this->locationsService = $locationsService;
        $this->environment = $environment;
    }

    public function getNearestCity(Request $request)
    {
        list($geoipCity, $geoipLongitude, $geoipLatitude) = $this->getGeoIpData($request);

        // Try direct match
        $cityGroups = $this->locationsService->getAllCities();
        /** @var City $cityGroup */
        foreach ($cityGroups as $cityGroup) {
            if (in_array(strtolower($geoipCity), array_map('strtolower', $cityGroup->getSubcities()))) {
                return $cityGroup;
            }
        }

        // If we have no long and lat, we stop here
        if ($geoipLatitude == 0 && $geoipLongitude == 0) {
            return null;
        }

        // Calculate distances (in Km), try the nearest
        $distances = [];
        /** @var City $cityGroup */
        foreach ($cityGroups as $cityGroup) {
            if ($cityGroup->getLatitude() != 0 && $cityGroup->getLongitude() != 0) {
                $distances[] = ['city' => $cityGroup, 'distance' => $this->haversineGreatCircleDistance(
                    $geoipLatitude,
                    $geoipLongitude,
                    $cityGroup->getLatitude(),
                    $cityGroup->getLongitude()
                )];
            }
        }
        if (empty($distances)) {
            return null;
        }

        usort($distances, function ($a, $b) {
            return $a['distance'] <=> $b['distance'];
        });
        $minDistance = $distances[0];
        if ($minDistance['distance'] < self::DISTANCE_MIN_THRESHOLD) {
            return $minDistance['city'];
        }

        return null;
    }

    protected function getGeoIpData(Request $request)
    {
        $city = $request->server->get(self::SERVER_KEY_GEOIP_CITY);
        $longitude = (float) $request->server->get(self::SERVER_KEY_GEOIP_LONGITUDE);
        $latitude = (float) $request->server->get(self::SERVER_KEY_GEOIP_LATITUDE);

        // For non-prod envs. these can be overriden with cookies
        if ($this->environment != 'prod') {
            $city = $request->cookies->get(self::SERVER_KEY_GEOIP_CITY, $city);
            $longitude = $request->cookies->get(self::SERVER_KEY_GEOIP_LONGITUDE, $longitude);
            $latitude = $request->cookies->get(self::SERVER_KEY_GEOIP_LATITUDE, $latitude);
        }

        return [ $city, $longitude, $latitude ];
    }

    private function haversineGreatCircleDistance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371)
    {
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);

        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;

        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
                cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
        return $angle * $earthRadius;
    }
}