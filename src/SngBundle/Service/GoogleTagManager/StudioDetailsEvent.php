<?php

namespace SngBundle\Service\GoogleTagManager;

class StudioDetailsEvent extends Event
{
    protected $studioName;
    protected $isFavourite;
    protected $classesNo;
    protected $teachersNo;
    protected $cityName;

    public function __construct()
    {
        parent::__construct('studioDetail');
    }

    public function setStudioName($studioName)
    {
        $this->studioName = $studioName;
        return $this;
    }

    public function setIsFavourite($isFavourite)
    {
        $this->isFavourite = $isFavourite;
        return $this;
    }
    public function setClassesNo($classesNo)
    {
        $this->classesNo = $classesNo;
        return $this;
    }
    public function setTeachersNo($teachersNo)
    {
        $this->teachersNo = $teachersNo;
        return $this;
    }
    public function setCityName($cityName)
    {
        $this->cityName = $cityName;
        return $this;
    }

    public function toArray() {
        return array_merge(parent::toArray(), [
            'studioName' => $this->studioName,
            'isFavourite' => $this->isFavourite,
            'classesNo' => $this->classesNo,
            'teachersNo' => $this->teachersNo,
            'cityName' => $this->cityName,
        ]);
    }
}