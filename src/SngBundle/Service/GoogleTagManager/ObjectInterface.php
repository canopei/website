<?php

namespace SngBundle\Service\GoogleTagManager;

interface ObjectInterface
{
    public function toArray();
}