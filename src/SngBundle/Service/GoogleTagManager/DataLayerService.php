<?php

namespace SngBundle\Service\GoogleTagManager;

class DataLayerService extends \Twig_Extension
{
    protected $dataLayerObjects;

    public function __construct()
    {
        $this->dataLayerObjects = [];
    }

    public function add(ObjectInterface $object)
    {
        $this->dataLayerObjects[] = $object;
    }

    public function getDataLayer()
    {
        $dataLayer = [];

        /** @var ObjectInterface $dlObject */
        foreach ($this->dataLayerObjects as $dlObject) {
            $dataLayer[] = $dlObject->toArray();
        }

        return $dataLayer;
    }

    public function getFunctions()
    {
        return array(
            new \Twig_Function('getDataLayer', [$this, 'getDataLayer']),
        );
    }
}