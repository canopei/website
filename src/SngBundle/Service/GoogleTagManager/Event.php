<?php

namespace SngBundle\Service\GoogleTagManager;

class Event implements ObjectInterface
{
    protected $eventName;

    public function __construct($eventName)
    {
        $this->eventName = $eventName;
    }
    
    public function toArray() {
        return [
            'event' => $this->eventName
        ];
    }
}