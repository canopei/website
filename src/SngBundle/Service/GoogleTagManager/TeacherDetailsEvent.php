<?php

namespace SngBundle\Service\GoogleTagManager;

class TeacherDetailsEvent extends Event
{
    protected $teacherName;
    protected $isFavourite;
    protected $classesNo;
    protected $locationsNo;
    protected $cityName;

    public function __construct()
    {
        parent::__construct('studioDetail');
    }

    public function setTeacherName($teacherName)
    {
        $this->teacherName = $teacherName;
        return $this;
    }

    public function setIsFavourite($isFavourite)
    {
        $this->isFavourite = $isFavourite;
        return $this;
    }
    public function setClassesNo($classesNo)
    {
        $this->classesNo = $classesNo;
        return $this;
    }
    public function setLocationsNo($locationsNo)
    {
        $this->locationsNo = $locationsNo;
        return $this;
    }
    public function setCityName($cityName)
    {
        $this->cityName = $cityName;
        return $this;
    }

    public function toArray() {
        return array_merge(parent::toArray(), [
            'teacherName' => $this->teacherName,
            'isFavourite' => $this->isFavourite,
            'classesNo' => $this->classesNo,
            'locationsNo' => $this->locationsNo,
            'cityName' => $this->cityName,
        ]);
    }
}