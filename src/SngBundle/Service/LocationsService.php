<?php

namespace SngBundle\Service;

use Sng\ClientInterface;
use Elastica\SearchableInterface;
use Sng\Model\Site\City;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Sng\Exception\NotFoundException;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class LocationsService
{
    const DEFAULT_LIMIT = 10;

    const CITY_LOCATIONS_CACHE_KEY = 'city.locations.';
    const CITY_LOCATIONS_CACHE_TTL = 300;

    const ALL_CITIES_CACHE_KEY = 'all_cities.';
    const ALL_CITIES_CACHE_TTL = 300;

    const FEATURED_LOCATIONS_CACHE_KEY = 'locations.featured.';
    const FEATURED_LOCATIONS_CACHE_TTL = 300;

    const LOCATION_CACHE_KEY = 'locations.details.';
    const LOCATION_CACHE_TTL = 300;

    const MAP_LOCATIONS_CACHE_KEY = 'locations.map.';
    const MAP_LOCATIONS_CACHE_TTL = 43200;

    const STAFF_LOCATIONS_CACHE_KEY = 'locations.staff.';
    const STAFF_LOCATIONS_CACHE_TTL = 300;

    const SITE_CACHE_KEY = 'site.';
    const SITE_CACHE_TTL = 28800;

    const FILTER_CITIES_KEY = 'cities';
    
    const SESSION_KEY_CURRENT_CITY = 'l.cc';

    private $sngClient;
    private $esLocationFinder;
    private $cache;
    private $session;

    private $allCities;

    public function __construct(
        ClientInterface $sngClient,
        SearchableInterface $esLocationFinder,
        AdapterInterface $cache,
        SessionInterface $session
    ) {
        $this->sngClient = $sngClient;
        $this->esLocationFinder = $esLocationFinder;
        $this->cache = $cache;
        $this->session = $session;
    }

    public function getAllCities()
    {
        if (!empty($this->allCities)) {
            return $this->allCities;
        }

        $cacheCities = $this->cache->getItem(self::ALL_CITIES_CACHE_KEY);

        if (!$cacheCities->isHit()) {
            $cities = $this->sngClient->Site()->getCities();
            if (empty($cities)) {
                return [];
            }

            $cacheCities->set($cities);
            $cacheCities->expiresAfter(self::ALL_CITIES_CACHE_TTL);
            $this->cache->save($cacheCities);
        }

        $this->allCities = $cacheCities->get();
        return $this->allCities;
    }

    public function getNewestLocations($limit = self::DEFAULT_LIMIT, $offset = 0)
    {
        $locationsData = $this->getLocations([], [
            'createdAt' => 'desc',
        ], $limit, $offset);

        return $locationsData['locations'];
    }

    public function getFeaturedLocations($filters = [], $limit = self::DEFAULT_LIMIT, $offset = 0)
    {
        $locationsData = $this->getLocations($filters, [
            'featured' => 'desc',
        ], $limit, $offset, true);

        return $locationsData['locations'];
    }

    public function getTrendingLocations($filters = [], $limit = self::DEFAULT_LIMIT, $offset = 0)
    {
        $locationsData = $this->getLocations($filters, [
            'trending' => 'desc',
        ], $limit, $offset, true);

        return $locationsData['locations'];
    }

    public function getLocationsByCityName($cityName, $limit = self::DEFAULT_LIMIT, $offset = 0, $useCache = false)
    {
        $locationsData = $this->getLocations([
            self::FILTER_CITIES_KEY => [$cityName],
        ], [
            'featured' => 'desc',
        ], $limit, $offset, $useCache);

        return $locationsData;
    }

    public function getLocations($filters = [], $sort = [], $limit = self::DEFAULT_LIMIT, $offset = 0, $useCache = false)
    {
        $cacheKey = self::CITY_LOCATIONS_CACHE_KEY.md5(serialize([$filters, $sort, $limit, $offset, $useCache]));
        $cacheLocations = $this->cache->getItem($cacheKey);
        if (!$useCache || !$cacheLocations->isHit()) {
            $boolQuery = new \Elastica\Query\BoolQuery();

            $hasClassesQuery = new \Elastica\Query\Terms();
            $hasClassesQuery->setTerms('hasClasses', array(true));
            $boolQuery->addMust($hasClassesQuery);

            if (isset($filters[self::FILTER_CITIES_KEY])) {
                $citiesFilter = array_filter((array) $filters[self::FILTER_CITIES_KEY]);

                if (!empty($citiesFilter)) {
                    $nameQuery = new \Elastica\Query\Terms();
                    $nameQuery->setTerms('cityNA', $citiesFilter);
                    $boolQuery->addMust($nameQuery);
                }
            }

            $query = new \Elastica\Query($boolQuery);
            $query
                ->setParam('stored_fields', ['id'])
                ->setFrom($offset)
                ->setSize($limit);

            foreach ($sort as $field => $dir) {
                $query->setSort([$field => $dir]);
            }

            $esResults = $this->esLocationFinder->search($query);

            $locationUuids = array_map(create_function('$o', 'return $o->getId();'), $esResults->getResults());
            $locations = $this->sngClient->Site()->getLocations($locationUuids);

            $locationsData = ['locations' => $locations, 'total' => $esResults->getTotalHits()];

            if (empty($locations)) {
                return ['locations' => [], 'total' => 0];
            }

            $cacheLocations->set($locationsData);
            if ($useCache) {
                $cacheLocations->expiresAfter(self::CITY_LOCATIONS_CACHE_TTL);
                $this->cache->save($cacheLocations);
            }
        }

        return $cacheLocations->get();
    }

    public function getAllLocationsForMap($city = false)
    {
        $cacheKey = self::MAP_LOCATIONS_CACHE_KEY . md5(serialize([$city]));
        $cacheMapLocations = $this->cache->getItem($cacheKey);
        if (!$cacheMapLocations->isHit()) {
            $locationsForMap = [];
            $batchSize = 30;

            $filters = $city ? [ self::FILTER_CITIES_KEY => [$city] ] : [];
            while (true) {
                $locationsBatch = $this->getLocations($filters, [], $batchSize, count($locationsForMap), false);
                foreach ($locationsBatch['locations'] as $location) {
                    $photos = $location->getPhotos();
                    $photoUrl = empty($photos) ? '/images/default-image.jpg' : $photos[0]['path'];

                    $locationsForMap[] = [
                        'uuid' => $location->getUuid(),
                        'name' => $location->getName(),
                        'image' => $photoUrl,
                        'slug' => $location->getSlug(),
                        'lat' => $location->getLatitude(),
                        'long' => $location->getLongitude()
                    ];
                }

                if (count($locationsBatch) < $batchSize) {
                    break;
                }
            }

            $cacheMapLocations->set($locationsForMap);
            $cacheMapLocations->expiresAfter(self::MAP_LOCATIONS_CACHE_TTL);
            $this->cache->save($cacheMapLocations);
        }

        return $cacheMapLocations->get();
    }

    public function getRandomLocations($limit = self::DEFAULT_LIMIT)
    {
        $cacheKey = self::CITY_LOCATIONS_CACHE_KEY.'random.'.md5(serialize([$limit]));
        $cacheLocations = $this->cache->getItem($cacheKey);
        if (!$cacheLocations->isHit()) {
            $functionScoreQuery = new \Elastica\Query\FunctionScore();
            $functionScoreQuery->setRandomScore();

            $boolQuery = new \Elastica\Query\BoolQuery();
            $hasClassesQuery = new \Elastica\Query\Terms();
            $hasClassesQuery->setTerms('hasClasses', array(true));
            $boolQuery->addMust($hasClassesQuery);
            $functionScoreQuery->setQuery($boolQuery);

            $query = new \Elastica\Query($functionScoreQuery);
            $query
                ->setParam('stored_fields', ['id'])
                ->setSize($limit);

            $esResults = $this->esLocationFinder->search($query);

            $locationUuids = array_map(create_function('$o', 'return $o->getId();'), $esResults->getResults());
            $locations = $this->sngClient->Site()->getLocations($locationUuids);

            if (empty($locations)) {
                return [];
            }

            $cacheLocations->set($locations);
            $cacheLocations->expiresAfter(self::CITY_LOCATIONS_CACHE_TTL);
            $this->cache->save($cacheLocations);
        }

        return $cacheLocations->get();
    }

    public function getLocationBySlug($slug)
    {
        $cacheKey = self::LOCATION_CACHE_KEY.md5($slug);
        $cacheLocation = $this->cache->getItem($cacheKey);

        if (!$cacheLocation->isHit()) {
            try {
                $location = $this->sngClient->Site()->getLocationBySlug($slug);
            } catch (NotFoundException $e) {
                // do nothing
            }

            if (empty($location)) {
                return null;
            }

            $cacheLocation->set($location);
            $cacheLocation->expiresAfter(self::LOCATION_CACHE_TTL);
            $this->cache->save($cacheLocation);
        }

        return $cacheLocation->get();
    }

    public function getStaffLocations($staffUuid)
    {
        $cacheKey = self::STAFF_LOCATIONS_CACHE_KEY.md5($staffUuid);
        $cache = $this->cache->getItem($cacheKey);
        if (!$cache->isHit()) {
            $mainBoolQuery = new \Elastica\Query\BoolQuery();

            $hasClassesQuery = new \Elastica\Query\Terms();
            $hasClassesQuery->setTerms('hasClasses', array(true));
            $mainBoolQuery->addMust($hasClassesQuery);


            $childQuery = new \Elastica\Query\BoolQuery();
            $isAvailableQuery = new \Elastica\Query\Terms();
            $isAvailableQuery->setTerms('isAvailable', array(true));
            $childQuery->addMust($isAvailableQuery);

            $isActiveQuery = new \Elastica\Query\Terms();
            $isActiveQuery->setTerms('active', array(true));
            $childQuery->addMust($isActiveQuery);

            $staffUuidQuery = new \Elastica\Query\Terms();
            $staffUuidQuery->setTerms('staff.uuid', array($staffUuid));
            $childQuery->addMust($staffUuidQuery);

            $startDatetimeQuery = new \Elastica\Query\Range();
            $startDatetimeQuery->addField('startDatetime', array('gte' => 'now-7d', 'lt' => 'now+7d'));
            $childQuery->addMust($startDatetimeQuery);


            $hasChildQuery = new \Elastica\Query\HasChild($childQuery, 'class');
            $mainBoolQuery->addMust($hasChildQuery);


            $query = new \Elastica\Query($mainBoolQuery);
            $query->setParam('stored_fields', ['id']);

            $esResults = $this->esLocationFinder->search($query);

            $locationUuids = array_map(create_function('$o', 'return $o->getId();'), $esResults->getResults());
            $locations = $this->sngClient->Site()->getLocations($locationUuids);

            if (empty($locations)) {
                return [];
            }

            $cache->set($locations);
            $cache->expiresAfter(self::STAFF_LOCATIONS_CACHE_TTL);
            $this->cache->save($cache);
        }

        return $cache->get();
    }

    public function setCurrentCity(City $currentCity)
    {
        $this->session->set(self::SESSION_KEY_CURRENT_CITY, [
            'id' => $currentCity->getId(),
            'empty' => $currentCity->getIsEmpty()
        ]);
    }

    public function unsetCurrentCity()
    {
        $this->session->set(self::SESSION_KEY_CURRENT_CITY, null);
    }

    public function getCurrentCity()
    {
        $cityData = $this->session->get(self::SESSION_KEY_CURRENT_CITY);
        if (!isset($cityData['id'])) {
            return null;
        }

        $cities = $this->getAllCities();
        /** @var City $city */
        foreach($cities as $city){
            if($city->getId() == $cityData['id']) {
                $city->setIsEmpty((bool) $cityData['empty']);
                return $city;
            }
        }

        return null;
    }

    public function getCityByName($cityName)
    {
        $cities = $this->getAllCities();
        foreach($cities as $city){
            if($city->getName() === $cityName){
                return $city;
            }
        }
        return null;
    }

    public function getSiteByUuid($siteUuid)
    {
        $cacheKey = self::SITE_CACHE_KEY . $siteUuid;
        $cache = $this->cache->getItem($cacheKey);

        if (!$cache->isHit()) {
            $site = null;
            try {
                $site = $this->sngClient->Site()->getSiteByUuid($siteUuid);
            } catch (NotFoundException $e) {
                // do nothing
            }

            if (empty($site)) {
                return null;
            }

            $cache->set($site);
            $cache->expiresAfter(self::SITE_CACHE_TTL);
            $this->cache->save($cache);
        }

        return $cache->get();
    }
}
