<?php


namespace SngBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sng\Model\Sale\Membership;
use Sng\Model\Sale\Purchase;
use Sng\Model\Sale\Service;
use Stripe\Error\Base;
use Stripe\Error\Card;
use Stripe\Stripe;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sng\Model\ScheduledClass\Booking;


class CheckoutController extends Controller
{
    /**
     * @Route("/checkout/", name="checkout", options={"expose"=true})
     */
    public function checkoutAction(Request $request)
    {
        $sngClient = $this->get('sng_client');
        $user = $this->getUser();

        $sessionServices = $this->get('session')->get(PurchaseController::SESSION_SERVICES_KEY);
        $cart = $this->get('session')->get(PurchaseController::SESSION_CART_KEY);

        if (empty($cart) || empty($sessionServices)) {
            throw new NotFoundHttpException('Empty basket');
        }

        $cartServices = [];
        $taxTotal = 0;
        $grandTotal = 0;
        foreach ($cart as $siteUuid => $siteServices) {
            if ($siteUuid == 'global') {
                /** @var Membership $membership */
                $membership = $this->get('sng.sale')->getMembershipByUuid($siteServices[0]);
                if ($membership === false) {
                    throw new NotFoundHttpException('Empty basket (membership not found).');
                }

                $grandTotal += $membership->getPrice();
                $taxTotal += $membership->getTax();

                $cartServices[] = $membership;
            } else {
                $site = $this->get('sng.locations')->getSiteByUuid($siteUuid);

                foreach ($siteServices as $siteServiceId) {
                    if (isset($sessionServices[$siteUuid][$siteServiceId])) {
                        /** @var Service $s */
                        $s = $sessionServices[$siteUuid][$siteServiceId];
                        $s->setSiteName($site->getName());
                        $cartServices[] = $s;

                        $tax = 0;
                        if (!$s->getTaxIncluded() && $s->getTaxRate() > 0) {
                            $tax = ($s->getTaxRate() / 100) * $s->getPrice();
                        }

                        $grandTotal += $s->getPrice() + $tax;
                        $taxTotal += $tax;
                    }
                }
            }
        }
        if (empty($cartServices)) {
            throw new NotFoundHttpException('Empty basket');
        }

        $form = $this->createForm('SngBundle\Form\CheckoutType', null, array(
            'action' => $this->generateUrl('checkout'),
            'method' => 'POST',
            'attr' => array('id' => 'checkoutForm'),
        ));

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $data = $form->getData();

                // create stripe token
                $stripeClient = $this->get('canopei_stripe.client');
                try {
                    $cardToken = $stripeClient->createCardToken($data['cardNumber'], $data['month'], $data['year'], $data['ccv']);
                } catch (\Exception $e) {
                    if ($e instanceof Card) {
                        $form->get('cardNumber')->addError(new FormError($e->getMessage()));
                        $this->get('session')->getFlashBag()->set('msg', $e->getMessage());
                    } else {
                        $this->get('logger')->error('Stripe token - unknown error - ' . $e->getMessage());
                        $this->get('session')->getFlashBag()->set('msg', 'An error occurred. Please try again.');
                    }
                }

                if (isset($cardToken)) {
                    $description = '';
                    /** @var Service $cs */
                    foreach ($cartServices as $cs) {
                        if ($cs instanceof Service) {
                            $description .= $cs->getName() . ' at ' . $cs->getSiteName() . ', ';
                        } else if ($cs instanceof Membership) {
                            $description .= $cs->getName() . ' - Global';
                        }
                    }
                    $description = substr($description, 0, -2);

                    // attempt to charge the client
                    try {
                        $charge = $stripeClient->createCharge($grandTotal * 100, 'usd', $cardToken, $user->getEmail(), $description, [
                            'sngUserUuid' => $user->getUuid(),
                            'services' => json_encode($cartServices)
                        ]);
                    } catch (\Exception $e) {
                        if ($e instanceof Card) {
                            // The card was rejected by the bank
                            $declineCode = $e->getDeclineCode();
                            if (!empty($declineCode)) {
                                $this->get('logger')->error('Stripe charge - declined - ' . $e->getMessage() . ' - ' . $declineCode);
                            }

                            $form->get('cardNumber')->addError(new FormError($e->getMessage()));
                            $this->get('session')->getFlashBag()->set('msg', $e->getMessage());
                        } else {
                            $this->get('logger')->error('Stripe charge - unknown error - ' . $e->getMessage());
                            $this->get('session')->getFlashBag()->set('msg', 'An error occurred. Please try again.');
                        }
                    }

                    if (isset($charge)) {
                        // save the purchase in our DB
                        $purchase = new Purchase();
                        $purchase
                            ->setReferenceId($charge->id)
                            ->setFirstName($data['firstName'])
                            ->setLastName($data['lastName'])
                            ->setCountry($data['country'])
                            ->setAddress($data['address'])
                            ->setAddress2($data['address2'])
                            ->setState($data['state'])
                            ->setZip($data['zip'])
                            ->setCity($data['city'])
                            ->setProducts(json_encode($cartServices))
                            ->setTotal($grandTotal)
                            ->setTax($taxTotal)
                        ;
                        $sngClient->Sale()->createPurchase($user->getUuid(), $purchase);

                        // save the purchase in Mindbody and add to class if any
                        $classUuid = (string)$this->get('session')->get(PurchaseController::SESSION_PURCHASE_CLASS_UUID);
                        $class = null;
                        if (!empty($classUuid)) {
                            $classes = $sngClient->ScheduledClass()->getClassesByUUIDs([$classUuid]);
                            if (empty($classes)) {
                                return new NotFoundHttpException('Class not found.');
                            }
                            /** @var \Sng\Model\ScheduledClass\ScheduledClass $class */
                            $class = $classes[0];
                        }

                        foreach ($cart as $siteUuid => $siteServices) {
                            if ($siteUuid == 'global') {
                                $newAccountMembership = null;
                                foreach ($siteServices as $membershipUuid) {
                                    $newAccountMembership = $sngClient->Account()->createAccountMembership($user->getUuid(), $membershipUuid);
                                }
                                if ($class && $newAccountMembership) {
                                    try {
                                        $this->get('sng.classes')->addAccountToClass($user, $class, null, $newAccountMembership->getId());
                                    } catch (\Exception $e) {
                                        $this->get('logger')->error("Failed to add account to class after global purchase: " . $e->getMessage());
                                    }
                                }
                            } else {
                                $classUuid = $class ? $class->getUuid() : '';

                                foreach ($siteServices as $siteServiceId) {
                                    if (isset($sessionServices[$siteUuid][$siteServiceId])) {
                                        /** @var Service $s */
                                        $s = $sessionServices[$siteUuid][$siteServiceId];

                                        try {
                                            $sngClient->Sale()->createSimpleSale($user->getUuid(), $s->getSiteUuid(), $s->getMbId(), $classUuid);
                                        } catch (\InvalidArgumentException $e) {
                                            throw new BadRequestHttpException('Failed creating the sale: ' . $e->getMessage());
                                        }

                                        // also create the booking
                                        if (!empty($classUuid)) {
                                            $booking = new Booking();
                                            $booking->setServiceId($s->getMbId());
                                            $sngClient->ScheduledClass()->createBooking($user->getUuid(), $classUuid, $booking);
                                        }
                                    }
                                }
                            }
                        }

                        // clear session cart / services / class
                        $this->get('session')->remove(PurchaseController::SESSION_CART_KEY);
                        $this->get('session')->remove(PurchaseController::SESSION_PURCHASE_CLASS_UUID);
                        $this->get('session')->remove(PurchaseController::SESSION_PURCHASE_SITE_UUID);

                        $this->get('sng.accounts')->invalidateAccountServicesCache($user->getUuid());
                        $this->get('sng.accounts')->invalidateAccountScheduleCache($user->getUuid());

                        $message = 'You successfully purchased the membership.';
                        if ($class) {
                            $message .= ' You were booked for class \''.$class->getName().'\'.';
                        }
                        $this->get('session')->getFlashBag()->set('msg', $message);

                        // check for referer
                        $url = $this->get('session')->get(PurchaseController::SESSION_REFERER_KEY);
                        if (empty($url)) {
                            $url = $this->get('router')->generate('homepage');
                        }

                        return new RedirectResponse($url);
                    }
                }
            }
        }

        return $this->render('FrontEndBundle:Default:checkout.html.twig', array(
            'form' => $form->createView(),
            'cartServices' => $cartServices,
            'taxTotal' => $taxTotal,
            'grandTotal' => $grandTotal,
        ));
    }
}