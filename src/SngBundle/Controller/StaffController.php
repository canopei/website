<?php

namespace SngBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sng\Exception\NotFoundException;
use Sng\Model\Staff\Staff;
use SngBundle\Service\ClassesService;
use SngBundle\Service\GoogleTagManager\TeacherDetailsEvent;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use SngBundle\Service\StaffService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use SngBundle\Form\UserLoginType;
use SngBundle\Security\User\User;
use SngBundle\Form\UserRegistrationType;

class StaffController extends Controller
{
    const STAFF_PER_PAGE = 20;

    /**
     * @Route("/teachers/", name="teachersList", options={"expose"=true})
     */
    public function teachersAction(Request $request)
    {
        $page = $request->query->get('page', 1);
        $offset = ($page - 1) * self::STAFF_PER_PAGE;

        $staffService = $this->get('sng.staff');
        $staffData = $staffService->getStaff([
            StaffService::FILTER_INCLUDE_NEXT_CLASSES => 1,
        ], [
            'featured' => 'desc',
            'staffNameNA' => 'asc',
        ], self::STAFF_PER_PAGE, $offset, $page === 1);
        $pages = ceil($staffData['total'] / self::STAFF_PER_PAGE);

        if (count($staffData['staff']) == 0) {
            throw new NotFoundHttpException('Teachers page not found.');
        }

        if ($request->isXmlHttpRequest()) {
            return new JsonResponse([
                'content' => $this->get('templating')->render('@FrontEnd/Teachers/partials/teachersProfileSimpleList.html.twig', [
                    'teachers' => $staffData['staff']
                ])
            ], Response::HTTP_OK);
        }

        return $this->render('FrontEndBundle:Teachers:teachers.html.twig', [
            'teachers' => $staffData['staff'],
            'currentPage' => $page,
            'totalPages' => $pages,
        ]);
    }

    /**
     * @Route("/teachers/{citySlug}/", name="teachersListByCity", options={"expose"=true})
     */
    public function listByCityAction(Request $request, $citySlug)
    {

        $locationsService = $this->get('sng.locations');

        $currentCity = null;
        $allCities = $locationsService->getAllCities();
        foreach ($allCities as $city) {
            if ($city->getSlug() == $citySlug) {
                $currentCity = $city;
                break;
            }
        }
        if (!$currentCity) {
            throw new NotFoundHttpException('City not found.');
        }

        $page = $request->query->get('page', 1);
        $offset = ($page - 1) * self::STAFF_PER_PAGE;

        $staffService = $this->get('sng.staff');
        $staffData = $staffService->getActiveStaff([
            StaffService::FILTER_INCLUDE_NEXT_CLASSES => 1,
            StaffService::FILTER_CITY_NAME_KEY => $currentCity->getName(),
            StaffService::FILTER_INCLUDE_NEXT_CLASSES => 1,
        ], 20, $offset, true);
        $pages = ceil($staffData['total'] / self::STAFF_PER_PAGE);

        if (count($staffData['staff']) == 0) {
            throw new NotFoundHttpException('Teachers page not found.');
        }

        if ($request->isXmlHttpRequest()) {
            return new JsonResponse([
                'content' => $this->get('templating')->render('@FrontEnd/Teachers/partials/teachersProfileSimpleList.html.twig', [
                    'teachers' => $staffData['staff']
                ])
            ], Response::HTTP_OK);
        }

        return $this->render('FrontEndBundle:Teachers:teachers.html.twig', [
            'city' => $currentCity,
            'teachers' => $staffData['staff'],
            'currentPage' => $page,
            'totalPages' => $pages,
        ]);
    }

    /**
     * @Route("/teachers/profile/{slug}/", name="teacherProfile")
     */
    public function teachersProfileAction($slug, Request $request)
    {

        // login
        $authenticationUtils = $this->get('security.authentication_utils');
        $exception = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        $loginForm = $this->createForm(UserLoginType::class, null, [
            'action' => $this->generateUrl('connect_sng_check'),
            'method' => 'POST',
            'data' => array('email' => $lastUsername),
        ]);

        // register
        $user = new User();
        $registerForm = $this->createForm(UserRegistrationType::class, $user);
        $registerForm->handleRequest($request);
        if ($registerForm->isSubmitted() && $registerForm->isValid()) {
            $sngClient = $this->get('sng_client');

            try {
                $account = $sngClient->Account()->createAccount($user->getName(), $user->getEmail(), $user->getPassword());

                $user->setUuid($account->getUuid());
                $user->setEmail($account->getEmail());
                $user->setName($account->getFullName());
                $user->setConfirmedAt($account->getConfirmedAt());
                $user->setCreatedAt($account->getCreatedAt());
                $user->addRole(User::ROLE_SNG_USER);

                $token = new PostAuthenticationGuardToken($user, 'main', $user->getRoles());
                $this->get('security.token_storage')->setToken($token);
                $this->get('session')->set('_security_main', serialize($token));

                $dispatcher = $this->get('event_dispatcher');
                $userRegisteredEvent = new UserRegisteredEvent($account);
                $dispatcher->dispatch(UserRegisteredEvent::NAME, $userRegisteredEvent);

                return $this->redirectToRoute('homepage');
            } catch (AlreadyExistsException $e) {
                $registerForm->get('email')->addError(new FormError('E-mail already used.'));
            } catch (\InvalidArgumentException $e) {
                $registerForm->get('email')->addError(new FormError('Invalid e-mail address.'));
            } catch (\UnexpectedValueException $e) {
                $registerForm->get('email')->addError(new FormError('An error occurred. Please try again later.'));
            }
        }

        $staffService = $this->get('sng.staff');
        /** @var Staff $staff */
        $staff = $staffService->getStaffBySlug($slug);
        if (!$staff) {
            throw new NotFoundHttpException('Teacher was not found.');
        }

        $locationsService = $this->get('sng.locations');
        $locations = $locationsService->getStaffLocations($staff->getUuid());

        $classesService = $this->get('sng.classes');
        $now = new \DateTime();
        $end = clone $now;
        $end->modify('+7 days');
        $classes = $classesService->getClasses(
            [
                ClassesService::FILTER_STAFF_KEY => [$staff->getUuid()],
                ClassesService::FILTER_START_DATETIME_KEY => $now->format('Y-m-d H:i:s'),
                ClassesService::FILTER_END_DATETIME_KEY => $end->format('Y-m-d H:i:s'),
            ],
            ['startDatetime' => 'asc'],
            6, 0, true
        );

        // Check for instagram photos
        $instagramMedia = [];
        $instagramProfileUrl = '';
        foreach ($staff->getSocialProfiles() as $sProfile) {
            if ($sProfile['type'] == 'instagram') {
                $instagramProfileUrl = $sProfile['url'];
                break;
            }
        }
        if ($instagramProfileUrl != '') {
            $ok = preg_match('#instagram.com/(?<name>[^/]+)/?$#i', $instagramProfileUrl, $matches);
            if ($ok) {
                try {
                    $instagramMedia = $this->get('sng.instagram')->getUserMedia($matches['name'], 6, true);
                } catch (NotFoundException | \UnexpectedValueException $e) {
                    // do nothing
                }
            }
        }

        // GTM
        $gtmObject = (new TeacherDetailsEvent())
            ->setTeacherName($staff->getName())
            ->setClassesNo(count($classes))
            ->setLocationsNo(count($locations))
            ->setCityName($staff->getAddress()->getCity())
            ->setIsFavourite($this->get('sng.favourites.staff')->isItemFavourite($staff->getUuid()));
        $this->get('sng.gtm.datalayer')->add($gtmObject);

        return $this->render('FrontEndBundle:Teachers:teacherProfile.html.twig', [
            'teacher' => $staff,
            'studios' => $locations,
            'classes' => $classes,
            'instagramMedia' => $instagramMedia,
            'registerForm' => $registerForm->createView(),
            'loginForm' => $loginForm->createView(),
        ]);
    }

    /**
     * @Route("/teachers/{staffUuid}/favourites/", name="staffAddToFavourites", options={"expose"=true})
     * @Method({"POST"})
     */
    public function addToFavouritesAction($staffUuid)
    {
        $staffFavService = $this->get('sng.favourites.staff');
        $ok = $staffFavService->addItem($staffUuid);

        return new Response(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @Route("/teachers/{staffUuid}/favourites/", name="staffRemoveFromFavourites", options={"expose"=true})
     * @Method({"DELETE"})
     */
    public function removeFromFavourites($staffUuid)
    {
        $staffFavService = $this->get('sng.favourites.staff');
        $ok = $staffFavService->removeItem($staffUuid);

        return new Response(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @Route("/teacher-training/", name="teachersTraining")
     */
    public function teachersTrainingAction()
    {
        return $this->render('FrontEndBundle:Teachers:teachers_training.html.twig');
    }
}
