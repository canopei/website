<?php

namespace SngBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sng\Model\Site\City;
use SngBundle\Security\User\User;
use SngBundle\Service\LocationsService;
use SngBundle\Service\StaffService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
 use Symfony\Component\Security\Guard\Token\PostAuthenticationGuardToken;

class HomepageController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function homepageAction(Request $request)
    {
        $locationsService = $this->get('sng.locations');
        $staffService = $this->get('sng.staff');

        // Do not unset the selected city if this is a redirect from the login / register page
        $previousUrl = $request->headers->get('referer');
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if (!($user instanceof User && strstr($previousUrl, '/login'))) {
            $locationsService->unsetCurrentCity();
        }

        // check GeoIP
        $nearestCity = $this->get('sng.geoip')->getNearestCity($request);
        $cityNameFilter = $nearestCity instanceof City ? $nearestCity->getName() : null;

        $featuredLocations = $locationsService->getFeaturedLocations([LocationsService::FILTER_CITIES_KEY => $cityNameFilter]);
        $mostPopularStaffData = $staffService->getActiveStaff([
            StaffService::FILTER_INCLUDE_NEXT_CLASSES => 1,
            StaffService::FILTER_CITY_NAME_KEY => $cityNameFilter
        ], 4, 0, true);

        $trendingLocations = $locationsService->getTrendingLocations(6);

        $moreStaffData = $staffService->getActiveStaff([
            StaffService::FILTER_INCLUDE_NEXT_CLASSES => 1,
        ], 4,0, true);

        return $this->render('FrontEndBundle:Default:index_v1.html.twig', [
            'nearestCity' => $nearestCity,
            'featuredStudios' => $featuredLocations,
            'trendingStudios' => $trendingLocations,
            'mostPopularStaff' => $mostPopularStaffData['staff'],
            'staff' => $moreStaffData['staff'],
        ]);
    }
}
