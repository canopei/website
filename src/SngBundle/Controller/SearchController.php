<?php

namespace SngBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sng\Model\ScheduledClass\ScheduledClass;
use Sng\Model\Site\City;
use SngBundle\Service\SearchService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SearchController extends Controller
{
    const RESULTS_PER_PAGE = 15;

    /**
     * @Route("/search/", name="search", options={"expose"=true})
     */
    public function searchAction(Request $request)
    {
        $locationsService = $this->get('sng.locations');
        $searchService = $this->get('sng.search');

        $queryString = trim($request->query->get('q'));
        $page = (int) $request->query->get('page', 1);
        $resultsOffset = ($page - 1) * self::RESULTS_PER_PAGE;

        $cityName = $request->query->get('city');
        $city = $locationsService->getCityByName($cityName);

        $filters = [];
        if ($city instanceof City) {
            $filters[SearchService::FILTER_CITY_KEY] = $city->getName();
        }
        $searchResult = $searchService->getSearchResults($queryString, $filters, self::RESULTS_PER_PAGE, $resultsOffset);

        $pages = ceil($searchResult['total'] / self::RESULTS_PER_PAGE);

        if ($page > 1 && empty($searchResult['results'])) {
            throw new NotFoundHttpException('Search results page not found.');
        }

        // stack classes by class schedule ID
        $stacks = [];
        /** @var ScheduledClass $searchRes */
        foreach ($searchResult['results'] as $idx => $searchRes) {
            if ($searchRes instanceof ScheduledClass) {
                $descriptionUuid = $searchRes->getDescriptionUuid();
                if (!isset($stacks[$descriptionUuid])) {
                    $stacks[$descriptionUuid] = [ 'idx' => $idx, 'classes' => [] ];
                }
                $stacks[$descriptionUuid]['classes'][] = $searchRes;
                unset($searchResult['results'][$idx]);
            }
        }
        foreach ($stacks as $stack) {
            $searchResult['results'][$stack['idx']] = $stack['classes'];
        }

        if ($request->isXmlHttpRequest()) {
            return new JsonResponse([
                'content' => $this->get('templating')->render('@FrontEnd/Search/partials/resultsList.html.twig', [
                    'results' => $searchResult['results']
                ])
            ], Response::HTTP_OK);
        }

        return $this->render('FrontEndBundle:Search:results.html.twig', [
            'queryString' => $queryString,
            'city' => $city,
            'result' => $searchResult,
            'currentPage' => $page,
            'totalPages' => $pages,
            'resultsPerPage' => self::RESULTS_PER_PAGE
        ]);
    }
}
