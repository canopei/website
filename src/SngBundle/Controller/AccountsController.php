<?php

namespace SngBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sng\Model\Account\Client;
use Sng\Model\Site\Site;
use SngBundle\Event\UserConfirmedEvent;
use SngBundle\Security\User\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sng\Exception\NotFoundException;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Guard\Token\PostAuthenticationGuardToken;
use Sng\Exception\AlreadyExistsException;
use SngBundle\Form\ChangePasswordType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Email;

class AccountsController extends Controller
{
    /**
     * @Route("/confirm/{uuid}/{code}", name="account_confirm")
     */
    public function confirmAccountAction($uuid, $code)
    {
        $exception = true;
        try {
            $account = $this->get('sng_client')->Account()->confirmAccount($uuid, $code);
            $exception = false;
        } catch (AlreadyExistsException $e) {
            // do nothing
        } catch (NotFoundException $e) {
            $this->get('session')->getFlashBag()->set('msg', 'Invalid account confirmation code.');
        } catch (\Exception $e) {
            $this->get('session')->getFlashBag()->set('msg', 'An error occurred while confirming your account. Please try again.');

        }
        if ($exception) {
            return $this->redirectToRoute('homepage');
        }

        $userProvider = $this->get('sng.auth.user_provider');
        // Since the above call worked, no exception is expected here!
        $user = $userProvider->loadUserByUuid($account->getUuid());
        $user->addRole(User::ROLE_SNG_USER);

        $authToken = new PostAuthenticationGuardToken($user, 'main', $user->getRoles());
        $this->get('security.token_storage')->setToken($authToken);
        $this->get('session')->set('_security_main', serialize($authToken));

        $dispatcher = $this->get('event_dispatcher');
        $userRegisteredEvent = new UserConfirmedEvent($account);
        $dispatcher->dispatch(UserConfirmedEvent::NAME, $userRegisteredEvent);

        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("/forgot-password/", name="forgot_password")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function changePasswordRequestAction(Request $request)
    {
        $form = $this->createFormBuilder([])
            ->add('email', EmailType::class, [ 'required' => true, 'constraints' => [
                new Email(['message' => 'Please insert a valid e-mail address.'])
            ]])
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $data = $form->getData();

                $err = null;
                try {
                    $account = $this->get('sng_client')->Account()->getAccount($data['email']);
                    $requestData = $this->get('sng_client')->Account()->createPasswordChangeRequest($account->getUuid());
                    $this->get('sng.email')->sendPasswordChangeEmail($account, $requestData['code']);
                } catch (NotFoundException $e) {
                    // do nothing
                } catch (\Exception $e) {
                    $err = $e;
                    $form->get('email')->addError(new FormError('An error occurred. Please try again.'));
                }

                if (!$err) {
                    $this->get('session')->getFlashBag()->set('msg', 'A password reset link has been sent.');
                    return $this->redirectToRoute('login');
                }
            }
        }

        return $this->render('SngBundle:Security:forgot_password.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/change-password/{uuid}/{code}", name="password_change")
     */
    public function changePasswordAction(Request $request, $uuid, $code)
    {
        try {
            $account = $this->get('sng_client')->Account()->getAccountByPasswordChangeRequestCode($code);
        } catch (NotFoundException $e) {
            throw new NotFoundHttpException('Account not found.');
        }
        if ($account->getUuid() != $uuid) {
            throw new NotFoundHttpException('Invalid account UUID.');
        }

        $form = $this->createForm(ChangePasswordType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $data = $form->getData();

                try {
                    $this->get('sng_client')->Account()->changePassword($account->getUuid(), $data['password']);
                } catch (\Exception $e) {
                    $form->get('password')->addError(new FormError('An error occurred. Please try again.'));
                }

                $user = $this->get('sng.auth.user_provider')->loadUserByUuid($account->getUuid());
                $user->addRole(User::ROLE_SNG_USER);

                $token = new PostAuthenticationGuardToken($user, 'main', $user->getRoles());
                $this->get('security.token_storage')->setToken($token);
                $this->get('session')->set('_security_main', serialize($token));

                $this->get('session')->getFlashBag()->set('msg', 'Your password has been successfully changed.');
                return $this->redirectToRoute('homepage');
            }
        }

        return $this->render('SngBundle:Security:change_password.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/sites/{siteUuid}/clients/", name="createClient", options={"expose"=true})
     * @Method("POST")
     */
    public function createClientAction(Request $request, $siteUuid)
    {
        $sngClient = $this->get('sng_client');
        /** @var User $user */
        $user = $this->getUser();

        /** @var Site $site */
        $site = $this->get('sng.locations')->getSiteByUuid($siteUuid);

        // Check that the user doesn't already has the account linked\
        // check if account has client on class' site
        $accountClients = $sngClient->Account()->getClientsByAccount($user->getUuid());
        /** @var \Sng\Model\Account\Client $client */
        foreach ($accountClients as $client) {
            if ($client->getSite()->getMbId() == $site->getMbId()) {
                return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
            }
        }

        /** @var Client $newClient */
        $newClient = $this->get('sng.accounts')->createClientForUser($site->getMbId(), $user);

        // link the new client with the current account
        $sngClient->Account()->linkClientToAccount($user->getUuid(), $newClient->getUuid());

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @Route("/accounts/send-confirmation-email", name="sendConfirmationEmail", options={"expose"=true})
     * @Method("POST")
     */
    public function sendConfirmationEmailAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();

        if ($request->isXmlHttpRequest()) {
            if ($user->isConfirmed()) {
                return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
            }

            $account = $this->get('sng_client')->Account()->getAccount($user->getUuid());

            $this->get('sng.email')->sendConfirmationEmail($account);

            return new JsonResponse(null, Response::HTTP_NO_CONTENT);
        }

        return new NotFoundHttpException();
    }
}
