<?php


namespace SngBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sng\Model\Account\Account;
use Sng\Model\Account\Client;
use Sng\Model\Sale\Service;
use Sng\Model\Site\Site;
use Sng\Model\ScheduledClass\ScheduledClass;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sng\Exception\AlreadyExistsException;
use SngBundle\Event\UserRegisteredEvent;
use SngBundle\Form\UserLoginType;
use SngBundle\Security\User\User;
use SngBundle\Form\UserRegistrationType;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Guard\Token\PostAuthenticationGuardToken;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Response;


class PurchaseController extends Controller
{
    const SESSION_PURCHASE_CLASS_UUID = 'purchase.cuuid';
    const SESSION_PURCHASE_SITE_UUID = 'purchase.suuid';
    const SESSION_CART_KEY = 'purchase.cart';
    const SESSION_SERVICES_KEY = 'session.services';
    const SESSION_REFERER_KEY = 'purchase.referer';
    const SNG_DROPIN_PRICING_NAMES = [
        'sng global drop in',
        'sng global pass'
    ];

    /**
     * @Route("/purchase/", name="purchase", options={"expose"=true})
     */
    public function purchaseAction(Request $request)
    {
        $sngClient = $this->get('sng_client');

        $classUuid = $this->get('session')->get(self::SESSION_PURCHASE_CLASS_UUID);
        $siteUuid = $this->get('session')->get(self::SESSION_PURCHASE_SITE_UUID);
        if (empty($classUuid) && empty($siteUuid)) {
            throw new NotFoundHttpException('Empty basket.');
        }

        $site = $this->get('sng.locations')->getSiteByUuid($siteUuid);
        if (!$site instanceof Site) {
            throw new NotFoundHttpException('Site not found.');
        }

        // get class details
        if (!empty($classUuid)) {
            $classes = $sngClient->ScheduledClass()->getClassesByUUIDs([$classUuid]);
            if (empty($classes)) {
                throw new NotFoundHttpException('Class not found.');
            }
            /** @var ScheduledClass $class */
            $class = $classes[0];
        }

        // cache these into session
        $sessionServices = $this->get('session')->get(self::SESSION_SERVICES_KEY);
        if (empty($sessionServices) || !is_array($sessionServices) || count($sessionServices) > 20) {
            $sessionServices = [];
        }

        // get services list for a class
        $saleService = $this->get('sng.sale');
        if (!empty($classUuid)) {
            $services = $saleService->getSngSaleServicesForClass($class->getUuid());
            if (empty($services)) {
                throw new NotFoundHttpException('No class services available.');
            }
        } else {
            $services = $saleService->getSngSaleServicesForSite($site->getUuid());
        }

        /** @var Service $service */
        foreach ($services as $service) {
            if (!isset($sessionServices[$service->getSiteUuid()])) {
                $sessionServices[$service->getSiteUuid()] = [];
            }
            $sessionServices[$service->getSiteUuid()][$service->getMbId()] = $service;
        }

        $this->get('session')->set(self::SESSION_SERVICES_KEY, $sessionServices);

        // get the global memberships only if the class program has a dropin service ID
        $globalMemberships = [];
        if (empty($classUuid) || $class->getProgramDropinServiceMbId() > 0) {
            $globalMemberships = $this->get('sng.sale')->getAvailableMemberships([], true);
        }

        // login
        $authenticationUtils = $this->get('security.authentication_utils');
        $lastUsername = $authenticationUtils->getLastUsername();

        $loginForm = $this->createForm(UserLoginType::class, null, [
            'action' => $this->generateUrl('connect_sng_check'),
            'method' => 'POST',
            'data' => array('email' => $lastUsername),
        ]);

        // register
        $user = new User();
        $registerForm = $this->createForm(UserRegistrationType::class, $user);
        $registerForm->handleRequest($request);

        // set the referer in session for redirection after purchase
        if (!$registerForm->isSubmitted()) {
            $this->get('session')->remove(self::SESSION_REFERER_KEY);

            $referer = $request->headers->get('referer');
            if (strpos($referer, '/purchase') === false && strpos($referer, '/checkout') === false) {
                $this->get('session')->set(self::SESSION_REFERER_KEY, $referer);
            }
        }

        if ($registerForm->isSubmitted() && $registerForm->isValid()) {
            $sngClient = $this->get('sng_client');

            try {
                /** @var Account $account */
                $account = $sngClient->Account()->createAccount($user->getName(), $user->getEmail(), $user->getPassword());

                $user->setUuid($account->getUuid());
                $user->setEmail($account->getEmail());
                $user->setName($account->getFullName());
                $user->setConfirmedAt($account->getConfirmedAt());
                $user->setCreatedAt($account->getCreatedAt());
                $user->addRole(User::ROLE_SNG_USER);

                $token = new PostAuthenticationGuardToken($user, 'main', $user->getRoles());
                $this->get('security.token_storage')->setToken($token);
                $this->get('session')->set('_security_main', serialize($token));

                $dispatcher = $this->get('event_dispatcher');
                $userRegisteredEvent = new UserRegisteredEvent($account);
                $dispatcher->dispatch(UserRegisteredEvent::NAME, $userRegisteredEvent);

                return $this->redirectToRoute('homepage');
            } catch (AlreadyExistsException $e) {
                $registerForm->get('email')->addError(new FormError('E-mail already used.'));
            } catch (\InvalidArgumentException $e) {
                $registerForm->get('email')->addError(new FormError('Invalid e-mail address.'));
            } catch (\UnexpectedValueException $e) {
                $registerForm->get('email')->addError(new FormError('An error occurred. Please try again later.'));
            }
        }

        return $this->render('FrontEndBundle:Default:purchase.html.twig' , array(
            'registerForm' => $registerForm->createView(),
            'loginForm' => $loginForm->createView(),
            'services' => $services,
            'site' => $site,
            'globalMemberships' => $globalMemberships,
        ));
    }

    /**
     * @Route("/addToCart/", name="addToCart", options={"expose"=true})
     */
    public function addToCartAction(Request $request)
    {
        $serviceId = $request->request->get('serviceId');
        $siteUuid = $request->request->get('siteUuid');
        $membershipUuid = $request->request->get('membershipUuid');

        $sngClient = $this->get('sng_client');
        $user = $this->getUser();

        if (!empty($siteUuid)) {
            $site = $this->get('sng.locations')->getSiteByUuid($siteUuid);

            // check if account has client on this site
            $accountClients = $sngClient->Account()->getClientsByAccount($user->getUuid());
            // search for site ID
            $siteClient = null;
            /** @var \Sng\Model\Account\Client $client */
            foreach ($accountClients as $client) {
                if ($client->getSite()->getUuid() == $siteUuid) {
                    $siteClient = $client;
                    break;
                }
            }

            if (empty($siteClient)) {
                $clientsByEmail = $sngClient->Account()->getClientsByEmail($user->getEmail(), $site->getId());
                if (!empty($clientsByEmail)) {
                    return new JsonResponse(['siteUuid' => $siteUuid], Response::HTTP_NOT_ACCEPTABLE);
                } else {
                    /** @var Client */
                    $siteClient = $this->get('sng.accounts')->createClientForUser($site->getMbId(), $user);

                    // link the new client with the current account
                    $sngClient->Account()->linkClientToAccount($user->getUuid(), $siteClient->getUuid());
                }
            }
            // done checking and creating the user
        }

        if (empty($membershipUuid) &&
            ((empty($serviceId) && empty($siteUuid)) || (!empty($serviceId) && empty($siteUuid)))) {
            return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
        }

//        $cart = $this->get('session')->get(self::SESSION_CART_KEY);
//        if (empty($cart)) {
//            $cart = [];
//        }
        // we always empty the cart for now!
        $cart = [];

        // if we add a MB service
        if (!empty($serviceId)) {
            if (!isset($cart[$siteUuid])) {
                $cart[$siteUuid] = [];
            }
            $cart[$siteUuid][] = $serviceId;
        } else if (!empty($membershipUuid)) {
            // if we add a global SNG membership
            $cart['global'] = [ $membershipUuid ];
        }

        $this->get('session')->set(self::SESSION_CART_KEY, $cart);

        return new JsonResponse();
    }
}