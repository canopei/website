<?php

namespace SngBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sng\Exception\NotFoundException;
use Sng\Model\Account\AccountMembership;
use Sng\Model\Account\Client;
use Sng\Model\Account\ClientService;
use Sng\Model\Account\Service;
use Sng\Model\Sale\Membership;
use Sng\Model\ScheduledClass\Booking;
use Sng\Model\ScheduledClass\ScheduledClass;
use Sng\Model\Site\Site;
use SngBundle\Security\User\User;
use SngBundle\Service\ClassesService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use SngBundle\Form\UserLoginType;
use SngBundle\Form\UserRegistrationType;
use SngBundle\Service\SaleService;

class ClassesController extends Controller
{
    /**
     * @Route("/studio/{slug}/schedule/", name="studioSchedule")
     */
    public function scheduleByLocationAction(Request $request, $slug)
    {
        // login
        $authenticationUtils = $this->get('security.authentication_utils');
        $lastUsername = $authenticationUtils->getLastUsername();

        $loginForm = $this->createForm(UserLoginType::class, null, [
            'action' => $this->generateUrl('connect_sng_check'),
            'method' => 'POST',
            'data' => array('email' => $lastUsername),
        ]);

        // register
        $user = new User();
        $registerForm = $this->createForm(UserRegistrationType::class, $user);
        $registerForm->handleRequest($request);
        if ($registerForm->isSubmitted() && $registerForm->isValid()) {
            $sngClient = $this->get('sng_client');

            try {
                $account = $sngClient->Account()->createAccount($user->getName(), $user->getEmail(), $user->getPassword());

                $user->setUuid($account->getUuid());
                $user->setEmail($account->getEmail());
                $user->setName($account->getFullName());
                $user->setConfirmedAt($account->getConfirmedAt());
                $user->setCreatedAt($account->getCreatedAt());
                $user->addRole(User::ROLE_SNG_USER);

                $token = new PostAuthenticationGuardToken($user, 'main', $user->getRoles());
                $this->get('security.token_storage')->setToken($token);
                $this->get('session')->set('_security_main', serialize($token));

                $dispatcher = $this->get('event_dispatcher');
                $userRegisteredEvent = new UserRegisteredEvent($account);
                $dispatcher->dispatch(UserRegisteredEvent::NAME, $userRegisteredEvent);

                return $this->redirectToRoute('homepage');
            } catch (AlreadyExistsException $e) {
                $registerForm->get('email')->addError(new FormError('E-mail already used.'));
            } catch (\InvalidArgumentException $e) {
                $registerForm->get('email')->addError(new FormError('Invalid e-mail address.'));
            } catch (\UnexpectedValueException $e) {
                $registerForm->get('email')->addError(new FormError('An error occurred. Please try again later.'));
            }
        }

        $locationsService = $this->get('sng.locations');

        $location = $locationsService->getLocationBySlug($slug);
        if (!$location) {
            throw new NotFoundHttpException('Studio was not found.');
        }

        $now = new \DateTime();
        $startLimit = clone $now;
        $startLimit->setTime(0 ,0 ,0);
        $startLimit->add(new \DateInterval('P3M'));
        $startLimit->sub(new \DateInterval('P7D'));

        $reqStart = $request->query->get('from');

        if (!empty($reqStart)) {
            try {
                $startDatetime = new \DateTime($reqStart);
                $startDatetime->setTime(0, 0,0 );

                // We have a hard limit of 3 months
                if ($startDatetime > $startLimit) {
                    $startDatetime = null;
                }
            } catch (\Exception $e) {}
        }
        if (!isset($startDatetime) || $startDatetime < $now) {
            $startDatetime = clone $now;
            $startDatetime->setTime(0, 0, 0);
        }

        $endDatetime = clone $startDatetime;
        $endDatetime->setTime(23, 59, 59);
        $endDatetime->add(new \DateInterval('P7D'));

        // Use the cache only when we don't have the 'start' param
        $classes = $this->get('sng.classes')->getClasses(
            [
                ClassesService::FILTER_LOCATIONS_KEY => [$location->getUuid()],
                ClassesService::FILTER_START_DATETIME_KEY => $startDatetime->format('Y-m-d H:i:s'),
                ClassesService::FILTER_END_DATETIME_KEY => $endDatetime->format('Y-m-d H:i:s'),
            ],
            ['startDatetime' => 'asc'],
            200,
            0,
            empty($reqStart)
        );

        // group by day
        $classesByDay = [];
        $interval = \DateInterval::createFromDateString('1 day');
        $period = new \DatePeriod($startDatetime, $interval, $endDatetime);
        foreach ($period as $dt) {
            $classesByDay[$dt->format('d-m-Y')] = [];
        }
        /** @var ScheduledClass $class */
        foreach ($classes as $class) {
            try {
                $startDate = new \DateTime($class->getStartDatetime());
            } catch (\Exception $e) {
                continue;
            }
            $classesByDay[$startDate->format('d-m-Y')][] = $class;
        }

        $nextDatetime = clone $endDatetime;
        $nextDatetime->add(new \DateInterval('PT0H1S'));
        if ($nextDatetime >= $startLimit) {
            $nextDatetime = null;
        }

        $prevDatetime = null;
        if ($startDatetime > $now) {
            $prevDatetime = clone $startDatetime;
            $prevDatetime->sub(new \DateInterval('P8D'));
        }

        return $this->render('FrontEndBundle:Locations:schedule.html.twig', [
            'studio' => $location,
            'classes' => $classes,
            'classesByDay' => $classesByDay,
            'startDatetime' => $startDatetime,
            'endDatetime' => $endDatetime,
            'nextDatetime' => $nextDatetime,
            'prevDatetime' => $prevDatetime,
            'registerForm' => $registerForm->createView(),
            'loginForm' => $loginForm->createView(),
        ]);
    }

    /**
     * @Route("/class/{classUuid}/book/", name="book", options={"expose"=true})
     * @Method({"POST"})
     */
    public function bookAction(Request $request, $classUuid)
    {
        $sngClient = $this->get('sng_client');
        /** @var User $user */
        $user = $this->getUser();

        if ($request->isXmlHttpRequest()) {
            // get class details
            $classes = $sngClient->ScheduledClass()->getClassesByUUIDs([$classUuid]);
            if (empty($classes)) {
                return new JsonResponse(null, Response::HTTP_NOT_FOUND);
            }
            /** @var ScheduledClass $class */
            $class = $classes[0];

            try {
                $site = $this->get('sng.locations')->getSiteByUuid($class->getLocation()->getSiteUuid());
            } catch (NotFoundException $e) {
                return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
            }

            // check if account has client on class' site
            $accountClients = $sngClient->Account()->getClientsByAccount($user->getUuid());
            // search for site ID
            $siteClient = null;
            /** @var \Sng\Model\Account\Client $client */
            foreach ($accountClients as $client) {
                if ($client->getSiteId() == $class->getSiteId()) {
                    $siteClient = $client;
                    break;
                }
            }

            if (empty($siteClient)) {
                $clientsByEmail = $sngClient->Account()->getClientsByEmail($user->getEmail(), $class->getSiteId());
                if (!empty($clientsByEmail)) {
                    return new JsonResponse(['siteUuid' => $class->getLocation()->getSiteUuid()], Response::HTTP_NOT_ACCEPTABLE);
                } else {
                    /** @var Client $newClient */
                    $siteClient = $this->get('sng.accounts')->createClientForUser($site->getMbId(), $user);

                    // link the new client with the current account
                    $sngClient->Account()->linkClientToAccount($user->getUuid(), $siteClient->getUuid());
                }
            }

            // check class availability
            $classes = $sngClient->ScheduledClass()->getClassesByUUIDs([$classUuid], true);
            // maybe the class was removed from MB since the last sync
            if (empty($classes)) {
                return new JsonResponse(null, Response::HTTP_NOT_FOUND);
            }
            $class = $classes[0];

            $now = new \DateTime();
            if ($class->getStartDatetime() > $now) {
                return new JsonResponse(['reason' => 'The class already started.'], Response::HTTP_BAD_REQUEST);
            }
            if ($class->getIsCanceled() || !$class->getIsAvailable()) {
                return new JsonResponse(['reason' => 'The class was cancelled.'], Response::HTTP_BAD_REQUEST);
            }
            if (($class->getWebCapacity() > 0 && $class->getWebBooked() >= $class->getWebCapacity())
                || ($class->getTotalBooked() >= $class->getMaxCapacity() && !$class->getIsWaitlistAvailable())
            ) {
                return new JsonResponse(['reason' => 'The class is fully booked.'], Response::HTTP_BAD_REQUEST);
            }

            $currentServices = [];

            // get client memberships if the class program has a dropin service ID only
            if ($class->getProgramDropinServiceMbId() > 0) {
                $accountMemberships = $sngClient->Account()->getAccountMemberships($user->getUuid());
                /** @var AccountMembership $accountMembership */
                foreach ($accountMemberships as $accountMembership) {
                    if ($accountMembership->getRemaining() > 0) {
                        $currentServices[] = $accountMembership;
                    }
                    if ($accountMembership->getRemaining() == -1 && strtotime($accountMembership->getValidUntil()) > time()) {
                        $currentServices[] = $accountMembership;
                    }
                }
            }

            // get client services
            $clientServices = $sngClient->Account()->getClientServicesForClass($siteClient->getUuid(), $class->getUuid());
            /** @var ClientService $clientService */
            foreach ($clientServices as $clientService) {
                if ($clientService->getCurrent()) {
                    if (in_array(strtolower(trim($clientService->getName())), PurchaseController::SNG_DROPIN_PRICING_NAMES)) {
                        continue;
                    }
                    $clientService->setName(preg_replace(SaleService::SNG_PM_VALIDATOR, "$1", $clientService->getName()));

                    $currentServices[] = $clientService;
                }
            }

            $this->get('session')->set(PurchaseController::SESSION_PURCHASE_SITE_UUID, $site->getUuid());
            $this->get('session')->set(PurchaseController::SESSION_PURCHASE_CLASS_UUID, $classUuid);

            return new JsonResponse(['clientServices' => $currentServices]);
        }

        return new NotFoundHttpException();
    }

    /**
     * @Route("/class/{classUuid}/visit", name="addAccountToClass", options={"expose"=true})
     * @Method({"POST"})
     */
    public function addAccountToClassAction(Request $request, $classUuid)
    {
        $user = $this->getUser();
        $sngClient = $this->get('sng_client');

        $serviceId = $request->request->get('serviceId');
        $membershipId = $request->request->get('membershipId');

        if ($request->isXmlHttpRequest()) {
            if (empty($classUuid) || (empty($serviceId) && empty($membershipId))) {
                return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
            }

            // get the class
            $classes = $sngClient->ScheduledClass()->getClassesByUUIDs([$classUuid]);
            if (count($classes) == 0) {
                return new JsonResponse(null, Response::HTTP_NOT_FOUND);
            }
            /** @var ScheduledClass $class */
            $class = $classes[0];

            $this->get('session')->remove(PurchaseController::SESSION_PURCHASE_CLASS_UUID);

            $result = $this->get('sng.classes')->addAccountToClass($user, $class, $serviceId, $membershipId);
            if ($result) {
                return new JsonResponse(null, Response::HTTP_OK);
            } else {
                return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
            }
        }

        return new NotFoundHttpException();
    }

    /**
     * @Route("/class/{classUuid}/visit", name="cancelClassVisit", options={"expose"=true})
     * @Method({"DELETE"})
     */
    public function cancelClassVisitAction(Request $request, $classUuid)
    {
        $sngClient = $this->get('sng_client');
        /** @var User $user */
        $user = $this->getUser();

        if ($request->isXmlHttpRequest()) {
            // Fetch the class and resync it
            $classes = $sngClient->ScheduledClass()->getClassesByUUIDs([$classUuid], true);
            if (empty($classes)) {
                return new JsonResponse(null, Response::HTTP_NOT_FOUND);
            }
            /** @var ScheduledClass $class */
            $class = $classes[0];

            if ($class->getHideCancel()) {
                // setup a flash message, clear the cache and reload the page
                $this->get('sng.accounts')->invalidateAccountScheduleCache($user->getUuid());

                return new JsonResponse(null, Response::HTTP_CONFLICT);
            }

            $minutesUntilClass = (strtotime($class->getStartDatetime()) - time()) / 60;
            $isLateCancel = $minutesUntilClass <= $class->getLocation()->getLateCancelInterval();
            // we do not allow late cancels
            if ($isLateCancel) {
                return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
            }

            $sngClient->ScheduledClass()->removeAccountFromClass($user->getUuid(), $classUuid, $isLateCancel);

            // Fetch the booking from DB
            $booking = $sngClient->ScheduledClass()->getBookingByAccountAndClass($user->getUuid(), $class->getUuid());
            if ($booking instanceof Booking && $booking->getStatus() == 'active') {
                // refund the membership is one was used
                if ($booking->getAccountMembershipId() > 0) {
                    // fetch the services
                    $accountServices = $this->get('sng.accounts')->getAccountServices($user->getUuid());

                    // find the account membership
                    /** @var AccountMembership $accountMembership */
                    $accountMembership = null;
                    foreach ($accountServices as $accountService) {
                        if ($accountService instanceof AccountMembership && $accountService->getId() == $booking->getAccountMembershipId()) {
                            $accountMembership = $accountService;
                        }
                    }
                    if (empty($accountMembership)) {
                        $this->get('logger')->error("Failed to find the account membership used for booking " . $booking->getId() . ".");
                    } else {
                        if ($accountMembership->getRemaining() > -1) {
                            $accountMembership->setRemaining($accountMembership->getRemaining() + 1);
                            $sngClient->Account()->updateAccountMembership($accountMembership);
                        }
                    }

                    // expire the dropin service used in MB
                    if ($booking->getMembershipServiceId() > 0) {
                        $sngClient->Account()->expireAccountService($class->getLocation()->getSiteUuid(), $booking->getMembershipServiceId());
                    }
                }

                $booking->setStatus("canceled");
                $sngClient->ScheduledClass()->updateBooking($booking);
            }

            $this->get('sng.accounts')->invalidateAccountScheduleCache($user->getUuid());
            $this->get('sng.accounts')->invalidateAccountServicesCache($user->getUuid(), false, true, true, true);

            return new JsonResponse(null, Response::HTTP_NO_CONTENT);
        }

        return new NotFoundHttpException();
    }
}