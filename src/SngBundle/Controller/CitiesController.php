<?php

namespace SngBundle\Controller;

use Elastica\Exception\Connection\GuzzleException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sng\Model\Site\City;
use SngBundle\Form\SubscriptionType;
use SngBundle\Security\User\User;
use SngBundle\Service\ClassesService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use SngBundle\Service\StaffService;
use Canopei\IntercomBundle\Entity\Lead;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Form\FormError;
use SngBundle\Form\UserLoginType;
use SngBundle\Form\UserRegistrationType;
use Symfony\Component\Security\Guard\Token\PostAuthenticationGuardToken;

class CitiesController extends Controller
{
    /**
     * @Route("/city/{slug}/", name="cityHomepage")
     */
    public function homeAction($slug, Request $request)
    {
        // Redirect to homepage if already loggedin
        $authorizationChecker = $this->get('security.authorization_checker');

        // login
        $authenticationUtils = $this->get('security.authentication_utils');
        $exception = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        $loginForm = $this->createForm(UserLoginType::class, null, [
            'action' => $this->generateUrl('connect_sng_check'),
            'method' => 'POST',
            'data' => array('email' => $lastUsername),
        ]);

        // register
        $user = new User();
        $registerForm = $this->createForm(UserRegistrationType::class, $user);
        $registerForm->handleRequest($request);
        if ($registerForm->isSubmitted() && $registerForm->isValid()) {
            $sngClient = $this->get('sng_client');

            try {
                $account = $sngClient->Account()->createAccount($user->getName(), $user->getEmail(), $user->getPassword());

                $user->setUuid($account->getUuid());
                $user->setEmail($account->getEmail());
                $user->setName($account->getFullName());
                $user->setConfirmedAt($account->getConfirmedAt());
                $user->setCreatedAt($account->getCreatedAt());
                $user->addRole(User::ROLE_SNG_USER);

                $token = new PostAuthenticationGuardToken($user, 'main', $user->getRoles());
                $this->get('security.token_storage')->setToken($token);
                $this->get('session')->set('_security_main', serialize($token));

                $dispatcher = $this->get('event_dispatcher');
                $userRegisteredEvent = new UserRegisteredEvent($account);
                $dispatcher->dispatch(UserRegisteredEvent::NAME, $userRegisteredEvent);

                return $this->redirectToRoute('homepage');
            } catch (AlreadyExistsException $e) {
                $registerForm->get('email')->addError(new FormError('E-mail already used.'));
            } catch (\InvalidArgumentException $e) {
                $registerForm->get('email')->addError(new FormError('Invalid e-mail address.'));
            } catch (\UnexpectedValueException $e) {
                $registerForm->get('email')->addError(new FormError('An error occurred. Please try again later.'));
            }
        }


        $currentCity = null;
        $allCities = $this->get('sng.locations')->getAllCities();
        /** @var City $city */
        foreach ($allCities as $city) {
            if ($city->getSlug() == $slug) {
                $currentCity = $city;
                break;
            }
        }
        if (!$currentCity) {
            throw new NotFoundHttpException('City not found.');
        }

        $locationsService = $this->get('sng.locations');
        $locationsData = $locationsService->getLocationsByCityName($currentCity->getName(), 3);

        $currentCity->setIsEmpty(empty($locationsData['locations']));
        $locationsService->setCurrentCity($currentCity);

        $user = $this->get('security.token_storage')->getToken()->getUser();

        if (empty($locationsData['locations'])) {
            $form = $this->createForm(SubscriptionType::class, ['email' => $user instanceof User ? $user->getEmail() : '' ], array(
                'action' => $this->generateUrl('cityHomepage', array('slug'=> $slug)),
                'method' => 'POST',
                'attr' => array('id' => 'subscriptionForm'),
            ));

            if ($request->isMethod('POST')) {
                $form->handleRequest($request);
                if($form->isSubmitted() && $form->isValid()){
                    $formData= $form->getData();
                    try{
                        $this->get('canopei_intercom.leads')->createLead(new Lead($formData['email'], [
                            'citycomingsoon' => $currentCity->getName(),
                            'app_env' => $this->getParameter('kernel.environment')
                        ]));

                        $this->get('session')->getFlashBag()->add('msg', sprintf(
                            'Thank you. We will let you know when %s is launched.',
                            $currentCity->getName()
                        ));

                        return $this->redirectToRoute('cityHomepage', ['slug' => $slug]);
                    }catch(GuzzleException $e){
                        $form->get('email')->addError(new FormError('An error occurred. Please try again.'));
                    }
                    
                }
            }

            return $this->render('FrontEndBundle:Locations:cityComingSoon.html.twig', ['currentCity' => $city, 'form' => $form->createView()]);
        }

        $staffService = $this->get('sng.staff');
        $staffData = $staffService->getActiveStaff(
            [
                StaffService::FILTER_CITY_NAME_KEY => $currentCity->getName(),
                StaffService::FILTER_INCLUDE_NEXT_CLASSES => 1,
            ],
            4, 0, true
        );

        $classService = $this->get('sng.classes');
        $classes = $classService->getClasses([
            ClassesService::FILTER_CITY_KEY => [$currentCity->getName()]
        ], ['startDatetime' => 'asc'], 12, 0, true);

        return $this->render('FrontEndBundle:Locations:city.html.twig', [
            'currentCity' => $city,
            'studios' => $locationsData['locations'],
            'teachers' => $staffData['staff'],
            'classes' => $classes,
            'registerForm' => $registerForm->createView(),
            'loginForm' => $loginForm->createView(),
        ]);
    }
}
