<?php

namespace SngBundle\Controller\Authentication;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sng\Exception\AlreadyExistsException;
use SngBundle\Event\UserRegisteredEvent;
use SngBundle\Form\UserLoginType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use SngBundle\Security\User\User;
use SngBundle\Form\UserRegistrationType;
use Symfony\Component\Security\Guard\Token\PostAuthenticationGuardToken;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormError;

class AuthController extends Controller
{
    /**
     * @Route("/login", name="login")
     */
    public function loginAction(Request $request)
    {
        // Redirect to homepage if already loggedin
        $authorizationChecker = $this->get('security.authorization_checker');
        if ($authorizationChecker->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirectToRoute('homepage');
        }

        // login
        $authenticationUtils = $this->get('security.authentication_utils');
        $exception = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        $loginForm = $this->createForm(UserLoginType::class, null, [
            'action' => $this->generateUrl('connect_sng_check'),
            'method' => 'POST',
            'data' => array('email' => $lastUsername),
        ]);

        // register
        $user = new User();
        $registerForm = $this->createForm(UserRegistrationType::class, $user);
        $registerForm->handleRequest($request);
        if ($registerForm->isSubmitted() && $registerForm->isValid()) {
            $sngClient = $this->get('sng_client');

            try {
                $account = $sngClient->Account()->createAccount($user->getName(), $user->getEmail(), $user->getPassword());

                $user->setUuid($account->getUuid());
                $user->setEmail($account->getEmail());
                $user->setName($account->getFullName());
                $user->setConfirmedAt($account->getConfirmedAt());
                $user->setCreatedAt($account->getCreatedAt());
                $user->addRole(User::ROLE_SNG_USER);

                $token = new PostAuthenticationGuardToken($user, 'main', $user->getRoles());
                $this->get('security.token_storage')->setToken($token);
                $this->get('session')->set('_security_main', serialize($token));

                $dispatcher = $this->get('event_dispatcher');
                $userRegisteredEvent = new UserRegisteredEvent($account);
                $dispatcher->dispatch(UserRegisteredEvent::NAME, $userRegisteredEvent);

                return $this->redirectToRoute('homepage');
            } catch (AlreadyExistsException $e) {
                $registerForm->get('email')->addError(new FormError('E-mail already used.'));
            } catch (\InvalidArgumentException $e) {
                $registerForm->get('email')->addError(new FormError('Invalid e-mail address.'));
            } catch (\UnexpectedValueException $e) {
                $registerForm->get('email')->addError(new FormError('An error occurred. Please try again later.'));
            }
        }

        return $this->render('SngBundle:Security:login.html.twig', array(
            'last_username' => $lastUsername,
            'error' => $exception ? $exception->getMessage() : null,
            'registerForm' => $registerForm->createView(),
            'loginForm' => $loginForm->createView(),
        ));
    }
}
