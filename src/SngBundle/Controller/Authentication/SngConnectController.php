<?php

namespace SngBundle\Controller\Authentication;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SngConnectController extends Controller
{
    /**
     * @Route("/connect/sng-check", name="connect_sng_check")
     */
    public function connectGenericActionCheck()
    {
        // will not be reached!
    }
}
