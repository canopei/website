<?php

namespace SngBundle\Controller\Authentication;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class GoogleConnectController extends Controller
{
    /**
     * @Route("/connect/google", name="connect_google")
     */
    public function connectGoogleAction(Request $request)
    {
        // redirect to Google
        $googleOAuthProvider = $this->get('sng.auth.google_provider');
        $url = $googleOAuthProvider->getAuthorizationUrl();

        return $this->redirect($url);
    }

    /**
     * @Route("/connect/google-check", name="connect_google_check")
     */
    public function connectGoogleActionCheck()
    {
        // will not be reached!
    }
}
