<?php

namespace SngBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sng\Exception\AlreadyExistsException;
use Sng\Exception\NotFoundException;
use Sng\Model\Account\Account;
use Sng\Model\Site\Location;
use SngBundle\Event\UserRegisteredEvent;
use SngBundle\Service\ClassesService;
use SngBundle\Service\GoogleTagManager\StudioDetailsEvent;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Response;
use SngBundle\Form\UserLoginType;
use SngBundle\Security\User\User;
use SngBundle\Form\UserRegistrationType;
use Symfony\Component\Security\Guard\Token\PostAuthenticationGuardToken;
use Sng\Model\Site\Company;
use Sng\Model\Site\Site;
use Symfony\Component\HttpFoundation\Session\Session;

class LocationsController extends Controller
{
    const LOCATIONS_PER_PAGE = 10;

    /**
     * @Route("/studio/{slug}/", name="studioDetails", options={"expose"=true})
     */
    public function viewAction($slug, Request $request)
    {
        // login
        $authenticationUtils = $this->get('security.authentication_utils');
        $lastUsername = $authenticationUtils->getLastUsername();

        $loginForm = $this->createForm(UserLoginType::class, null, [
            'action' => $this->generateUrl('connect_sng_check'),
            'method' => 'POST',
            'data' => array('email' => $lastUsername),
        ]);

        // register
        $user = new User();
        $registerForm = $this->createForm(UserRegistrationType::class, $user);
        $registerForm->handleRequest($request);
        if ($registerForm->isSubmitted() && $registerForm->isValid()) {
            $sngClient = $this->get('sng_client');

            try {
                /** @var Account $account */
                $account = $sngClient->Account()->createAccount($user->getName(), $user->getEmail(), $user->getPassword());

                $user->setUuid($account->getUuid());
                $user->setEmail($account->getEmail());
                $user->setName($account->getFullName());
                $user->setConfirmedAt($account->getConfirmedAt());
                $user->setCreatedAt($account->getCreatedAt());
                $user->addRole(User::ROLE_SNG_USER);

                $token = new PostAuthenticationGuardToken($user, 'main', $user->getRoles());
                $this->get('security.token_storage')->setToken($token);
                $this->get('session')->set('_security_main', serialize($token));

                $dispatcher = $this->get('event_dispatcher');
                $userRegisteredEvent = new UserRegisteredEvent($account);
                $dispatcher->dispatch(UserRegisteredEvent::NAME, $userRegisteredEvent);

                return $this->redirectToRoute('homepage');
            } catch (AlreadyExistsException $e) {
                $registerForm->get('email')->addError(new FormError('E-mail already used.'));
            } catch (\InvalidArgumentException $e) {
                $registerForm->get('email')->addError(new FormError('Invalid e-mail address.'));
            } catch (\UnexpectedValueException $e) {
                $registerForm->get('email')->addError(new FormError('An error occurred. Please try again later.'));
            }
        }



        $locationsService = $this->get('sng.locations');
        /** @var Location $location */
        $location = $locationsService->getLocationBySlug($slug);
        if (!$location) {
            throw new NotFoundHttpException('Studio was not found.');
        }

        $now = new \DateTime();
        $endOfTheDay = clone $now;
        $endOfTheDay->modify('tomorrow');
        $endOfTheDay->modify('1 second ago');
        $todaysClasses = $this->get('sng.classes')->getClasses(
            [
                ClassesService::FILTER_LOCATIONS_KEY => [$location->getUuid()],
                ClassesService::FILTER_START_DATETIME_KEY => $now->format('Y-m-d H:i:s'),
                ClassesService::FILTER_END_DATETIME_KEY => $endOfTheDay->format('Y-m-d H:i:s'),
            ],
            ['startDatetime' => 'asc'],
            12,
            0,
            true
        );

        $staff = $this->get('sng.staff')->getActiveStaffByLocation(
            $location->getUuid(),
            ['featured' => 'desc']
        );

        $saleService = $this->get('sng.sale');
        $siteServices = $saleService->getSngSaleServicesForSite($location->getSiteUuid());

        // Check if the site has setup in MB the plans for Global SNG Drop In (on every program)
        $hasGlobalDropInPlan = $this->get('sng.classes')->hasSiteGlobalDropInPlan($location->getSiteUuid());

        // GTM
        $gtmObject = (new StudioDetailsEvent())
            ->setStudioName($location->getName())
            ->setClassesNo(count($todaysClasses))
            ->setTeachersNo(count($staff))
            ->setCityName($location->getCity())
            ->setIsFavourite($this->get('sng.favourites.locations')->isItemFavourite($location->getUuid()));
        $this->get('sng.gtm.datalayer')->add($gtmObject);

        return $this->render('FrontEndBundle:Locations:locationStudio.html.twig', [
            'studio' => $location,
            'classes' => $todaysClasses,
            'teachers' => $staff,
            'siteServices' => $siteServices,
            'hasGlobalDropInPlan' => $hasGlobalDropInPlan,
            'registerForm' => $registerForm->createView(),
            'loginForm' => $loginForm->createView(),
        ]);
    }

    /**
     * @Route("/studios/", name="studiosList", options={"expose"=true})
     */
    public function listAction(Request $request)
    {
        $page = $request->query->get('page', 1);
        $offset = ($page - 1) * self::LOCATIONS_PER_PAGE;

        $locationsService = $this->get('sng.locations');
        $locationsData = $locationsService->getLocations(
            [],
            ['featured' => 'desc'],
            self::LOCATIONS_PER_PAGE,
            $offset,
            $page === 1
        );
        $pages = ceil($locationsData['total'] / self::LOCATIONS_PER_PAGE);

        if (count($locationsData['locations']) == 0) {
            throw new NotFoundHttpException('Studios page not found.');
        }

        if ($request->isXmlHttpRequest()) {
            return new JsonResponse([
                'content' => $this->get('templating')->render('FrontEndBundle:Locations/partials:studiosByCitySimpleList.html.twig', [
                    'studios' => $locationsData['locations']
                ])
            ], Response::HTTP_OK);
        }

        $mapLocations = $locationsService->getAllLocationsForMap();

        return $this->render('FrontEndBundle:Locations:studios.html.twig', array(
            'studios' => $locationsData['locations'],
            'currentPage' => $page,
            'totalPages' => $pages,
            'mapLocations' => $mapLocations,
        ));
    }

    /**
     * @Route("/studios/{citySlug}/", name="studiosListByCity", options={"expose"=true})
     */
    public function listByCityAction(Request $request, $citySlug)
    {
        $locationsService = $this->get('sng.locations');

        $currentCity = null;
        $allCities = $locationsService->getAllCities();
        foreach ($allCities as $city) {
            if ($city->getSlug() == $citySlug) {
                $currentCity = $city;
                break;
            }
        }
        if (!$currentCity) {
            throw new NotFoundHttpException('City not found.');
        }

        $page = $request->query->get('page', 1);
        $offset = ($page - 1) * self::LOCATIONS_PER_PAGE;

        $locationsData = $locationsService->getLocationsByCityName($currentCity->getName(), self::LOCATIONS_PER_PAGE, $offset, true);
        $pages = ceil($locationsData['total'] / self::LOCATIONS_PER_PAGE);

        if (count($locationsData['locations']) == 0) {
            throw new NotFoundHttpException('Studios page not found.');
        }

        if ($request->isXmlHttpRequest()) {
            return new JsonResponse([
                'content' => $this->get('templating')->render('FrontEndBundle:Locations/partials:studiosByCitySimpleList.html.twig', [
                    'studios' => $locationsData['locations']
                ])
            ], Response::HTTP_OK);
        }

        $mapLocations = $locationsService->getAllLocationsForMap($currentCity->getName());

        return $this->render('FrontEndBundle:Locations:studios.html.twig', array(
            'city' => $currentCity,
            'studios' => $locationsData['locations'],
            'currentPage' => $page,
            'totalPages' => $pages,
            'mapLocations' => $mapLocations,
        ));
    }

    /**
     * @Route("/studios/{locationUuid}/favourites/", name="studioAddToFavourites", options={"expose"=true})
     * @Method({"POST"})
     */
    public function addToFavouritesAction($locationUuid)
    {
        $locationFavService = $this->get('sng.favourites.locations');
        $ok = $locationFavService->addItem($locationUuid);

        return new Response(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @Route("/studios/{locationUuid}/favourites/", name="studioRemoveFromFavourites", options={"expose"=true})
     * @Method({"DELETE"})
     */
    public function removeFromFavourites($locationUuid)
    {
        $locationFavService = $this->get('sng.favourites.locations');
        $ok = $locationFavService->removeItem($locationUuid);

        return new Response(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @Route("/activate-studio/", name="activateStudio")
     */
    public function activateStudioAction(Request $request)
    {
        $data = array();
        $form = $this->createFormBuilder($data, array(
            'action' => $this->generateUrl('activateStudio'),
        ))
            ->add('mbId', NumberType::class, [
                'invalid_message' => 'Please insert a valid (numeric) Studio ID.'
            ])
            ->add('submit', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        /** @var FormInterface $form */
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $data = $form->getData();
                try {
                    $response = $this->get('sng_client')->Site()->getActivationCode((int)$data['mbId']);
                } catch (NotFoundException $e) {
                    $this->get('session')->getFlashBag()->add('msg', 'This Studio ID was not found in MINDBODY.');
                } catch (\Exception $e) {
                    $this->get('session')->getFlashBag()->add('msg', 'An error occurred. Please try again.');
                }

                if (isset($response)) {
                    if (!isset($response['activationLink'])) {
                        $this->get('session')->getFlashBag()->add('msg', 'An error occurred. Please try again.');
                    } else {
                        return $this->redirect($response['activationLink']);
                    }
                }
            } else {
                $this->get('session')->getFlashBag()->add('msg', $form->getErrors(true)->current()->getMessage());
            }
        }

        return $this->render('FrontEndBundle:Default:activateStudio.html.twig', [ 'activationForm' => $form->createView() ]);
    }

    /**
     * @Route("/connected-sites/", name="connectedSites")
     */
    public function connectedSitesAction()
    {
        $env = $this->getParameter('kernel.environment');
        if ($env == 'prod') {
            throw $this->createNotFoundException('Page not found.');
        }

        $sites = $this->get('sng_client')->Site()->getConnectedSites();

        return $this->render('@FrontEnd/Locations/connectedSites.html.twig', ['sites' => $sites]);
    }



    /**
     * @Route("/schedule-form/", name="scheduleForm")
     */
    public function scheduleFormAction(Request $request)
    {
        $form = $this->createForm('SngBundle\Form\ScheduleFormType', null, array(
            'action' => $this->generateUrl('scheduleForm'),
            'method' => 'POST',
            'attr' => array('id' => 'form'),
        ));
        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            $data = $form->getData();

            $this->get('session')->set('name', $data['name']);
            $this->get('session')->set('current_website', $data['current_website']);
            $this->get('session')->set('entity', $data['entity']);
            $this->get('session')->set('address', $data['address']);
            $this->get('session')->getFlashBag()->add('name', $data['name']);
            $this->get('session')->getFlashBag()->add('current_website', $data['current_website']);
            return $this->redirectToRoute('scheduleTerms', array());
        }
        return $this->render('FrontEndBundle:Default:scheduleForm.html.twig', array(
            'form' => $form->createView(),
        ));
    }


    /**
     * @Route("/schedule-calendar/", name="scheduleCalendar")
     */
    public function scheduleCalendarAction(Request $request)
    {
        $name = $this->get('session')->get('name');
        $current_website = $this->get('session')->get('current_website');
        $address = $this->get('session')->get('address');
        $entity = $this->get('session')->get('entity');
        $timestamp = date("Y-m-d H:i:s");
        $addString = $current_website.",".$name.",".$entity.",".$address.",".$timestamp."\r\n";
        $filename = 'documents.txt';
        $file = fopen($filename, 'a+');
        //$contents = fread($file, filesize($filename));
        $contents = $addString;
        if($name != "" && $current_website != "")
        {
            $written = fwrite($file , $contents);
        }
        fclose($file);
        $this->get('session')->remove('name');
        $this->get('session')->remove('current_website');
        $this->get('session')->remove('address');
        $this->get('session')->remove('entity');
        return $this->render('FrontEndBundle:Default:scheduleCalendar.html.twig');
    }


    /**
     * @Route("/schedule-terms/", name="scheduleTerms")
     */
    public function scheduleTermsAction(Request $request)
    {
        return $this->render('FrontEndBundle:Default:schedule-terms.html.twig', array());
    }

    /**
     * @Route("/studio/{siteUuid}/addToPurchase/", name="addToPurchase", options={"expose"=true})
     * @Method({"POST"})
     */
    public function addToPurchaseAction(Request $request, $siteUuid)
    {
        if ($request->isXmlHttpRequest()) {
            $site = $this->get('sng.locations')->getSiteByUuid($siteUuid);
            if (!$site instanceof Site) {
                throw new NotFoundHttpException('Site not found.');
            }

            $this->get('session')->set(PurchaseController::SESSION_PURCHASE_SITE_UUID, $siteUuid);

            return new JsonResponse();
        }

        return new NotFoundHttpException();
    }
}
