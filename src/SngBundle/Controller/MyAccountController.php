<?php

namespace SngBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sng\Model\Account\AccountMembership;
use Sng\Model\Account\Service;
use SngBundle\Form\NewPasswordType;
use SngBundle\Form\PersonalInfoType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;

class MyAccountController extends Controller
{

    /**
     * @Route("/account/home/", name="myaccount")
     */
    public function myAccountAction(Request $request)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();

        /* CHANGE PASSWORD UPDATE */

        $formPass = $this->createForm(NewPasswordType::class, null, array(
            'action' => $this->generateUrl('myaccount'),
            'method' => 'POST',
            'attr' => array('id' => 'changePassForm'),
        ));
        $formPass->handleRequest($request);
        if ($formPass->isSubmitted() && $formPass->isValid()) {
            $data = $formPass->getData();

            try {
                $this->get('sng_client')->Account()->changePassword($user->getUuid(), $data['new_password'], $data['old_password']);
                $this->get('session')->getFlashBag()->add('msg', 'Thanks, your personal password is changed!');
            } catch (\InvalidArgumentException $e) {
                $error = new FormError("The current password is not correct.");
                $formPass->get('old_password')->addError($error);
            } catch (\Exception $e) {
                $error = new FormError("An error occurred. Please try again.");
                $formPass->get('old_password')->addError($error);
            }
        }

        /* PERSONAL INFORMATION UPDATE */
        $form = $this->createForm(PersonalInfoType::class, null, array(
            'action' => $this->generateUrl('myaccount'),
            'method' => 'POST',
            'attr' => array('id' => 'changeInfoForm'),
            'data' => array('name' => $user->getName(), 'email' => $user->getEmail()),
        ));

        return $this->render('FrontEndBundle:Account:myaccount.html.twig', array(
            'formInfo' => $form->createView(),
            'formPass' => $formPass->createView(),
        ));
    }

    /**
     * @Route("/account/favorites/", name="myfavorites")
     */
    public function myFavoritesAction()
    {
        $locationFavService = $this->get('sng.favourites.locations');
        $locations = $locationFavService->getItems();

        $staffFavService = $this->get('sng.favourites.staff');
        $staff = $staffFavService->getItems();

        return $this->render('FrontEndBundle:Account:myfavorites.html.twig', array(
            'studios' => $locations,
            'teachers' => $staff,
        ));
    }

    /**
     * @Route("/account/memberships/", name="memberships")
     */
    public function membershipsAction()
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $services = $this->get('sng.accounts')->getAccountServices($user->getUuid(), false, true, true);

        $activeServices = $pastServices = [];
        foreach ($services as $service) {
            if ($service instanceof Service) {
                if ($service->getCurrent()) {
                    $activeServices[] = $service;
                } else {
                    $pastServices[] = $service;
                }
            } else if ($service instanceof AccountMembership) {
                if ($service->getRemaining() === 0) {
                    $pastServices[] = $service;
                } elseif ($service->getRemaining() == -1) {
                    $validUntil = strtotime($service->getValidUntil());

                    if ($validUntil > time()) {
                        $activeServices[] = $service;
                    } else {
                        $pastServices[] = $service;
                    }
                } else {
                    $activeServices[] = $service;
                }
            }
        }

        return $this->render('FrontEndBundle:Account:memberships.html.twig', [
            'activeServices' => $activeServices,
            'pastServices' => $pastServices,
        ]);
    }

    /**
     * @Route("/account/history/", name="history")
     */
    public function historyAction()
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $visits = $this->get('sng.accounts')->getAccountVisits($user->getUuid(), 15, 0, true);
        $schedule = $this->get('sng.accounts')->getAccountSchedule($user->getUuid(), null, true);

        return $this->render('FrontEndBundle:Account:history.html.twig', [
            'visits' => $visits,
            'schedule' => $schedule,
        ]);
    }

    /**
     * @Route("/account/link/", name="account_link_list", options={"expose"=true})
     * @Method({"GET"})
     */
    public function linkAccountAction()
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $foundClients = $this->get('sng_client')->Account()->getClientsByEmail($user->getEmail());
        $linkedClients = $this->get('sng_client')->Account()->getClientsByAccount($user->getUuid());

        $links = [];
        foreach ($foundClients as $foundClient) {
            $link = ['client' => $foundClient, 'clientLinked' => false, 'siteLinked' => false];

            foreach ($linkedClients as $linkedClient) {
                if ($foundClient->getUuid() == $linkedClient->getUuid()) {
                    $link['clientLinked'] = true;
                    break;
                }
                if ($foundClient->getSiteId() == $linkedClient->getSiteId()) {
                    $link['siteLinked'] = true;
                    break;
                }
            }

            $links[] = $link;
        }

        return $this->render('FrontEndBundle:Account:mylinks.html.twig', array(
            'links' => $links,
        ));
    }

    /**
     * @Route("/account/link/", name="account_link_add", options={"expose"=true})
     * @Method({"POST"})
     */
    public function linkClientToAccountAction(Request $request) {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $clientUuid = $request->request->get('clientUuid');

        $this->get('sng_client')->Account()->linkClientToAccount($user->getUuid(), $clientUuid);

        return new Response(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @Route("/account/link/", name="account_link_remove", options={"expose"=true})
     * @Method({"DELETE"})
     */
    public function removeClientFromAccountAction(Request $request) {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $clientUuid = $request->request->get('clientUuid');

        $this->get('sng_client')->Account()->unlinkClientFromAccount($user->getUuid(), $clientUuid);

        return new Response(null, Response::HTTP_NO_CONTENT);
    }
}
