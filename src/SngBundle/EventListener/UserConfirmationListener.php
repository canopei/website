<?php

namespace SngBundle\EventListener;

use SngBundle\Event\UserConfirmedEvent;
use SngBundle\Service\EmailService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class UserConfirmationListener implements EventSubscriberInterface
{
    const FROM_CONFIRMATION_FLASHBAG_KEY = 'fromConfirmation';

    protected $emailService;
    protected $session;

    public function __construct(EmailService $emailService, SessionInterface $session)
    {
        $this->emailService = $emailService;
        $this->session = $session;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            UserConfirmedEvent::NAME => [
                [ 'onUserConfirmationSendWelcomeEmail' ],
                [ 'onUserConfirmationSetFlashMessage' ]
            ]
        ];
    }

    public function onUserConfirmationSendWelcomeEmail(UserConfirmedEvent $event)
    {
        $account = $event->getUser();
        $this->emailService->sendWelcomeEmail($account);
    }

    public function onUserConfirmationSetFlashMessage(UserConfirmedEvent $event)
    {
        $this->session->getFlashBag()->set(self::FROM_CONFIRMATION_FLASHBAG_KEY, true);
        $this->session->getFlashBag()->set('msg', 'Account confirmed! Thanks for joining SweatNGlow.');
    }
}
