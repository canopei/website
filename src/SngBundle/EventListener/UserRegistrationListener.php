<?php

namespace SngBundle\EventListener;

use Canopei\IntercomBundle\Entity\User;
use Canopei\IntercomBundle\Service\UsersService;
use Elastica\Exception\Connection\GuzzleException;
use SngBundle\Event\UserRegisteredEvent;
use SngBundle\Service\EmailService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class UserRegistrationListener implements EventSubscriberInterface
{
    const FROM_REGISTRATION_FLASHBAG_KEY = 'fromRegistration';

    protected $emailService;
    protected $session;
    protected $intercomUsersService;
    protected $env;

    public function __construct(
        EmailService $emailService,
        SessionInterface $session,
        UsersService $intercomUsersService,
        $env
    ) {
        $this->emailService = $emailService;
        $this->session = $session;
        $this->intercomUsersService = $intercomUsersService;
        $this->env = $env;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            UserRegisteredEvent::NAME => [
                [ 'onUserRegistrationSendEmail' ],
                [ 'onUserRegistrationSetFlashMessage' ],
                [ 'onUserRegistrationAddToIntercom' ]
            ]
        ];
    }

    public function onUserRegistrationSendEmail(UserRegisteredEvent $event)
    {
        $account = $event->getUser();

        // Send only if we have a confirmation code.
        // Social registrations don't have a confirmation code because is not needed.
        if (!empty($account->getConfirmationCode())) {
            $this->emailService->sendConfirmationEmail($account);
        } else {
            // If a user registered and has no confirmation code, then must be social registration
            // and we'll send the welcome e-mail.
            $this->emailService->sendWelcomeEmail($account);
        }
    }

    public function onUserRegistrationSetFlashMessage(UserRegisteredEvent $event)
    {
        $account = $event->getUser();

        $this->session->getFlashBag()->set(self::FROM_REGISTRATION_FLASHBAG_KEY, true);

        // For social login accounts we don't need confirmation
        if (!empty($account->getConfirmationCode())) {
//            $this->session->getFlashBag()->set('msg', 'Thank for registering! You will receive an email shortly to confirm your account.');
        } else {
            $this->session->getFlashBag()->set('msg', 'Thanks for joining SweatNGlow.');
        }
    }

    public function onUserRegistrationAddToIntercom(UserRegisteredEvent $event)
    {
        $account = $event->getUser();
        $now = new \DateTime();

        try {
            $this->intercomUsersService->createUser(new User(
                $account->getUuid(),
                $account->getEmail(),
                $account->getFullName(),
                $now,
                $now,
                [ 'app_env' => $this->env ]
            ));
        } catch (GuzzleException $e) {}
    }
}
