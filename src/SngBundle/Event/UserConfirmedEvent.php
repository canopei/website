<?php

namespace SngBundle\Event;

use Sng\Model\Account\Account;
use Symfony\Component\EventDispatcher\Event;

class UserConfirmedEvent extends Event
{
    const NAME = 'user.confirmed';

    protected $user;

    public function __construct(Account $user)
    {
        $this->user = $user;
    }

    public function getUser()
    {
        return $this->user;
    }
}