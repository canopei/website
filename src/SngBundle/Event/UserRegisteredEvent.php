<?php

namespace SngBundle\Event;

use Sng\Model\Account\Account;
use Symfony\Component\EventDispatcher\Event;

class UserRegisteredEvent extends Event
{
    const NAME = 'user.registered';

    protected $user;

    public function __construct(Account $user)
    {
        $this->user = $user;
    }

    public function getUser()
    {
        return $this->user;
    }
}