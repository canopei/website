<?php

namespace SngBundle\Twig;

class StringDateExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('string_date', array($this, 'processFilter'))
        );
    }

    public function processFilter($date, $dateFormat = 'd-m-Y', $timeFormat = 'G:i')
    {
        $today = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
        $today_end = mktime(23, 59, 59, date('m'), date('d'), date('Y'));

        $u_date = strtotime($date);
        $nextday = $today_end + 1;

        $formattedDate = date($dateFormat, $u_date);
        if($u_date >= $nextday && $u_date < $nextday + (3600*24)) {
            $formattedDate = 'tomorrow';
        } elseif ($u_date >= $today && $u_date < $today_end){
            $formattedDate = 'today';
        } elseif($u_date < $today && $u_date > $today - (3600*24)) {
            $formattedDate = 'yesterday';
        }

        return $formattedDate.' '.date($timeFormat, $u_date);
    }
}