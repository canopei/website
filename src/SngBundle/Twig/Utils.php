<?php

namespace SngBundle\Twig;

class Utils extends \Twig_Extension
{
    public function getTests()
    {
        return array(
            new \Twig_Test('instanceof', [$this, 'isInstanceOf']),
            new \Twig_Test('simpletext', [$this, 'isSimpleText']),
        );
    }

    public function isInstanceOf($object, $class)
    {
        try {
            $reflectionClass = new \ReflectionClass($class);
        } catch (\ReflectionException $e) {
            return false;
        }
        return $reflectionClass->isInstance($object);
    }

    public function isSimpleText($string)
    {
        return preg_match('#^[\w\d\s\.,;\-]+$#i', $string);
    }
}