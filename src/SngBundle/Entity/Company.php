<?php

namespace SngBundle\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="company_enroll")
 */
class Company
{
    private $id;
    private $name;
    private $company_name;
    private $hasAccepted;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCompanyName()
    {
        return $this->company_name;
    }

    /**
     * @param mixed $company_name
     */
    public function setCompanyName($company_name)
    {
        $this->company_name = $company_name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHasAccepted()
    {
        return $this->hasAccepted;
    }

    /**
     * @param mixed $hasAccepted
     */
    public function setHasAccepted($hasAccepted)
    {
        $this->hasAccepted = $hasAccepted;
        return $this;
    }

    public static function fromApi($data)
    {
        $company = new self();
        $company
            ->setId($data['id'])
            ->setName($data['name'])
            ->setCompanyName($data['company_name'])
            ->setHasAccepted($data['hasAccepted'])
        ;
        return $company;
    }
}