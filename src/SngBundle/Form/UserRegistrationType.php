<?php

namespace SngBundle\Form;

use SngBundle\Security\User\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class UserRegistrationType extends AbstractType
{
    private $router;

    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setAction($this->router->generate('login'))
            ->setMethod('POST')
            ->add('email', EmailType::class, [
                'label' => false,
                'required' => true,
            ])
            ->add('password', PasswordType::class, [
                'label' => false,
                'required' => true,
            ])
            ->add('name', TextType::class, [
                'label' => false,
                'required' => true,
            ])
            // ->add('termsAccepted', CheckboxType::class, array(
            //     'mapped' => false,
            //     'constraints' => new IsTrue(),
            // ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => User::class,
        ));
    }
}
