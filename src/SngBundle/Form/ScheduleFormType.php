<?php 
namespace SngBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ScheduleFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('current_website', TextType::class, array('attr' => array('placeholder' => 'Current Website', 'class'=> 'text-field', 'id'=> 'curentWebsiteInput')))
            ->add('name', TextType::class, array('attr' => array('placeholder' => 'Studio owner', 'class'=> 'text-field', 'id'=> 'nameInput')))
            ->add('entity', TextareaType::class, array('attr' => array('placeholder' => 'If Studio Owner is an entity, please identify by legal name, type (corporation, LLC, partnership), state of incorporation or organization, and any d/b/a name.', 'class'=> 'message-field', 'id'=> 'entityInput')))
            ->add('address', TextType::class, array('attr' => array('placeholder' => 'Address of Record', 'class'=> 'text-field', 'id'=> 'addressInput')))
        ;
    }

    public function setDefaultOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'validation_groups' => false,
        ));
    }

    public function getName()
    {
        return 'form';
    }
}