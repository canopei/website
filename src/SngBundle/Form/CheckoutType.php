<?php 
namespace SngBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

class CheckoutType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class, array('attr' => array('placeholder' => 'First Name', 'class'=> 'half-text-field', 'id'=> 'firstNameInput')))
            ->add('lastName', EmailType::class, array('attr' => array('placeholder' => 'Last Name', 'class'=> 'half-text-field', 'id'=> 'lastNameInput')))
            ->add('country', TextType::class, array('attr' => array('value' => 'United States','placeholder' => 'Country', 'class'=> 'text-field hidden', 'id'=> 'countryInput')))
            ->add('address', TextType::class, array('attr' => array('placeholder' => 'Address', 'class'=> 'text-field', 'id'=> 'addressInput')))
            ->add('address2', TextType::class, array('attr' => array('placeholder' => 'Address2', 'class'=> 'text-field', 'id'=> 'address2Input')))
            ->add('city', TextType::class, array('attr' => array('placeholder' => 'City', 'class'=> 'text-field', 'id'=> 'cityInput')))
            ->add('state', TextType::class, array('attr' => array('placeholder' => 'State', 'class'=> 'text-field', 'id'=> 'stateInput')))
            ->add('zip', TextType::class, array('attr' => array('placeholder' => 'Zip', 'class'=> 'text-field', 'id'=> 'zipInput')))
            ->add('cardNumber', TextType::class, array('attr' => array('placeholder' => 'Card number', 'class'=> 'text-field', 'id'=> 'cardNumberInput')))
            ->add('month', TextType::class, array('attr' => array('placeholder' => 'MM', 'class'=> 'half-text-field', 'id'=> 'monthInput')))
            ->add('year', TextType::class, array('attr' => array('placeholder' => 'YYYY', 'class'=> 'half-text-field', 'id'=> 'yearInput')))
            ->add('ccv', TextType::class, array('attr' => array('placeholder' => 'CCV', 'class'=> 'half-text-field', 'id'=> 'ccvInput')))
        ;
    }

    public function setDefaultOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'validation_groups' => false,
        ));
    }

    public function getName()
    {
        return 'checkout_form';
    }
}