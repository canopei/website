<?php 
namespace SngBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

class NewPasswordType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('old_password', PasswordType::class, array('attr' => array('placeholder' => 'Old password', 'class'=> 'text-field', 'id'=> 'oldPassInput')))
            ->add('new_password', PasswordType::class, array('attr' => array('placeholder' => 'New password', 'class'=> 'text-field', 'id'=> 'newPassinput')))
            ->add('confirm_password', PasswordType::class, array('attr' => array('placeholder' => 'Confirm password', 'class'=> 'text-field', 'id'=> 'newPassinput')))
        ;
    }

    public function setDefaultOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'validation_groups' => false,
        ));
    }

    public function getName()
    {
        return 'change_password_form';
    }
}