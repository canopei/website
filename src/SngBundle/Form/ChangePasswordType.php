<?php

namespace SngBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Validator\Constraints\Length;

class ChangePasswordType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('password', RepeatedType::class, array(
                'type' => PasswordType::class,
                'invalid_message' => 'The password fields must match.',
                'constraints' => array(
                    new Length(array('min' => 6)),
                ),
                'options' => array('attr' => array('class' => 'password-field')),
                'required' => true,
                'first_options' => array('label' => false, 'attr' => array('placeholder' => 'New password'), 'error_bubbling' => true),
                'second_options' => array('label' => false, 'attr' => array('placeholder' => 'Confirm new password'), 'error_bubbling' => true),
            ))
        ;
    }
}
