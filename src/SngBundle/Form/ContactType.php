<?php 
namespace SngBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array('attr' => array('placeholder' => 'Your name', 'class'=> 'text-field', 'id'=> 'nameInput')))
            ->add('email', EmailType::class, array('attr' => array('placeholder' => 'Your email', 'class'=> 'text-field', 'id'=> 'emailInput')))
            ->add('message', TextareaType::class, array('attr' => array('placeholder' => 'Your message', 'class'=> 'message-field', 'id'=> 'messageInput')))
        ;
    }

    public function setDefaultOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'validation_groups' => false,
        ));
    }

    public function getName()
    {
        return 'contact_form';
    }
}