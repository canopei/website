<?php

namespace SngBundle\Processor;

class ServiceNameProcessor
{
    public function processRecord(array $record)
    {
        $record['extra']['service'] = 'website';

        return $record;
    }
}