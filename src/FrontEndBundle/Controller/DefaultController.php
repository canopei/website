<?php

namespace FrontEndBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    const ROBOTS_CACHE_KEY = 'robotstxt.%s';
    const ROBOTS_CACHE_TTL = 600;

    /**
     * @Route("/robots.txt", name="robotstxt")
     * @Method({"GET"})
     */
    public function robotsAction()
    {
        $env = $this->container->getParameter('kernel.environment');
        $cache = $this->get('cache.redis');

        $cacheKey = sprintf(self::ROBOTS_CACHE_KEY, $env);
        $cacheItem = $cache->getItem($cacheKey);
        if (!$cacheItem->isHit()) {
            $response = new Response();
            $response->headers->set('Content-Type', 'text/plain');

            $template = sprintf("FrontEndBundle:robots:%s.txt.twig", $env);
            if (!$this->get('templating')->exists($template)) {
                $template = "FrontEndBundle:robots:default.txt.twig";
            }
            $response = $this->render($template, array(), $response);

            $cacheItem->set($response);
            $cacheItem->expiresAfter(self::ROBOTS_CACHE_TTL);
            $cache->save($cacheItem);
        }

        return $cacheItem->get();
    }

    /**
     * @Route("/whoops/", name="whoops")
     */
    public function whoopsAction()
    {
        return $this->render('FrontEndBundle:Default:404.html.twig');
    }

    /**
     * @Route("/about/", name="about")
     */
    public function aboutAction()
    {
        return $this->render('FrontEndBundle:Default:about.html.twig');
    }

    /**
     * @Route("/contact/", name="contact")
     */
    public function contactAction(Request $request)
    {
        $form = $this->createForm('SngBundle\Form\ContactType', null, array(
            'action' => $this->generateUrl('contact'),
            'method' => 'POST',
            'attr' => array('id' => 'contactForm'),
        ));

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($this->sendContactEmail($form->getData())) {
                $this->get('session')->getFlashBag()->add('msg', 'Thanks, we’ll be in touch soon!');

                return $this->redirectToRoute('contact', array());
            } else {
            }
        }

        return $this->render('FrontEndBundle:Default:contact.html.twig', array(
            'form' => $form->createView(),
        ));
    }


    /**
     * @Route("/terms/", name="terms")
     */
    public function termsAction()
    {
            return $this->render('FrontEndBundle:Default:terms.html.twig');
    }

    /**
     * @Route("/guides/", name="guides")
     */
    public function guidesAction()
    {
            return $this->render('FrontEndBundle:Default:guides.html.twig');
    }

    /**
     * @Route("/privacy/", name="privacy")
     */
    public function privacyAction()
    {
        return $this->render('FrontEndBundle:Default:privacy.html.twig');
    }

    /**
     * @Route("/hot-yoga-experience/", name="hot-yoga-experience")
     */
    public function hotYogaExperienceAction()
    {
        return $this->render('FrontEndBundle:Default:hot-yoga-experience.html.twig');
    }

    /**
     * @Route("/hot-yoga-benefits/", name="hot-yoga-benefits")
     */
    public function hotYogaBenefitsAction()
    {
        return $this->render('FrontEndBundle:Default:hot-yoga-benefits.html.twig');
    }

    /**
     * @Route("/first-hot-yoga-class/", name="first-hot-yoga-class")
     */
    public function firstHotYogaClassAction()
    {
        return $this->render('FrontEndBundle:Default:first-hot-yoga-class.html.twig');
    }


    /**
     * @Route("/hot-yoga-tips/", name="hot-yoga-tips")
     */
    public function hotYogaTipsAction()
    {
        return $this->render('FrontEndBundle:Default:hot-yoga-tips.html.twig');
    }


    /**
     * @Route("/7-reasons-for-hot-yoga/", name="7-reasons-for-hot-yoga")
     */
    public function reasonsForHotYogaAction()
    {
        return $this->render('FrontEndBundle:Default:reasons-for-hot-yoga.html.twig');
    }

    private function sendContactEmail($data)
    {
        $mailer = $this->get('mailer');
        $message = (new \Swift_Message('[Website][Contact] New message'))
            ->setFrom('noreply@sweatnglow.com')
            ->setTo('contact@sweatnglow.com')
            ->setBody(
                $this->renderView(
                    'SngBundle:Emails:Contact/contact_message.html.twig', array(
                        'name' => $data['name'],
                        'email' => $data['email'],
                        'message' => $data['message'],
                    )
                ),
                'text/html'
            );

        return $mailer->send($message);
    }
}
