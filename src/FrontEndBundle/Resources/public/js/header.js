// JavaScript Document
(function(){
    var smMatchPoint = window.matchMedia( "(min-width: 768px)" )
    var snglocation = {
        
        init:function(){
           if(!smMatchPoint.matches){
                $('.navbar-nav').height($(window).height() - 76);
                $('.sec-nav').height($(window).height())
                $('.city-list').height($(window).height());
            }
            $('.resend-mail').unbind('click').bind('click',function(e){
                e.preventDefault();
                snglocation.resendConfirmationEmail();
            })
        },
        resendConfirmationEmail: function(){
            $.ajax(Routing.generate('sendConfirmationEmail'), {
                method: 'POST'
            }).done(function(data) {
                $('.resend-mail').fadeOut();
            }).fail(function(xhr) {
                switch(xhr.status) {
                    case 400:
                        alert("The class is not available to book at this time.");
                        break;
                    default:
                        alert("An error occurred. Please try again.");
                }
            });
        },

        openCityListBig:function(){
            $('.city-list').animate({
                marginTop: "0px"
            }, 300, function(){
                $('.city-select').addClass('open');
            })
            $('.sMap').animate({
                top: "255px"
            }, 300, function(){

            });
        },
        closeCityListBig:function(){
            $('.city-list').animate({
                marginTop: "-255px"
            }, 300, function(){
                $('.city-select').removeClass('open');
            })
            $('.sMap').animate({
                top: "0px"
            }, 300, function(){

            });
        },
        openCityListMobile:function(){
            $('.city-list').show().animate({
                left: "0px"
            }, 500, function(){
                
            })
        },
        closeCityListMobile:function(){
            $('.city-list').animate({
                left: "100%"
            }, 500, function(){
                
                $(this).hide();
            })
        },
        showMenuBack:function(){
            $('.menu-back').show();
        },
        hideMenuBack:function(){
            $('.menu-back').hide();
        },
        showSecNav:function(){
            $('.sec-nav').animate({
                left: "0px"
            }, 500, function(){

            })
        },
        hideSecNav:function(){
            $('.sec-nav').animate({
                left: "100%"
            }, 500, function(){

            })
        }
    }
    $('.city-select').click(function(){
         if(smMatchPoint.matches){
             if($(this).hasClass('open')){
                snglocation.closeCityListBig();
             }
             else{
                snglocation.openCityListBig();
             }
         }
         else{
             snglocation.openCityListMobile();
         }
        
     })
    $('.city-list .icon-close').click(function(){
        snglocation.closeCityListBig();
    })
    $('.navbar-toggle').click(function(){
        $('html, body').css({
            overflow: 'hidden',
            height: '100%'
        });
    })
    $('.mobile-nav .icon-close').click(function(){
        $('.navbar-toggle').trigger('click');
        $('.view-map-container').show();
        snglocation.closeCityListMobile();
        $('html, body').css({
            overflowY: 'auto',
            height: 'auto'
        });
    })
    
    $('.mobile-city-nav .menu-back').click(function(){
        snglocation.closeCityListMobile();
    })

    $('.mobile-sec-nav .menu-back').click(function(){
       snglocation.hideSecNav();
    });

    /* open my account menu for all the pages when logged in  */
    if(!smMatchPoint.matches){
        $('.account a').click(function(event){
            if(!$(this).hasClass('login')){
                event.preventDefault();
                snglocation.showSecNav();
            }
            
        })
    }
    /* for studio listing page */
    $('.navbar-toggle').click(function(){
        $('.view-map-container').hide();
    });

    snglocation.init();

})();