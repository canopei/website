require('../scss/homecity.scss');

$(() => {
    var homecity = {
        init: function () {

            $('.book, .button').click(function () {
                if (!window.App.user) {
                    homecity.showMessage();
                    homecity.showLoginOverlay();
                }
            })
            $('.close-login').click(function () {
                homecity.hideMessage();
                homecity.hideLoginOverlay();
            })

            $('.register-tab a').click(function () {
                homecity.showRegister();
            });

            $('.login-tab a').click(function () {
                homecity.showLogin();
            })
        },
        showRegister: function () {
            $('.login-tab').removeClass('active');
            $('.register-tab').addClass('active');
            $('form#login').hide();
            $('form#registration').css('display', 'inline-block');
        },

        showLogin: function () {
            $('.login-tab').addClass('active');
            $('.register-tab').removeClass('active');
            $('form#login').show();
            $('form#registration').hide();
            $('body').scrollTop(0);
        },

        showMessage: function () {
            $('#login-message').show();
        },
        showLoginOverlay: function () {
            $('.login-overlay').show();
        },
        hideMessage: function () {
            $('#login-message').hide();
        },
        hideLoginOverlay: function () {
            $('.login-overlay').hide();
        },
    }
    homecity.init();
});
