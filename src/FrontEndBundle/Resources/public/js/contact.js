require('../scss/contact.scss');

(function(){
    var contact = {
        init: function(){

        },

        validateForm: function(){
            var validation = true;
            var nameReg = /^[A-Za-z\s]+$/;
            var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

            var names = $('#contact_name').val();
            var email = $('#contact_email').val();
            var message = $('#contact_message').val();
            var inputVal = new Array(names, email, message);
            $('.error').hide();

            if(inputVal[0] == ""){
                $('#contact_name').after('<span class="error"> please enter your name</span>');
                validation = false;
            }
            else if(!nameReg.test(names)){
                $('#contact_name').after('<span class="error">please enter letters only</span>');
                validation = false;
            }
            if(inputVal[1] == ""){
                $('#contact_email').after('<span class="error"> please enter your email</span>');
                validation = false;
            }
            else if(!emailReg.test(email)){
                $('#contact_email').after('<span class="error"> please enter a valid email address</span>');
                validation = false;
            }

            if(inputVal[2] == ""){
                $('#contact_message').after('<span class="error"> please add a message</span>');
                validation = false;
            }

            if(validation != false)
            {
                $('#contactForm').submit();
            }
            else{
            }

        }

    }
    //contact.init();
    window.contact=contact;
    $('.submit').click(function(){
        contact.validateForm();
    })
})();