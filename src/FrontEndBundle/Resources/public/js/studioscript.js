require('../scss/studios.scss');

// JavaScript Document
(function(){
    let debounce = require('../../../../../node_modules/lodash/debounce');
    let PaginationInfiniteScroll = require('./pagination-infinite-scroll').default;

    var mq = window.matchMedia( "(min-width: 768px)" );
    var mapStyle = [{"elementType": "geometry", "stylers": [{"color": "#f5f5f5"}]},{"elementType": "labels.icon","stylers": [{"visibility": "off"}]},{"elementType": "labels.text.fill","stylers": [{"color": "#616161"}]},{"elementType": "labels.text.stroke","stylers": [{"color": "#f5f5f5"}]},{"featureType": "administrative.land_parcel","elementType": "labels.text.fill","stylers": [{"color": "#bdbdbd"}]},{"featureType": "poi","elementType": "geometry","stylers": [{"color": "#eeeeee"}]},{"featureType": "poi","elementType": "labels.text.fill","stylers": [{"color": "#757575"}]},{"featureType": "poi.park","elementType": "geometry","stylers": [{"color": "#e5e5e5"}]},{"featureType": "poi.park","elementType": "labels.text.fill","stylers": [{ "color": "#9e9e9e"}]},{"featureType": "road","elementType": "geometry","stylers": [ {"color": "#ffffff"}]},{"featureType": "road.arterial","elementType": "labels.text.fill","stylers": [{"color": "#757575"}]},{"featureType": "road.highway","elementType": "geometry","stylers": [{"color": "#dadada"}]},{"featureType": "road.highway","elementType": "labels.text.fill","stylers": [{"color": "#616161"}]}, {"featureType": "road.local","elementType": "labels.text.fill", "stylers": [{"color": "#9e9e9e"}]},{"featureType": "transit.line","elementType": "geometry","stylers": [{"color": "#e5e5e5"}]},{"featureType": "transit.station","elementType": "geometry","stylers": [{"color": "#eeeeee"}]},{"featureType": "water", "elementType": "geometry","stylers": [{"color": "#c9c9c9"}]},{"featureType": "water","elementType": "labels.text.fill","stylers": [{"color": "#9e9e9e"}]}]
    var myIcon ='/images/marker.png';
    var Markers = [];
    var infoMarkers = [];
    var currentOpenMarker;
    var defaultIcon = {
      url: myIcon,
      size: new google.maps.Size(28, 40),
      scaledSize: new google.maps.Size(28, 40),
      origin: new google.maps.Point(0,0)
    }
    var hoverIcon = {
      url: '/images/marker-hover.png',
      size: new google.maps.Size(28, 40),
      scaledSize: new google.maps.Size(28, 40),
      origin: new google.maps.Point(0,0)
    }
    var snglocation = {
        init:function(){
            $('.sMap').css('height', ($(window).height()));

            $(() => {
                if (mq.matches) {
                    this.initMap();
                } else{
                    this.initMap();
                }
            });
        },

        initMap:function(){
            if (typeof window.mapLocations === 'undefined') {
                return;
            }

            // compute the mean long and lat
            var lat_max = 0, lat_min = 0, long_min = 0, long_max = 0;
            $.each(window.mapLocations, function(index, val) {
                var long = val.long,
                    lat = val.lat;

                if (long && lat) {
                    if (long < long_min || long_min === 0) {
                        long_min = long;
                    }
                    if (long > long_max || long_max === 0) {
                        long_max = long;
                    }

                    if (lat < lat_min || lat_min === 0) {
                        lat_min = lat;
                    }
                    if (lat > lat_max || lat_max === 0) {
                        lat_max = lat;
                    }
                }
            });

            var uluru = {lat: ((lat_max + lat_min) / 2.0), lng: ((long_max + long_min) / 2.0)};
            var initialMap = new google.maps.Map(document.getElementById('map_canvas'), {
                zoom: 1,
                center: uluru,
                styles: mapStyle,
                zoomControl: true,
                mapTypeControl: false,
                scaleControl: true,
                streetViewControl: false,
                rotateControl: false,
                fullscreenControl: true,
            });
            snglocation.getMarkers(initialMap);

            initialMap.fitBounds(new google.maps.LatLngBounds(
                //bottom left
                new google.maps.LatLng(lat_min-0.3, long_min-0.3),
                //top right
                new google.maps.LatLng(lat_max+0.3, long_max+0.3)
            ));

            var myoverlay = new google.maps.OverlayView();
            myoverlay.draw = function () {
                // add an id to the layer that includes all the markers so you can use it in CSS
                this.getPanes().markerLayer.id='markerLayer';
            };
            myoverlay.setMap(initialMap);

        },

        getMarkers:function(xMap) {
            $.each(window.mapLocations, function(index, val) {
                var markerLatlng = new google.maps.LatLng(val.lat, val.long);
                var markerTitle = val.name;
                var href = Routing.generate('studioDetails', { slug: val.slug });
                var markerImage = val.image;
                var markerId = val.uuid;
                var markerInfoContent = '<a href="'+href+'"><div id="content">'+
                      '<div id="bodyContent">'+
                          '<img src="' + markerImage +'" />' +
                          '<p>'+ markerTitle + '</p>'+
                      '</div>'+
                  '</div></a>';
                snglocation.createMarker(markerLatlng, markerTitle, markerId, markerInfoContent, xMap);
            });
        },

        createMarker:function(markerLatlng, markerTitle, markerId, infowindow , xMap){
            var markerInfo = snglocation.createInfoWindow(infowindow);
            var marker = new google.maps.Marker({
                record_id: markerId,
                position: markerLatlng,
                title: markerTitle,
                icon: defaultIcon,
                map: xMap
            });
            Markers.push(marker);
            infoMarkers.push(markerInfo);
            marker.addListener('click', function () {
                if(currentOpenMarker !== marker)
                {
                    for(i in infoMarkers)
                    {
                        infoMarkers[i].close();
                    }

                    markerInfo.open(xMap, marker);
                }
                currentOpenMarker = marker;
            });
            for(i in infoMarkers){
                google.maps.event.addListener(infoMarkers[i],'closeclick',function(){
                    currentOpenMarker = '';
                });
            }

        },

        createInfoWindow:function(infoWindowContent){
            var infowindow = new google.maps.InfoWindow({
                content: infoWindowContent
            });
            this.overwriteInfoWindow(infowindow);
            return infowindow;
        },

        overwriteInfoWindow: function(infowindow){
            google.maps.event.addListener(infowindow, 'domready', function() {
                // Reference to the DIV which receives the contents of the infowindow using jQuery
                var iwOuter = $('.gm-style-iw');
                /* The DIV we want to change is above the .gm-style-iw DIV.
                    * So, we use jQuery and create a iwBackground variable,
                    * and took advantage of the existing reference to .gm-style-iw for the previous DIV with .prev().
                    */
                var iwBackground = iwOuter.prev();

                // Remove the background shadow DIV
                iwBackground.children(':nth-child(2)').css({'display' : 'none'});

                // Remove the white background DIV
                iwBackground.children(':nth-child(4)').css({'display' : 'none'});
                iwBackground.css('z-index','1');
                var iwCloseBtn = iwOuter.next();

                // Apply the desired effect to the close button
                iwCloseBtn.css({
                    opacity: '1', // by default the close button has an opacity of 0.7
                    right: '33px', top: '25px', // button repositioning
                });

                // clickable area for responsive fix
                iwCloseBtn.next().css({
                    right: '20px', top: '15px', // button repositioning
                });

                // The API automatically applies 0.7 opacity to the button after the mouseout event.
                // This function reverses this event to the desired value.
                iwCloseBtn.mouseout(function(){
                    $(this).css({opacity: '1'});
                });
            });
        },

        changeMarker: function(record_id, iconSet, zIndex){
            for (i in Markers){
                if(Markers[i].record_id === record_id){
                    Markers[i].setIcon(iconSet);
                    Markers[i].setZIndex(zIndex);
                }
            }
        },

        scrollMap:function(){
            if(mq.matches){
                $( window ).scroll(function() {
                    if($(this).scrollTop() > 68)
                    {
                        $( ".map-container" ).css( "top", ($(this).scrollTop() - 68));
                    }
                    else{
                        $( ".map-container" ).css( "top", 0);
                    }
                });
            }
        }
    };
    snglocation.init();
    window.snglocation=snglocation;
    $('.view-map').click(function(){
        $('.sList').hide();
        $('.sMap').show();
        snglocation.initMap();
    })

    $('.results').click(function(){
        $('.sList').show();
        $('.sMap').hide();
    })

    $('.show-map').click(function(){
        $('#map_canvas').animate({
            height: "500px"
        },500 , function(){

        })
    });

    $(".box").hover(function() {
        var id = $(this).data('id');
        snglocation.changeMarker(id, hoverIcon, 1000);
    }, function() {
        var id = $(this).data('id');
        snglocation.changeMarker(id, defaultIcon, 1);
    });

    // Infinite scrolling
    let loadPage = (page) => {
        let citySlug = $('.studio-items').data('citySlug');

        let url = typeof citySlug === 'undefined' ? Routing.generate('studiosList', {page: page})
            : Routing.generate('studiosListByCity', {citySlug: citySlug, page: page});

        return $.ajax({
            url: url,
            method: 'GET'
        });
    };
    let infiniteScroll = new PaginationInfiniteScroll({
        containerSelector: '.studio-items',
        itemSelector: '.studio-box',
        scrollContainer: window,
        negativeMargin: 1000,
        onLoadPage: loadPage
    });
    infiniteScroll
        .on('next', (page) => {
            $('.preloader').show();
        })
        .on('loaded', (page) => {
            $('.preloader').hide();

            if (typeof window.dataLayer !== 'undefined') {
                window.dataLayer.push({event: 'loadListingPage', pageType: 'studios', page: page});
            }
        })
        .on('noneLeft', () => {
            $('.preloader').hide();
        })
        .init();

    // Favourites
    var profileContainer = $('.studio-box');
    var addFavHandler = debounce(function(element) {
        var studioUuid = $(element).parent('a').parent('.studio-box').data('id');

        if (studioUuid) {
            $.ajax(Routing.generate('studioAddToFavourites', {locationUuid: studioUuid}), {
                method: 'POST'
            }).done(function() {
                $(element).removeClass('icon-favorite-empty');
                $(element).addClass('glyphicon-heart');
            });
        }
    }, 500, {'leading': true, 'trailing': false});
    profileContainer.on('click', '.icon-favorite-empty', function(e) {
        e.preventDefault();
        addFavHandler(this);
    });

    var removeFavHandler = debounce(function(element) {
        var studioUuid = $(element).parent('a').parent('.studio-box').data('id');

        if (studioUuid) {
            $.ajax(Routing.generate('studioRemoveFromFavourites', {locationUuid: studioUuid}), {
                method: 'DELETE'
            }).done(function() {
                $(element).removeClass('glyphicon-heart');
                $(element).addClass('icon-favorite-empty');
            });
        }
    }, 500, {'leading': true, 'trailing': false});
    profileContainer.on('click', '.glyphicon-heart', function(e) {
        e.preventDefault();
        removeFavHandler(this);
    });

})();
