$(() => {
    var sngbook = {
        init: function () {
            $('.close-login').click(function () {
                sngbook.hideMessage();
                sngbook.hideLoginOverlay();
            })

            $('.register-tab a').click(function () {
                sngbook.showRegister();
            });

            $('.login-tab a').click(function () {
                sngbook.showLogin();
            })
        },

        showRegister: function () {
            $('.login-tab').removeClass('active');
            $('.register-tab').addClass('active');
            $('form#login').hide();
            $('form#registration').css('display', 'inline-block');
        },

        showLogin: function () {
            $('.login-tab').addClass('active');
            $('.register-tab').removeClass('active');
            $('form#login').show();
            $('form#registration').hide();
            $('body').scrollTop(0);
        },

        showMessage: function () {
            $('#login-message').show();
        },
        showLoginOverlay: function () {
            $('.login-overlay').show();
        },
        hideMessage: function () {
            $('#login-message').hide();
        },
        hideLoginOverlay: function () {
            $('.login-overlay').hide();
        }
    };

    sngbook.init();

    let handleCreateClient = (siteUuid, classUuid, callback) => {
        $.ajax(Routing.generate('cre' +
            'ateClient', {siteUuid: siteUuid}), {
            method: 'POST'
        }).done(function() {
            if (callback) {
                callback();
            }
        }).fail(function() {
            alert("FAILED to create a new client!");
        });
    };
    let formatDate = (date) => {
        var formattedDate;
        var today = new Date();

        var now = new Date(today.getFullYear(), today.getMonth(), today.getDate());

        date = date.substring(0,10).split('-');
        var expirationDate = new Date(date[0], date[1]-1, date[2]);

        var remainingDays = Math.floor(Math.abs(expirationDate - now)/86400000);
        formattedDate = date[2]+'/'+date[1]+'/'+ date[0]+ ' ('+ remainingDays + ' days)';
        return formattedDate;
    };
    let handleBookClass = (classUuid) => {
        $.ajax(Routing.generate('book', {classUuid: classUuid}), {
            method: 'POST'
        }).done(function(data) {
            if (!data.hasOwnProperty('clientServices')) {
                alert("An error occurred. Please try again.");
            }

            let clientServices = data.clientServices;
            if (clientServices.length > 0) {
                // show popup here,
                $('.black-overlay').show();
                $('#modalMembership').show();
                $('body').scrollTop(0);

                let membContainer = $('#modalMembership .modal-content .cards');
                membContainer.html('');

                $('.book-class').data('classUuid', classUuid);
                let isSingle = clientServices.length == 1 ? 'active' : '';
                for (let i = 0; i < clientServices.length; i++)
                {
                    let clientService = clientServices[i];
                    if (clientService.hasOwnProperty('expirationDate')) {

                        //$('.book-class').data('serviceId', clientService['mbId']);

                        membContainer.append('<div class="box '+ isSingle+'" id="membership'+i+'" data-service-Id="'+clientService['mbId']+'">' +
                            '<div class="card ">' +
                            '<p class="name">' + clientService['name'] + '</p>' +
                            '<p class="address">' + clientService['siteName'] + '</p>' +
                            '<p class="remaining-classes">Remaining classes: <span>' + clientService['remaining'] + '</span></p>' +
                            '<p class="expiration-date">Valid until: <br /><span>' + formatDate(clientService['expirationDate']) + '</span> </p>' +
                            '<div class="corner"></div>' +
                            '<div class="selector '+ isSingle+ '"><span class="glyphicon glyphicon-ok"></span></span></div>' +
                            '</div>' +
                            '</div>'
                        );
                    } else {
                        //$('.book-class').data('membershipId', clientService['id']);

                        membContainer.append('<div class="box '+ isSingle+'" id="membership'+i+'" data-membership-Id="'+clientService['id']+'">' +
                            '<div class="card ">' +
                            '<p class="name">' + clientService['membership']['name'] + '</p>' +
                            '<p class="address">Global SweatNGlow</p>' +
                            (clientService['remaining'] > 0 ? '<p class="remaining-classes">Remaining classes: <span>' + clientService['remaining'] + '</span></p>' : '') +
                            '<p class="expiration-date">Valid until: <br /><span>' + formatDate(clientService['validUntil']) + '</span> </p>' +
                            '<div class="corner"></div>' +
                            '<div class="selector '+ isSingle+ '"><span class="glyphicon glyphicon-ok"></span></div>' +
                            '</div>' +
                            '</div>'
                        );
                    }
                }

                $(".clientServices").show();
            } else {
                window.location.href = Routing.generate('purchase');
            }
        }).fail(function(xhr) {
            switch(xhr.status) {
                case 400:
                    if (xhr.responseJSON && xhr.responseJSON.reason) {
                        alert(xhr.responseJSON.reason);
                    } else {
                        alert("The class is not available to book at this time.");
                    }
                    break;
                case 406:
                    res = confirm("It seems that you might have existing memberships. Try to link them?");
                    if (res) {
                        window.location.href = Routing.generate('account_link_list');
                    } else {
                        handleCreateClient(xhr.responseJSON.siteUuid, classUuid, function() { handleBookClass(classUuid); });
                    }
                    break;
                case 409:
                    alert("You are already booked at this time.");
                    break;
                default:
                    alert("An error occurred. Please try again.");
            }
        });
    };

    $('.bookBtn').on('click', function(ev) {
        ev.preventDefault();

        if (!window.App.user) {
            sngbook.showMessage();
            sngbook.showLoginOverlay();
            $('body').scrollTop(0);
            return;
        }


        let classUuid = $(this).data('classUuid');
        if (classUuid) {
            handleBookClass(classUuid);
        }
    });

    $('.book-class').on('click', function(ev) {
        ev.preventDefault();
        let boxes = $('.cards .box');
        let serviceId;
        let membershipId;
        boxes.each(function() {
            if($(this).hasClass('active')){
                serviceId = $(this).data('serviceId');
                membershipId = $(this).data('membershipId');
            }
        });

        let classSelected = $(this);
        let classUuid = classSelected.data('classUuid');

        let data = {};
        if (classUuid && serviceId) {
            data.serviceId = serviceId;
        } else if (classUuid && membershipId) {
            data.membershipId = membershipId;
        }

        $.ajax(Routing.generate('addAccountToClass', {classUuid: classUuid}), {
            method: 'POST',
            data: data
        }).done(function() {
            $('#booked-banner .banner-success .container-md').html('Your class has been booked');
            $('#booked-banner').show();
            let classes = $('.course-box');
            let classSelected;
            classes.each(function(){
                if($(this).data('uuid') == classUuid){
                    classSelected = $(this);
                    classSelected.children('.book').hide();
                    classSelected.append('<p class="status">Booked </p>');
                }
            })
            $('.black-overlay').hide();
            $('#modalMembership').hide();
        }).fail(function() {
            $('#booked-banner .banner-success .container-md').html('An error occurred. Please try again.');
            $('#booked-banner').show();

            $('.black-overlay').hide();
            $('#modalMembership').hide();
        });
    });

    let handleAddToCart = (serviceId, siteUuid, membershipUuid) => {
        let reqData = null;
        if (serviceId && siteUuid) {
            reqData = {serviceId: serviceId, siteUuid: siteUuid};
        } else if (membershipUuid) {
            reqData = {membershipUuid: membershipUuid};
        }

        if (reqData) {
            $.ajax(Routing.generate('addToCart'), {
                method: 'POST',
                data: reqData,
            }).done(function () {
                window.location.href = Routing.generate('checkout');
            }).fail(function () {
                switch(xhr.status) {
                    case 406:
                        res = confirm("It seems that you might have existing memberships. Try to link them?");
                        if (res) {
                            window.location.href = Routing.generate('account_link_list');
                        } else {
                            handleCreateClient(xhr.responseJSON.siteUuid, classUuid, function() {
                                handleAddToCart(serviceId, siteUuid, membershipUuid);
                            });
                        }
                        break;
                    default:
                        alert("An error occurred. Please try again.");
                }
            });
        }
    };

    $('.btnCheckout').on('click', function(ev) {
        ev.preventDefault();

        if (!window.App.user) {
            $('body').scrollTop(0);
            sngbook.showMessage();
            sngbook.showLoginOverlay();
        } else {
            let serviceId = $(this).data('serviceId');
            let siteUuid = $(this).data('siteUuid');
            let membershipUuid = $(this).data('membershipUuid');

            handleAddToCart(serviceId, siteUuid, membershipUuid);
        }
    });

    $('#modalMembership .icon-close').on('click', function() {
        $('.black-overlay').hide();
        $('#modalMembership').hide();
    });

    $('#modalMembership').on('click', '.selector', function() {
        let boxes = $('.selector');
        boxes.each(function(){
            $(this).removeClass('active');
        })
        $(this).addClass('active');
        $(this).parent('.card').parent('.box').addClass('active');
    })
});