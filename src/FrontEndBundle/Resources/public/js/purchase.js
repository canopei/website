require('../scss/purchase.scss');


(function(){
    var purchase = {
        init: function(){
            $('.purchase-button').click(function(){
                if (!window.App.user) {
                    purchase.showMessage();
                    purchase.showLoginOverlay();
                }
            })
            $('.close-login').click(function(){
                purchase.hideMessage();
                purchase.hideLoginOverlay();
            })

            $('.register-tab a').click(function(){
                purchase.showRegister();
            });

            $('.login-tab a').click(function(){
                purchase.showLogin();
            })
        },

        showMessage: function(){
            $('#login-message').show();
        },
        showLoginOverlay: function(){
            $('.login-overlay').show();
        },
        hideMessage: function(){
            $('#login-message').hide();
        },
        hideLoginOverlay: function(){
            $('.login-overlay').hide();
        },
        validateLoginForm: function(){
            var validation = true;
            var nameReg = /^[A-Za-z\s]+$/;
            var emailReg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;


            var email = $('#user_login_email').val();
            var pass = $('#user_login_password').val();
            var inputVal = new Array(email, pass);

            $('.error').hide();

            if(inputVal[0] == ""){
                $('#user_login_email').after('<span class="error"> please enter your email address</span>');
                validation = false;
            }
            else if(!emailReg.test(email)){
                $('#user_login_email').after('<span class="error">please enter a valid email address</span>');
                validation = false;
            }
            if(inputVal[1] == ""){
                $('#user_login_password').after('<span class="error"> please enter your password</span>');
                validation = false;
            }
            return validation;

        },

        validateRegForm: function(){
            var validation = true;
            var nameReg = /^[A-Za-z\s]+$/;
            var emailReg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

            var name = $('#user_registration_name').val();
            var email = $('#user_registration_email').val();
            var pass = $('#user_registration_password').val();
            var inputVal = new Array(name, email, pass);
            $('.error').hide();

            if(inputVal[0] == ""){
                $('#user_registration_name').after('<span class="error"> please enter your name</span>');
                validation = false;
            }
            else if(!nameReg.test(name)){
                $('#user_registration_name').after('<span class="error">please enter a valid name format</span>');
                validation = false;
            }
            else if(name.length < 3){
                $('#user_registration_name').after('<span class="error">please enter a longer name</span>');
                validation = false;
            }
            if(inputVal[1] == ""){
                $('#user_registration_email').after('<span class="error"> please enter your email</span>');
                validation = false;
            }
            else if(!emailReg.test(email)){
                $('#user_registration_email').after('<span class="error">please enter a valid email address</span>');
                validation = false;
            }
            if(inputVal[2] == ""){
                $('#user_registration_password').after('<span class="error"> please enter your password</span>');
                validation = false;
            }
            else if(pass.length < 6){
                $('#user_registration_password').after('<span class="error"> please enter a longer password</span>');
                validation = false;
            }

            return validation;

        },

        showRegister: function(){
            $('.login-tab').removeClass('active');
            $('.register-tab').addClass('active');
            $('form#login').hide();
            $('form#registration').css('display','inline-block');
        },

        showLogin: function(){
            $('.login-tab').addClass('active');
            $('.register-tab').removeClass('active');
            $('form#login').show();
            $('form#registration').hide();
        }
    }
    purchase.init();
    window.purchase=purchase;
    $('#registration .login-button').click(function(){
        return purchase.validateRegForm();
    });

    $('form#login').submit(function(){
        return purchase.validateLoginForm();
    });
})();