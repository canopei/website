require('../scss/homepage.scss');

// JavaScript Document
(function(){
    var mq = window.matchMedia( "(min-width: 768px)" );
    var ratio3x = ($(window).width() - 38)/218;
    var ratio4x = ($(window).width() - 44)/158;
    var sngindex = {
        init:function(){
            
            if(mq.matches){
                // new Swiper('#homeStudios .swiper-container', {
                //     slidesPerView: 3,
                //     slidesPerColumn: 1,
                //     paginationClickable: true,
                //     slidesOffsetAfter: 0,
                //     nextButton: '.swiper-button-next',
                //     prevButton: '.swiper-button-prev',
                //     freeMode: true,

                // });
                // new Swiper('#homeTeachers .swiper-container', {
                //     slidesPerView: 4,
                //     slidesPerColumn: 1,
                //     paginationClickable: true,
                //     slidesOffsetAfter: 0,
                //     nextButton: '.teachers-swiper-button-next',
                //     prevButton: '.teachers-swiper-button-prev',
                //     freeMode: true,

                // });
                new Swiper('#cities .swiper-container', {
                    slidesPerView: 4,
                    slidesPerColumn: 1,
                    paginationClickable: true,
                    slidesOffsetAfter: 0,
                    nextButton: '.city-swiper-button-next',
                    prevButton: '.city-swiper-button-prev',
                    freeMode: true,

                });
            }
            else {
                // new Swiper('#homeStudios .swiper-container', {
                //     slidesPerColumn: 1,
                //     slidesPerView: ratio3x,
                //     slidesOffsetAfter: 0,
                //     nextButton: '.swiper-button-next',
                //     prevButton: '.swiper-button-prev',
                //     freeMode: true,

                // });
                // new Swiper('#homeTeachers .swiper-container', {
                //     slidesPerColumn: 1,
                //     slidesPerView: ratio4x,
                //     slidesOffsetAfter: 0,
                //     nextButton: '.teachers-swiper-button-next',
                //     prevButton: '.teachers-swiper-button-prev',
                //     freeMode: true,

                // });
                new Swiper('#cities .swiper-container', {
                    slidesPerColumn: 1,
                    paginationClickable: true,
                    slidesPerView: ratio4x,
                    slidesOffsetAfter: 0,
                    nextButton: '.city-swiper-button-next',
                    prevButton: '.city-swiper-button-prev',
                    freeMode: true,

                });
            }  

            // bind the search form
            let $searchContainer = $('.search'),
                $searchInput = $searchContainer.find('> input[type=text]'),
                $currentCityLabel = $searchContainer.find('.current-city'),
                allCitiesLabel = 'All cities';

            $searchContainer.find('.dropdown-menu li a').on('click', function(ev) {
                ev.preventDefault();
                $('.dropdown-menu li a').each(function(){
                    $(this).removeClass('active');
                });
                $(this).addClass('active');
                let cityName = $(this).data('cityName');
                if (!cityName) {
                    $currentCityLabel.text(allCitiesLabel);
                } else {
                    $currentCityLabel.text(cityName);
                }

                $searchInput.focus();
            });
            let submitSearchHandler = () => {
                let searchQuery = $searchInput.val();
                if (searchQuery === "") {
                    $searchInput.focus();
                    return;
                }
                let queryParams = { q: searchQuery };

                let cityName = $currentCityLabel.text();
                if (cityName !== allCitiesLabel) {
                    queryParams.city = cityName;
                }

                window.location.href = Routing.generate('search', queryParams);
            };
            $searchInput.on('keydown', (ev) => {
                if (ev.keyCode === 13) {
                    submitSearchHandler()
                }
            });
            $searchContainer.find('.submit-search').on('click', (ev) => {
                ev.preventDefault();

                submitSearchHandler()
            });

            // Hide the flash message notification
            // let successBanners = $('.banner-success');
            // if (successBanners.length > 0) {
            //     setTimeout(() => {
            //         successBanners.slideUp(() => {
            //             successBanners.remove();
            //         });
            //     }, 3000);
            // }
        }
    };

    sngindex.init();
})();