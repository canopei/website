require('../scss/search.scss');

$(() => {
    let PaginationInfiniteScroll = require('./pagination-infinite-scroll').default;
    var mq = window.matchMedia( "(min-width: 768px)" );
    var ratio3x = ($(window).width() - 55)/233;
    function loadCarousel(){
        
    }
    if(!mq.matches){
        var scheduleSwipe = new Swiper('.staffs.swiper-container', {
            slidesPerColumn: 1,
            slidesPerView: ratio3x,
            slidesOffsetAfter: 0,
            freemode: true,
        });
        var scheduleSwipe = new Swiper('.studios.swiper-container', {
            slidesPerColumn: 1,
            slidesPerView: ratio3x,
            slidesOffsetAfter: 0,
            freemode: true,
        });
        var scheduleSwipe = new Swiper('.classes.swiper-container', {
            slidesPerColumn: 1,
            slidesPerView: ratio3x,
            slidesOffsetAfter: 0,
            freemode: true,
        })
    }
    let loadPage = (page) => {
        return $.ajax({
            url: Routing.generate('search', {q: window.SearchCurrentQuery, page: page}),
            method: 'GET'
        });
    };
    let infiniteScroll = new PaginationInfiniteScroll({
        containerSelector: '.search-container',
        itemSelector: '.result-row',
        scrollContainer: window,
        negativeMargin: 1000,
        onLoadPage: loadPage
    });
    infiniteScroll
        .on('next', (page) => {
            $('.preloader').show();
        })
        .on('loaded', (page) => {
            $('.preloader').hide();

            if (typeof window.dataLayer !== 'undefined') {
                window.dataLayer.push({event: 'loadListingPage', pageType: 'search', queryString: window.SearchCurrentQuery, page: page});
            }
        })
        .on('noneLeft', () => {
            $('.preloader').hide();
        })
        .init();
});