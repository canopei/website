require('../scss/homeaccount.scss');

(function(){
    var debounce = require('../../../../../node_modules/lodash/debounce');

    var smMatchPoint = window.matchMedia( "(min-width: 768px)" )
    var myaccount = {
        init: function(){

        },

        validatePassForm: function(){
            var validation = true;

            var oldPass = $('#new_password_old_password').val();
            var newPass = $('#new_password_new_password').val();
            var confirmPass = $('#new_password_confirm_password').val();
            var inputVal = new Array(oldPass, newPass, confirmPass);
            $('.error').hide();

            if(inputVal[0] == ""){
                $('#new_password_old_password').after('<span class="error"> please enter your old password</span>');
                validation = false;
            }
            if(inputVal[1] == ""){
                $('#new_password_new_password').after('<span class="error"> please enter your new password</span>');
                validation = false;
            }
            else if(newPass.length < 6){
                $('#new_password_new_password').after('<span class="error"> please enter a longer password</span>');
                validation = false;
            }

            if(inputVal[2] == ""){
                $('#new_password_confirm_password').after('<span class="error"> please confirm your new password</span>');
                validation = false;
            }
            else if(inputVal[2] != inputVal[1]){
                $('#new_password_confirm_password').after('<span class="error"> your password does not match</span>');
                validation = false;
            }

            return validation;
        },

    }
    //contact.init();
    //window.myaccount=myaccount;

    $('.change-password form').submit(function(){
        return myaccount.validatePassForm();
    });



    var profileContainer = $('.box');
    var removeFavHandler = debounce(function(element) {
        var studioUuid = $(element).parent('.name').parent('a').parent('.box').data('suuid');
        var staffUuid = $(element).parent('.name').parent('a').parent('.box').data('tuuid');

        if (staffUuid) {
            $.ajax(Routing.generate('staffRemoveFromFavourites', {staffUuid: staffUuid}), {
                method: 'DELETE'
            }).done(function() {
                $(element).parent('.name').parent('a').parent('.box').remove();
                if($('.teachersList .container-sm .box').length < 1 ){
                    $('.teachersList').remove();
                    $('.teacher').removeClass('hidden');
                }
            });
        }
        if (studioUuid) {
            $.ajax(Routing.generate('studioRemoveFromFavourites', {locationUuid: studioUuid}), {
                method: 'DELETE'
            }).done(function() {
                $(element).parent('.name').parent('a').parent('.box').remove();
                if($('.studioList .container-sm .box').length < 1 ){
                    $('.studioList').remove();
                    $('.studios').removeClass('hidden');
                }
            });
        }
    }, 500, {'leading': true, 'trailing': false});
    profileContainer.on('click', '.glyphicon-heart', function(e) {
        e.preventDefault();
        removeFavHandler(this);
    });


    var linksContainer = $('.client-links');
    var linkClientHandler = debounce(function(element) {
        var $elementContainer = $(element).closest('.client-link'),
            clientUuid = $elementContainer.data('clientUuid'),
            siteUuid = $elementContainer.data('siteUuid');

        if (clientUuid) {
            $.ajax(Routing.generate('account_link_add'), {
                method: 'POST',
                data: {
                    clientUuid: clientUuid
                }
            }).done(function() {
                // switch links
                $elementContainer.find('.btn-link').addClass('hidden');
                $elementContainer.find('.btn-unlink').removeClass('hidden');

                // handle clients on the same site
                $(element).closest('.client-links').find('.client-link').each(function (i, el) {
                    var _clientUuid = $(el).data('clientUuid'),
                        _siteUuid = $(el).data('siteUuid');

                    if (_siteUuid == siteUuid && _clientUuid != clientUuid) {
                        $(el).find('.site-linked').removeClass('hidden');
                        $(el).find('.btn-link').addClass('hidden');
                    }
                });
            });
        }
    }, 500, {'leading': true, 'trailing': false});
    linksContainer.on('click', '.btn-link', function(ev) {
        ev.preventDefault();
        linkClientHandler(this);
    });

    var unlinkClientHandler = debounce(function(element) {
        var $elementContainer = $(element).closest('.client-link'),
            clientUuid = $elementContainer.data('clientUuid'),
            siteUuid = $elementContainer.data('siteUuid');

        if (clientUuid) {
            $.ajax(Routing.generate('account_link_remove'), {
                method: 'DELETE',
                data: {
                    clientUuid: clientUuid
                }
            }).done(function() {
                // switch links
                $elementContainer.find('.btn-unlink').addClass('hidden');
                $elementContainer.find('.btn-link').removeClass('hidden');

                // handle clients on the same site
                $(element).closest('.client-links').find('.client-link').each(function (i, el) {
                    var _clientUuid = $(el).data('clientUuid'),
                        _siteUuid = $(el).data('siteUuid');

                    if (_siteUuid == siteUuid && _clientUuid != clientUuid) {
                        $(el).find('.site-linked').addClass('hidden');
                        $(el).find('.btn-link').removeClass('hidden');
                    }
                });
            });
        }
    }, 500, {'leading': true, 'trailing': false});
    linksContainer.on('click', '.btn-unlink', function(ev) {
        ev.preventDefault();
        unlinkClientHandler(this);
    });

    if(smMatchPoint.matches){
    }
    else {
        var historySwipe = new Swiper('#history .swiper-container', {
            spaceBetween: 25,
            slidesPerView: 'auto',
            freeMode: true,
        })
    }

    $(() => {
        $('.upcoming-classes .button').on('click', function(ev) {
            ev.preventDefault();

            let classUuid = $(this).data('classUuid');
            if (classUuid) {
                $.ajax(Routing.generate('cancelClassVisit', {classUuid: classUuid}), {
                    method: 'DELETE'
                }).done(function () {
                    window.location.reload();
                }).fail(function (xhr) {
                    switch(xhr.status) {
                        case 409:
                            alert("You cannot cancel this class anymore.");
                            window.location.reload();
                            break;
                        default:
                            alert("An error occurred. Please try again.");
                    }
                });
            }
        });
    });
})();