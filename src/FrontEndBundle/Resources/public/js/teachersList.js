
require('../scss/teachers.scss');

(function() {
    let debounce = require('../../../../../node_modules/lodash/debounce');
    let PaginationInfiniteScroll = require('./pagination-infinite-scroll').default;

    let loadPage = (page) => {
        let citySlug = $('#teachers-profile').data('citySlug');

        let url = typeof citySlug === 'undefined' ? Routing.generate('teachersList', {page: page})
            : Routing.generate('teachersListByCity', {citySlug: citySlug, page: page});

        return $.ajax({
            url: url,
            method: 'GET'
        });
    };
    let infiniteScroll = new PaginationInfiniteScroll({
        containerSelector: '#teachers-profile .container-sm',
        itemSelector: '#teachers-profile .container-sm .profile',
        scrollContainer: window,
        negativeMargin: 1000,
        onLoadPage: loadPage
    });
    infiniteScroll
        .on('next', (page) => {
            $('.preloader').show();
        })
        .on('loaded', (page) => {
            $('.preloader').hide();

            if (typeof window.dataLayer !== 'undefined') {
                window.dataLayer.push({event: 'loadListingPage', pageType: 'teachers', page: page});
            }
        })
        .on('noneLeft', () => {
            $('.preloader').hide();
        })
        .init();

    // Favourites
    var profileContainer = $('.profile');
    var addFavHandler = debounce(function(element) {
        var staffUuid = $(element).parent('.teacher-profile').data('uuid');

        if (staffUuid) {
            $.ajax(Routing.generate('staffAddToFavourites', {staffUuid: staffUuid}), {
                method: 'POST'
            }).done(function() {
                $(element).removeClass('icon-favorite-empty');
                $(element).addClass('glyphicon-heart');
            });
        }
    }, 500, {'leading': true, 'trailing': false});
    profileContainer.on('click', '.icon-favorite-empty', function(e) {
        e.preventDefault();
        addFavHandler(this);
    });
    var removeFavHandler = debounce(function(element) {
        var staffUuid = $(element).parent('.teacher-profile').data('uuid');

        if (staffUuid) {
            $.ajax(Routing.generate('staffRemoveFromFavourites', {staffUuid: staffUuid}), {
                method: 'DELETE'
            }).done(function() {
                $(element).addClass('icon-favorite-empty');
                $(element).removeClass('glyphicon-heart');
            });
        }
    }, 500, {'leading': true, 'trailing': false});
    profileContainer.on('click', '.glyphicon-heart', function(e) {
        e.preventDefault();
        removeFavHandler(this);
    });
})();