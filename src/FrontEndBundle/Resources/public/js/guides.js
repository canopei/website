require('../scss/guides.scss');
(function() {
    $('.legal-nav-mobile').click(function(){
        $('.mobile-dropdown').css('display', 'inline-block');
    })

    $('.icon-close').click(function(){
        $('.mobile-dropdown').hide();
    })

    $(document).on('click', '.mobile-dropdown a', function(event){
            event.preventDefault();
            $('html, body').animate({
                scrollTop: $( $.attr(this, 'href') ).offset().top - 50
            }, 1000);
        });
})();