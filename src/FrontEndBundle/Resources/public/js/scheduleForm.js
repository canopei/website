require('../scss/scheduleForm.scss');

(function(){
    var scheduleForm = {
        init: function(){

        },

        validateForm: function(){
            var validation = true;
            var nameReg = /^[A-Za-z\s]+$/;

            var name = $('#schedule_form_name').val();
            var current_website = $('#schedule_form_current_website').val();
            var address = $('#schedule_form_address').val();

            if(name == ""){
                $('#schedule_form_name').addClass('error');
                validation = false;
            }
            else {
                $('#schedule_form_name').removeClass('error');
            }
            if(current_website == ""){
                $('#schedule_form_current_website').addClass('error');
                validation = false;
            }
            else {
                $('#schedule_form_current_website').removeClass('error');
            }
            if(address == ""){
                $('#schedule_form_address').addClass('error');
                validation = false;
            }
            else {
                $('#schedule_form_address').removeClass('error');
            }

            if(validation != false)
            {
                console.log('dasda');
                $('#form').submit();
            }
            else{
            }

        }

    }
    //contact.init();
    window.scheduleForm=scheduleForm;
    $('.submit').click(function(){
        scheduleForm.validateForm();
    })
})();