import debounce from 'lodash/debounce';
const UNDETERMINED_SCROLLOFFSET = -1;

require('es6-object-assign/auto');

export default class PaginationInfiniteScroll
{
    constructor(options) {
        this.isInitialized = false;
        this.listeners = {
            'noneLeft': [],
            'scroll': [],
            'next': [],
            'loaded': [],
        };
        this.isPaused = true;
        this.currentPage = 1;

        this.options = Object.assign({}, this.getDefaultOptions(), options);

        this.onLoadPage = this.options.onLoadPage;
        this.scrollContainer = this.options.scrollContainer;
        this.negativeMargin = this.options.negativeMargin;
    }

    init() {
        if (this.isInitialized) {
            return;
        }

        this.bind();
        this.isInitialized = true;

        this.resume();
    }

    bind() {
        this.scrollContainer.addEventListener('scroll', debounce(this.scrollHandler.bind(this), 300));
    }

    scrollHandler() {
        if (this.isPaused) {
            return;
        }

        let currentScrollOffset = this.getCurrentScrollOffset(this.scrollContainer),
            scrollThreshold = this.getScrollThreshold();

        // invalid scrollThreshold. The DOM might not have loaded yet...
        if (UNDETERMINED_SCROLLOFFSET === scrollThreshold) {
            return;
        }

        this.fire('scroll', [currentScrollOffset, scrollThreshold]);

        if (currentScrollOffset >= scrollThreshold) {
            this.next();
        }
    }

    next() {
        let self = this;

        this.pause();

        self.fire('next', [this.currentPage + 1]);
        this.onLoadPage(this.currentPage + 1)
            .done((data) => {
                if (data.hasOwnProperty('content')) {
                    this.currentPage++;

                    let listContainer = document.querySelector(this.options.containerSelector);
                    if (listContainer !== null) {
                        listContainer.insertAdjacentHTML('beforeend', data.content);
                    }

                    self.fire('loaded', [this.currentPage, data]);
                }

                self.resume();
            })
            .fail((xhr, err) => {
                if (xhr.status === 404) {
                    self.fire('noneLeft', [this.getLastItem()]);
                    self.offAll('noneLeft');
                } else {
                    console.log("Scrolling error: ", err);
                    self.resume();
                }
            });
    }

    getScrollThreshold(negativeMargin) {
        negativeMargin = negativeMargin || this.negativeMargin;
        negativeMargin = (negativeMargin >= 0 ? negativeMargin * -1 : negativeMargin);

        let lastElement = this.getLastItem();

        // if the don't have a last element, the DOM might not have been loaded,
        // or the selector is invalid
        if (lastElement === null) {
            return UNDETERMINED_SCROLLOFFSET;
        }

        return (lastElement.offsetTop + lastElement.clientHeight + negativeMargin);
    }

    getCurrentScrollOffset(container) {
        let scrollTop = 0,
            containerHeight = 0;

        if (window === container)  {
            scrollTop = container.pageYOffset || document.documentElement.scrollTop;
            containerHeight = container.innerHeight;
        } else {
            scrollTop = container.offsetTop;
            containerHeight = container.clientHeight;
        }

        // compensate for iPhone
        if (navigator.platform.indexOf("iPhone") !== -1 || navigator.platform.indexOf("iPod") !== -1) {
            containerHeight += 80;
        }

        return (scrollTop + containerHeight);
    }

    getLastItem() {
        let items = document.querySelectorAll(this.options.itemSelector);
        return items[items.length -1 ];
    }

    fire(event, args) {
        if (this.listeners.hasOwnProperty(event)) {
            this.listeners[event].forEach((callback) => {
                callback.call(this, ...args)
            });
        }
        return null;
    }

    on(event, callback) {
        if (typeof this.listeners[event] === 'undefined') {
            throw new Error('There is no event called "' + event + '"');
        }
        this.listeners[event].push(callback);

        return this;
    }

    one(event, callback) {
        let self = this;

        let remover = function() {
            self.off(event, callback);
            self.off(event, remover);
        };

        this.on(event, callback);
        this.on(event, remover);

        return this;
    }

    off(event, callback) {
        if (typeof this.listeners[event] === 'undefined') {
            throw new Error('There is no event called "' + event + '"');
        }

        let index = this.listeners[event].indexOf(callback);
        if (index > -1) {
            this.listeners[event].splice(index, 1);
        }

        return this;
    }
    offAll(event) {
        if (typeof this.listeners[event] === 'undefined') {
            throw new Error('There is no event called "' + event + '"');
        }
        this.listeners[event] = [];
        return this;
    }

    pause() {
        this.isPaused = true;
    }
    resume() {
        this.isPaused = false;
    }

    onLoadPage(page) {
        return [];
    }

    getDefaultOptions() {
        return {
            containerSelector: '.container',
            itemSelector: '.item',
            negativeMargin: 10,
            onLoadPage: this.onLoadPage
        };
    }
};