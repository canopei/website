require('../scss/teachers.scss');

(function() {
    var debounce = require('../../../../../node_modules/lodash/debounce');

    var sngteacher = {
        init: function(){
            $('.book, .button').click(function(){
                if (!window.App.user) {
                    sngteacher.showMessage();
                    sngteacher.showLoginOverlay();
                }
            });
            $('.close-login').click(function(){
                sngteacher.hideMessage();
                sngteacher.hideLoginOverlay();
            });
            $('.register-tab a').click(function(){
                sngteacher.showRegister();
            });

            $('.login-tab a').click(function(){
                sngteacher.showLogin();
            })
        },

        showPopOver:function(container){
            container.children('.bubble').show();
        },
        hidePopOver:function(){
            $('.bubble').hide();
        },
        showMessage: function(){
            $('#login-message').show();
        },
        showLoginOverlay: function(){
            $('.login-overlay').show();
        },
        hideMessage: function(){
            $('#login-message').hide();
        },
        hideLoginOverlay: function(){
            $('.login-overlay').hide();
        },
        showRegister: function(){
            $('.login-tab').removeClass('active');
            $('.register-tab').addClass('active');
            $('form#login').hide();
            $('form#registration').css('display','inline-block');
        },

        showLogin: function(){
            $('.login-tab').addClass('active');
            $('.register-tab').removeClass('active');
            $('form#login').show();
            $('form#registration').hide();
        },
    };

    $('.icon-favorites-fill, .icon-favorite-empty').hover(
        function(){
            var $this = $(this);
            sngteacher.showPopOver($this);
        }, function(){
            sngteacher.hidePopOver();
        }
    );

    // Favourites
    var profileContainer = $('.profile-container');
    var addFavHandler = debounce(function(element) {
        var staffUuid = $('.profile-container').data('uuid');

        if (staffUuid) {
            $.ajax(Routing.generate('staffAddToFavourites', {staffUuid: staffUuid}), {
                method: 'POST'
            }).done(function() {
                $(element).removeClass('icon-favorite-empty');
                $(element).addClass('icon-favorites-fill');
                $(element).children('.bubble').children('.content').html('Remove from favorites');
            });
        }
    }, 500, {'leading': true, 'trailing': false});
    profileContainer.on('click', '.icon-favorite-empty', function(e) {
        e.preventDefault();
        addFavHandler(this);
    });


    var removeFavHandler = debounce(function(element) {
        var staffUuid = $('.profile-container').data('uuid');

        if (staffUuid) {
            $.ajax(Routing.generate('staffRemoveFromFavourites', {staffUuid: staffUuid}), {
                method: 'DELETE'
            }).done(function() {
                $(element).removeClass('icon-favorites-fill');
                $(element).addClass('icon-favorite-empty');
                $(element).children('.bubble').children('.content').html('Add to favorites');
            });
        }
    }, 500, {'leading': true, 'trailing': false});
    profileContainer.on('click', '.icon-favorites-fill', function(e) {
        e.preventDefault();
        removeFavHandler(this);
    });
    sngteacher.init();



})();