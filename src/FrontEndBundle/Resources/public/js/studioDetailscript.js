require('../scss/studios.scss');

// JavaScript Document
(function(){
    var debounce = require('../../../../../node_modules/lodash/debounce');

    var mq = window.matchMedia( "(min-width: 768px)" );
    var mapStyle = [{"elementType": "geometry", "stylers": [{"color": "#f5f5f5"}]},{"elementType": "labels.icon","stylers": [{"visibility": "off"}]},{"elementType": "labels.text.fill","stylers": [{"color": "#616161"}]},{"elementType": "labels.text.stroke","stylers": [{"color": "#f5f5f5"}]},{"featureType": "administrative.land_parcel","elementType": "labels.text.fill","stylers": [{"color": "#bdbdbd"}]},{"featureType": "poi","elementType": "geometry","stylers": [{"color": "#eeeeee"}]},{"featureType": "poi","elementType": "labels.text.fill","stylers": [{"color": "#757575"}]},{"featureType": "poi.park","elementType": "geometry","stylers": [{"color": "#e5e5e5"}]},{"featureType": "poi.park","elementType": "labels.text.fill","stylers": [{ "color": "#9e9e9e"}]},{"featureType": "road","elementType": "geometry","stylers": [ {"color": "#ffffff"}]},{"featureType": "road.arterial","elementType": "labels.text.fill","stylers": [{"color": "#757575"}]},{"featureType": "road.highway","elementType": "geometry","stylers": [{"color": "#dadada"}]},{"featureType": "road.highway","elementType": "labels.text.fill","stylers": [{"color": "#616161"}]}, {"featureType": "road.local","elementType": "labels.text.fill", "stylers": [{"color": "#9e9e9e"}]},{"featureType": "transit.line","elementType": "geometry","stylers": [{"color": "#e5e5e5"}]},{"featureType": "transit.station","elementType": "geometry","stylers": [{"color": "#eeeeee"}]},{"featureType": "water", "elementType": "geometry","stylers": [{"color": "#c9c9c9"}]},{"featureType": "water","elementType": "labels.text.fill","stylers": [{"color": "#9e9e9e"}]}]
    var myIcon ='/images/marker.png';
    var defaultIcon = {
      url: myIcon,
      size: new google.maps.Size(28, 40),
      scaledSize: new google.maps.Size(28, 40),
      origin: new google.maps.Point(0,0)
    }
    var snglocation = {
        init:function(){
            if (mq.matches) {
                this.initMap();
            }
            else{
                this.initMap();
            }

            $('.book, .button').click(function(){
                if (!window.App.user) {
                    snglocation.showMessage();
                    snglocation.showLoginOverlay();
                }
            })
            $('.close-login').click(function(){
                snglocation.hideMessage();
                snglocation.hideLoginOverlay();
            })

            $('.register-tab a').click(function(){
                snglocation.showRegister();
            });

            $('.login-tab a').click(function(){
                snglocation.showLogin();
            })
        },

        showRegister: function(){
            $('.login-tab').removeClass('active');
            $('.register-tab').addClass('active');
            $('form#login').hide();
            $('form#registration').css('display','inline-block');
        },

        showLogin: function(){
            $('.login-tab').addClass('active');
            $('.register-tab').removeClass('active');
            $('form#login').show();
            $('form#registration').hide();
            $('body').scrollTop(0);
        },

        showMessage: function(){
            $('#login-message').show();
        },
        showLoginOverlay: function(){
            $('.login-overlay').show();
        },
        hideMessage: function(){
            $('#login-message').hide();
        },
        hideLoginOverlay: function(){
            $('.login-overlay').hide();
        },

        initMap:function(){
            var markers = [];
            var lat = $('.main-area').data('lat');
            var long = $('.main-area').data('long');
            if(mq.matches){
                var offsetLong = long -0.025;
            }
            else {
                var offsetLong = long;
            }

            var uluru = {lat: lat, lng: offsetLong}
            var initialMap = new google.maps.Map(document.getElementById('map_canvas'), {
                    zoom: 14,
                    center: uluru,
                    styles: mapStyle,
                    zoomControl: true,
                    mapTypeControl: false,
                    scaleControl: true,
                    streetViewControl: false,
                    rotateControl: false,
                    fullscreenControl: true
                });
            var markerLatlng = new google.maps.LatLng(lat, long);
            snglocation.createMarker(markerLatlng, initialMap);
        },

        getMarkers:function(xMap){
            var items = $('.item-line');
            $.each(items, function(index, val){

                var markerLatlng = new google.maps.LatLng($(this).data('lat'), $(this).data('lng'));
                var markerTitle =  $(this).data('title');
                var markerAddress = $(this).children('#address').html();
                var markerCity = $(this).children('#city').html();
                var markerPhone = $(this).children('#phone').html();
                snglocation.createMarker(markerLatlng, markerTitle, xMap);
            });
        },

        createMarker:function(markerLatlng, xMap){
            //  var markerInfo = snglocation.createInfoWindow(infowindow);
                var marker = new google.maps.Marker({
                    position: markerLatlng,
                    map: xMap,
                    icon: defaultIcon,
                });

        },

        createInfoWindow:function(infoWindowContent){
            var infowindow = new google.maps.InfoWindow({
                content: infoWindowContent
            });
            return infowindow;
        },

        displayMap:function(infoWindowContent){
            $('.map-container').animate({
                height: "460px"
            },500 , function(){
                $('.icon-arrow-down').hide();
                $('.icon-arrow-up').show();
                $('.show-map').addClass('open');
            })
        },

        hideMap:function(infoWindowContent){
            $('.map-container').animate({
                height: "50px"
            },500 , function(){
                $('.icon-arrow-down').show();
                $('.icon-arrow-up').hide();
                $('.show-map').removeClass('open');
            })
        },

        showPopOver:function(container){
            container.children('.bubble').show();
        },

        hidePopOver:function(){
            $('.bubble').hide();
        }
    }
    snglocation.init();

    $('.show-map').click(function(){
        if($(this).hasClass('open')){
            snglocation.hideMap();
        }
        else{
            snglocation.displayMap();
        }
    });
    if(mq.matches){
        $('.icon-clock, .icon-favorites-fill, .icon-favorite-empty').hover(
            function(){
                var $this = $(this);
                snglocation.showPopOver($this);
            }, function(){
                snglocation.hidePopOver();
            }
        );
    }
    

    // Favourites
    var profileContainer = $('.profile-container');
    var addFavHandler = debounce(function(element) {
        var studioUuid = $('.profile-container').data('uuid');

        if (studioUuid) {
            $.ajax(Routing.generate('studioAddToFavourites', {locationUuid: studioUuid}), {
                method: 'POST'
            }).done(function() {
                $(element).removeClass('icon-favorite-empty');
                $(element).addClass('icon-favorites-fill');
                $(element).children('.bubble').children('.content').html('Remove from favorites');
            });
        }
    }, 500, {'leading': true, 'trailing': false});
    profileContainer.on('click', '.icon-favorite-empty', function(e) {
        e.preventDefault();
        addFavHandler(this);
    });

    var removeFavHandler = debounce(function(element) {
        var studioUuid = $('.profile-container').data('uuid');

        if (studioUuid) {
            $.ajax(Routing.generate('studioRemoveFromFavourites', {locationUuid: studioUuid}), {
                method: 'DELETE'
            }).done(function() {
                $(element).removeClass('icon-favorites-fill');
                $(element).addClass('icon-favorite-empty');
                $(element).children('.bubble').children('.content').html('Add to favorites');
            });
        }
    }, 500, {'leading': true, 'trailing': false});
    profileContainer.on('click', '.icon-favorites-fill', function(e) {
        e.preventDefault();
        removeFavHandler(this);
    });


    $(() => {
        $('.icon-clock').click(function(){
            if(!$(this).hasClass('open')) {
                $('.opening-popover').show();
                $(this).addClass('icon-clock-selected');
                $(this).removeClass('icon-clock');
                $(this).addClass('open');
            }
            else {
                $('.opening-popover').hide();
                $(this).removeClass('icon-clock-selected');
                $(this).addClass('icon-clock');
                $(this).removeClass('open');
            }
        });

        $('.btn-buy-studio-plans').click(function(ev) {
            ev.preventDefault();
            
            const siteUuid = $(this).data('siteUuid');

            $.ajax(Routing.generate('addToPurchase', {siteUuid: siteUuid}), {
                method: 'POST'
            }).done(function () {
                window.location.href = Routing.generate('purchase');
            }).fail(function () {
                alert("An error occurred. Please try again.");
            });
        });
    });
    
    if(mq.matches) {
        $('.crsl').thumbCarousel();
        $('.tmb-wrap').mouseout(function () {
            $('.image-wrap img').each(function () {
                $(this).css('display', 'none');
            })
            $('.image-wrap img:first-child').css('display', 'block');
        });
    }
})();
