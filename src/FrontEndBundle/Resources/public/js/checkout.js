require('../scss/checkout.scss');

(function(){
    var mq = window.matchMedia( "(min-width: 768px)" );
    var checkout = {
        init: function(){
            $('.dropdown-menu li').click(function(){
                $('.current-city').html($(this).html());
                $('#checkout_country').val($(this).html());
            })

            $('.icon-ccv-help').hover(
                function(){
                    var $this = $(this);
                    checkout.showPopOver($this);
                }, function(){
                    checkout.hidePopOver();
                }
            );
        },

        validateForm: function(){
            var validation = true;
            var nameReg = /^[A-Za-z\s]+$/;
            var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

            var cardNo = $('#checkout_cardNumber').val();
            var mm = $('#checkout_month').val();
            var yy = $('#checkout_year').val();

            var firstName = $('#checkout_firstName').val();
            var lastName = $('#checkout_lastName').val();
            var country = $('#checkout_country').val();
            var address = $('#checkout_address').val();
            var city = $('#checkout_city').val();
            var state = $('#checkout_state').val();
            var zip = $('#checkout_zip').val();

            var inputVal = new Array(cardNo, mm, yy, firstName, lastName, country, address, city, state, zip);
            $('.error').hide();
            $('.half-error').hide();
            if(inputVal[0] == "" || typeof inputVal[0] === "undefined"){
                $('#checkout_cardNumber').after('<span class="error"> please enter your card number</span>');
                validation = false;
            }
            if(inputVal[1] == "" || typeof inputVal[1] === "undefined"){
                $('#checkout_month').after('<span class="half-error"> please enter month</span>');
                validation = false;
            }
            if(inputVal[2] == "" || typeof inputVal[2] === "undefined"){
                $('#checkout_year').after('<span class="half-error"> please enter year</span>');
                validation = false;
            }
            if(inputVal[3] == "" || typeof inputVal[3] === "undefined"){
                $('#checkout_firstName').after('<span class="half-error">enter your first name</span>');
                validation = false;
            }
            if(inputVal[4] == "" || typeof inputVal[4] === "undefined"){
                $('#checkout_lastName').after('<span class="half-error">enter your last name</span>');
                validation = false;
            }
            if(inputVal[6] == "" || typeof inputVal[6] === "undefined"){
                $('#checkout_address').after('<span class="error"> please enter your address</span>');
                validation = false;
            }
            if(inputVal[7] == "" || typeof inputVal[7] === "undefined"){
                $('#checkout_city').after('<span class="error"> please enter your city</span>');
                validation = false;
            }
            if(inputVal[8] == "" || typeof inputVal[8] === "undefined"){
                $('#checkout_state').after('<span class="error"> please enter your state</span>');
                validation = false;
            }
            if(inputVal[9] == "" || typeof inputVal[9] === "undefined"){
                $('#checkout_zip').after('<span class="error"> please enter your zip</span>');
                validation = false;
            }


            if (validation !== false)
            {
                $('#checkoutForm').submit();
            }
            else{
            }
        },

        showPopOver:function(container){
            container.children('.bubble').show();
        },

        hidePopOver:function(){
            $('.bubble').hide();
        }
    };

    $(() => {
        checkout.init();

        $('.purchase-button').click(function(ev) {
            ev.preventDefault();

            checkout.validateForm();
        });
    });
})();