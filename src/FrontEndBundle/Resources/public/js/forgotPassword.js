require('../scss/forgot-password.scss');

$(() => {
    let emailReg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    $('.forgot-password-container form').on('submit', () => {
        let email = $('#form_email').val();
        if (!emailReg.test(email)) {
            $(".form-errors").html('<ul><li>Please insert a valid e-mail address.</li></ul>');
            return false;
        }
    });


    $('.change-password-container form').on('submit', () => {
        if ($('#change_password_password_first').val() === '') {
            $(".form-errors").html('<ul><li>Please insert your new password.</li></ul>');
            return false;
        }

        if ($('#change_password_password_first').val().length < 6) {
            $(".form-errors").html('<ul><li>The password must have a length of at least 6 characters.</li></ul>');
            return false;
        }

        if ($('#change_password_password_first').val() !== $('#change_password_password_second').val()) {
            $(".form-errors").html('<ul><li>The password fields must match.</li></ul>');
            return false;
        }
    });
});