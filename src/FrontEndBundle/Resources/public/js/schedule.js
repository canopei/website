
require('../scss/schedule.scss');
// JavaScript Document
(function(){
    var mq = window.matchMedia( "(min-width: 768px)" );
    var weekdays = ['Mon', "Tue", "Wed", "Thu", "Fri", "Sun", "Sat"]
    var sngindex = {
        init:function(){
            for(i=0; i<7; i++){
                if(mq.matches){
                    var scheduleSwipe = new Swiper('.'+weekdays[i]+'.swiper-container', {
                        slidesPerView: 5,
                        spaceBetween: 3,
                        simulateTouch: false,
                        nextButton: '.swiper-button-next-'+weekdays[i],
                        prevButton: '.swiper-button-prev-'+weekdays[i],
                    })
                }
                else {
                    var scheduleSwipe = new Swiper('.'+weekdays[i]+'.swiper-container', {
                        spaceBetween: 3,
                        slidesPerView: 'auto',
                        freeMode: true,
                    })
                }
            }
            $('.book, .button').click(function(){
                if (!window.App.user) {
                    sngindex.showMessage();
                    sngindex.showLoginOverlay();
                }
            })
            $('.close-login').click(function(){
                sngindex.hideMessage();
                sngindex.hideLoginOverlay();
            })

            $('.register-tab a').click(function(){
                sngindex.showRegister();
            });

            $('.login-tab a').click(function(){
                sngindex.showLogin();
            })
        },
        showRegister: function(){
            $('.login-tab').removeClass('active');
            $('.register-tab').addClass('active');
            $('form#login').hide();
            $('form#registration').css('display','inline-block');
        },

        showLogin: function(){
            $('.login-tab').addClass('active');
            $('.register-tab').removeClass('active');
            $('form#login').show();
            $('form#registration').hide();
            $('body').scrollTop(0);
        },

        showMessage: function(){
            $('#login-message').show();
        },
        showLoginOverlay: function(){
            $('.login-overlay').show();
        },
        hideMessage: function(){
            $('#login-message').hide();
        },
        hideLoginOverlay: function(){
            $('.login-overlay').hide();
        },
    };
    sngindex.init();

    $(() => {
        let locationHash = window.location.hash.substr(1)
        if (locationHash !== '') {
            $('.bookBtn').each((idx, bookBtn) => {
                if ($(bookBtn).data('classUuid') === locationHash) {
                    console.log('FOUND', $(bookBtn).parent());
                    $(bookBtn).parent().addClass('highlight');
                }
            });
        }
    });
})();
