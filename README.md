# SweatNGlow

## General architecture
Standard microservice architecture:
- The services are: `account`, `auth`, `class`, `sale`, `site`, `staff`.
- The `gatekeeper` represents the gateway, which should also handle the authorization (resolve the user and forward it to the services)
- There also exists a NSQ message broker user for async communication
- An ElasticSearch is used for indexing the public data for searching and filtering (studios, staff, classes)
- An ELK stack is used for logging and monitoring
- Each service has two subcomponents:
  - a gRPC service - it connects directly to the SQL server - acts like a Repository for the models. A gRPC service should NEVER connect to another service's gRPC service
  - a REST API - which uses the services gRPC components to aggregate data, and provide it to the requesting client.
- Besides the components, most of the services have a `sync` process which syncs data from Mindbody (periodically and manually) to our DB and ES and a `reindex` process, which is orchestrated by the `runreindex` process in the `site` service and creates a new index in ES with fresh data from DB.


- Mindbody exposes a SOAP API and we use `gowsdl` to create a Go service based on its WSDL. The script which does this is located in `mindbody/scripts/gowsdl.sh`. It also does some modifications to fix some issues.

- The SQL migrations are being managed with `sql-migrate`. You can find them in the `sql` folder.
- The ES schema is managed in `site/scripts/es_indices.sh`. Just edit the script, run it and then trigger a reindex from Jenkins and you will have the new schema in place.

- As for the build process, for the services, each has its own `build.xml` file which is used by Jenkins. It has tasks for config preparations, running tests etc. Everything is done a Go docker dev container (the `godev` image in the docker container).
- After the images are built, they are tagged wit hthe Git SHA1 and the pushed to the GKE Registry. On deployment, we update the image on the deployment in GKE (see `build.xml`).


---

## Data flow
- We keep in sync data from Mindbody to our DB and ES
- The website fetches filtered data IDs from ES and then hydrates them from DB.

---

## Syncing new studios
Postman requests: https://www.getpostman.com/collections/fc51cfd5fba9b5e1b294

1. Create the site in SNG with `Site - create from Mindbody` by passing the site's MB ID. This will trigger some syncs from MB to SNG DB & ES.
2. Wait for the sync to finish (when the number of `account.client` stops rising)
3. Update `is_available` to `1` on `staff`, `site` and `location` tables for the new site.
4. Update other fields like location descriptions, name, coords.
5. Add the location's city to an existing city group, or create a new city group for it (in case of a new city group, the group itself must contain the city with the same name too!).
6. Then you can add the photos through the API (after they have been uploaded to `website/web/images`.
    1. `Site - get list` - pick your site UUID
    2. `Location - get from site` - pick the location UUID
    3. `Location photo - create` - fill in the image path
7. You can also use `Location - update` to set the Opening hors.
8. After all the DB updates, trigger a reindex in Jenkins and wait for it to finish.

---
## Dev workflow
- Clone all SNG repos in a folder
- In each folder copy `config.toml.sample` to `config.toml` and do adjustments if needed (secret keys, API keys, paswords).
- Do the same for the `website` `parameters.yml.sample` file.
- In each of the services folders run `glide up` (you need to setup Go and install `glide` globally)
- Then go into `docker` and do the `d4m-nfs` setup (https://github.com/IFSight/d4m-nfs) and the run d4m-nfs.
- Then start the Docker compose containers (`docker-compose up -d`)
- Add to your `/etc/hosts` file:

`127.0.0.1 local.www.sng.com auth.local.sng.com account.local.sng.com site.local.sng.com staff.local.sng.com class.local.sng.com sale.local.sng.com`

`::1 local.www.sng.com auth.local.sng.com account.local.sng.com site.local.sng.com staff.local.sng.com class.local.sng.com sale.local.sng.com`
- There is a handy `dsh.sh` script in `docker` to quickly enter containers (ie. `./dsh.sh class`).

- For each service, the gRPC protocol is represented using `protobuf`. The file is located in the `protobuf/*.proto` folder for each service and you can run `scripts/protobuf.sh` to recreate the Go files after you update the protocol.
- During dev, you can easily get into shell inside the services container, and kill various components, recompile them (`go build -o api_ api/*.go`), and re-start them (`./api_`).

---
## The booking flow
1. The user clicks Book on a class.
2. We check if the user has a client from that studio already linked to his account.
   - 2.a. If not, we check if that studio has a client with the same address as our user.
       - 2.a.i. If yes, we offer the user to redirect him to the linking page. If he refuses, we continue to 2.a.ii.
       - 2.a.ii. If no, we create a new client in Mindbody, on the studio, and link it to our user account.
   - 2.b. If yes, we continue the flow.
3. We check if the user already has eligible memberships to be used for this Class type book (Globals and in Mindbody)
    - 3.a. If yes, we display him a popup to choose an existing membership to use, or buy another one and direct him to the purchase page
       - 3.a.i. If he uses an existing Global membership
           - We book the client in Mindbody using a dummy 'SNG Drop In" pricing option (we buy it and then use it).
           - We decrease the number of uses on the membership in our DB if is a limited uses one
           - We redirect the user to the studio detail page and mark the class as "Booked"
       - 3.a.ii. If he uses an existing Mindbody pricing option
           - We do the booking in Mindbody using that pricing option
           - We redirect the user to the studio detail page and mark the class as "Booked"
    - 3.b. If not, we direct the user to the purchase page.
4. On the purchase page, we display the global memberships and the Mindbody pricing options available for that class.
    - 4.a. If he buys a global membership, we register the purchase and assign the new membership, decrease the number of uses if the case, and book the user in Mindbody in the class he chose (described at 3.a.i)
    - 4.b. If the buys a Mindbody Pricing option, we register the purchase in Mindbody, and then do the booking using the newly bought pricing option.
